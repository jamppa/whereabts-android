package com.whereabts.gcm;

import com.google.inject.Inject;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.FollowNotificationService;

import android.os.Bundle;

public class FollowHandler implements GCMMessageHandler {

	@Inject
	private FollowNotificationService notificationService;
	@Inject
	private SettingsPreferencesRepository settingsRepository;
	
	@Override
	public String type() {
		return "TYPE_MESSAGE_NEW_FOLLOWER";
	}

	@Override
	public void handle(Bundle bundle) {
		if(settingsRepository.isNewFollowerNotificationsEnabled()){
			notificationService.notify(bundle);
		}
	}

}
