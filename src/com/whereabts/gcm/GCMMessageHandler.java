package com.whereabts.gcm;

import android.os.Bundle;

public interface GCMMessageHandler {
	public String type();
	public void handle(Bundle bundle);
}
