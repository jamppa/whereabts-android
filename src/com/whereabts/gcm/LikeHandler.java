package com.whereabts.gcm;

import com.google.inject.Inject;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.MessageLikeNotificationService;

import android.os.Bundle;

public class LikeHandler implements GCMMessageHandler {

	@Inject
	private MessageLikeNotificationService messageLikeNotificationService;
	@Inject
	private SettingsPreferencesRepository settingsPreferences;
	
	@Override
	public String type() {
		return "TYPE_MESSAGE_LIKE";
	}

	@Override
	public void handle(Bundle bundle) {
		if(settingsPreferences.isLikeNotificationsEnabled()){
			messageLikeNotificationService.notify(bundle);
		}
	}

}
