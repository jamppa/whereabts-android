package com.whereabts.gcm;

import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;

import com.google.inject.Inject;

public class GCMMessageHandlers {

	@Inject
	private ReplyHandler replyHandler;
	@Inject
	private ReplyRespondentsHandler replyRespondentsHandler;
	@Inject
	private LikeHandler likeHandler;
	@Inject
	private FollowHandler followHandler;
	
	private Map<String, GCMMessageHandler> handlers = new HashMap<String, GCMMessageHandler>();
	
	public boolean hasHandlerWithType(String type) {		
		return handlers().containsKey(type);
	}
	
	public void callHandler(String type, Bundle bundle) {
		if(hasHandlerWithType(type)){
			handlers().get(type).handle(bundle);
		}
	}
	
	private Map<String, GCMMessageHandler> handlers() {
		handlers.clear();
		handlers.put(replyHandler.type(), replyHandler);
		handlers.put(replyRespondentsHandler.type(), replyRespondentsHandler);
		handlers.put(likeHandler.type(), likeHandler);
		handlers.put(followHandler.type(), followHandler);
		return handlers;
	}

	
}
