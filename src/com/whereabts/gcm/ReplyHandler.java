package com.whereabts.gcm;

import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.MessageReplyNotificationService;

public class ReplyHandler implements GCMMessageHandler {

	@Inject
	private MessageReplyNotificationService messageReplyNotificationService;
	@Inject
	private SettingsPreferencesRepository settingsPreferences;
	
	@Override
	public void handle(Bundle bundle) {
		if(settingsPreferences.isReplyNotificationsEnabled()){
			messageReplyNotificationService.notify(bundle);
		}
	}

	@Override
	public String type() {
		return "TYPE_MESSAGE_REPLY";
	}

}
