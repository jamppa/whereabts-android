package com.whereabts.model;

import com.google.android.gms.maps.model.MarkerOptions;

public class CompactMessageAndMarkerOptions {

	private final CompactMessage compactMessage;
	private final MarkerOptions markerOptions;
	
	public CompactMessageAndMarkerOptions(CompactMessage compactMessage, MarkerOptions markerOptions) {
		this.compactMessage = compactMessage;
		this.markerOptions = markerOptions;
	}
	
	public CompactMessage compactMessage() {
		return compactMessage;
	}
	
	public MarkerOptions markerOptions() {
		return markerOptions;
	}
	
}
