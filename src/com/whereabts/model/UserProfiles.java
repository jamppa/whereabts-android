package com.whereabts.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.os.Bundle;

public class UserProfiles implements Serializable {

	public static final String BUNDLE_KEY = "com.whereabts.models.UserProfiles";
	private static final long serialVersionUID = 1L;
	
	private final List<UserProfile> userProfiles = new ArrayList<UserProfile>();
	
	public UserProfiles(JSONArray json) throws JSONException {
		userProfiles.addAll(extractFromJson(json));
	}
	
	private Collection<? extends UserProfile> extractFromJson(JSONArray json) throws JSONException {
		List<UserProfile> profiles = new ArrayList<UserProfile>();
		for(int i = 0; i < json.length(); i++){
			profiles.add(new UserProfile(json.getJSONObject(i)));
		}
		return profiles;
	}

	public List<UserProfile> get() {
		return userProfiles;
	}

	public Bundle asBundle() {
		Bundle bundle = new Bundle();
		bundle.putSerializable(BUNDLE_KEY, this);
		return bundle;
	}
}
