package com.whereabts.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import com.google.android.gms.maps.model.LatLng;

public class CompactMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String id;
	private final String message;
	private final long createdAt;
	private final long updatedAt;
	private final WhereabtsLocation location;
	private final Category category;
	private final UserProfile userProfile;
	
	public CompactMessage(JSONObject json) throws JSONException {
		this.id = json.getString("_id");
		this.message = json.getString("message");
		this.createdAt = json.getLong("created-at");
		this.updatedAt = json.getLong("updated-at");
		this.location = new WhereabtsLocation(json.getJSONArray("loc"));
		this.category = getCategory(json.getString("category"));
		this.userProfile = new UserProfile(json.getJSONObject("user-profile"));
	}
	
	private Category getCategory(String categoryKey) {
		return new CategoryIdToCategory().get(categoryKey);
	}

	public CompactMessage(Message message) {
		this.id = message.id();
		this.message = message.message();
		this.createdAt = message.createdAt();
		this.location = message.location();
		this.updatedAt = message.updatedAt();
		this.category = message.category();
		this.userProfile = message.userProfile();
	}

	public String id() {
		return this.id;
	}
	
	public String shortMessage() {
		return this.message;
	}
	
	public long createdAt() {
		return this.createdAt;
	}
	
	public long updatedAt() {
		return this.updatedAt;
	}
	
	public WhereabtsLocation location() {
		return this.location;
	}
	
	public LatLng locationAsLatLng() {
		return new LatLng(location.latitude(), location.longitude());
	}
	
	public Category category() {
		return this.category;
	}
	
	public UserProfile userProfile() {
		return this.userProfile;
	}
	
	public String timestampAsPretty() {
		PrettyTime t = new PrettyTime(new Locale(""));
		return t.format(new Date(updatedAt));
	}
	
	public String creationTimeAsPretty() {
		PrettyTime t = new PrettyTime(new Locale(""));
		return t.format(new Date(createdAt));
	}

}
