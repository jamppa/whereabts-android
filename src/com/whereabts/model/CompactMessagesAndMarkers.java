package com.whereabts.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.android.gms.maps.model.Marker;

public class CompactMessagesAndMarkers implements Iterable<CompactMessageAndMarker>, Serializable{

	private static final long serialVersionUID = 1L;
	private final Map<String, CompactMessageAndMarker> markersAndMessages = new HashMap<String, CompactMessageAndMarker>();
	
	public void add(CompactMessageAndMarker messageAndMarker){
		markersAndMessages.put(messageAndMarker.marker().getId(), messageAndMarker);
	}
	
	public void clearAndAddAll(CompactMessagesAndMarkers messagesAndMarkers) {
		for(CompactMessageAndMarker each: markersAndMessages.values()){
			each.remove();
		}
		markersAndMessages.clear();
		markersAndMessages.putAll(messagesAndMarkers.markersAndMessages);
	}
	
	public void clearAll() {
		for(CompactMessageAndMarker each: markersAndMessages.values()){
			each.remove();
		}
		markersAndMessages.clear();
	}
	
	public CompactMessage get(final Marker marker){
		return markersAndMessages.get(marker.getId()).compactMessage();
	}
	
	public void remove(CompactMessageAndMarker messageAndMarker){
		markersAndMessages.remove(messageAndMarker.marker().getId());
	}

	public List<Marker> getMarkers() {
		List<Marker> markers = new ArrayList<Marker>();
		for(CompactMessageAndMarker each : markersAndMessages.values()){
			markers.add(each.marker());
		}
		return markers;
	}

	public Marker getMarkerOfMessage(CompactMessage message) {
		for(CompactMessageAndMarker each : markersAndMessages.values()){
			if(each.compactMessage().id().equals(message.id())){
				return each.marker();
			}
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Iterator<CompactMessageAndMarker> iterator() {
		return new ArrayList(markersAndMessages.values()).iterator();
	}
	
}
