package com.whereabts.model;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class UserWithProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final User me;
	private final User user;
	private final UserProfile userProfile;
	private final int followers;
	private final int following;
	private final int whereabouts;
	
	private boolean followed = false;
	
	public UserWithProfile(JSONObject json, User me) throws JSONException {
		this.me = me;
		this.user = new User(json);
		this.userProfile = new UserProfile(json.getJSONObject("user-profile"));
		this.followed = json.getBoolean("followed");
		this.followers = json.getInt("followers");
		this.following = json.getInt("following");
		this.whereabouts = json.getInt("messages-count");
	}
	
	public User user() {
		return user;
	}
	
	public UserProfile userProfile() {
		return userProfile;
	}
	
	public String userNick() {
		return userProfile.nick();
	}
	
	public String userCountry() {
		return userProfile.country().getDisplayCountry();
	}
	
	public String userBio() {
		return userProfile.bio();
	}
	
	public boolean isOwnProfile() {
		return user.isSameAs(me);
	}
	
	public void isFollowing(boolean is){
		this.followed = is;
	}
	
	public boolean isFollowing() {
		return followed;
	}
	
	public String followersAsString() {
		return Integer.toString(followers);
	}
	
	public String followingAsString() {
		return Integer.toString(following);
	}
	
	public String whereabouts() {
		return Integer.toString(whereabouts);
	}

	public boolean hasFollowers() {
		return followers > 0;
	}

	public boolean hasFollowings() {
		return following > 0;
	}

	public boolean hasWhereabouts() {
		return whereabouts > 0;
	}
}
