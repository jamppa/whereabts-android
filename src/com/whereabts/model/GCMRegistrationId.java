package com.whereabts.model;

import org.json.JSONException;
import org.json.JSONObject;

public class GCMRegistrationId {

	private final String registrationId;
	
	public GCMRegistrationId(String id) {
		this.registrationId = id;
	}
	
	public String registrationId() {
		return registrationId;
	}
	
	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private JSONObject buildJson() throws JSONException {
		return new JSONObject().put("gcm-id", registrationId);
	}
	
}
