package com.whereabts.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

public class Replies implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final List<Reply> replies = new ArrayList<Reply>();
	
	public Replies(JSONArray repliesJson) throws JSONException {
		replies.addAll(toDomain(repliesJson));
	}

	public List<Reply> get() {
		return new ArrayList<Reply>(replies);
	}
	
	public long numOfReplies() {
		return replies.size();
	}
	
	private Collection<? extends Reply> toDomain(JSONArray repliesJson) throws JSONException {
		List<Reply> replies = new ArrayList<Reply>();
		for(int i = 0; i < repliesJson.length(); i++){
			replies.add(new Reply(repliesJson.getJSONObject(i)));
		}
		return replies;
	}
	
	public void addNewReply(Reply sentReply) {
		replies.add(sentReply);
	}
}
