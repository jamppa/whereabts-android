package com.whereabts.model;

import java.io.Serializable;

import com.google.android.gms.maps.model.LatLngBounds;

public class BoundingBox implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final WhereabtsLocation lowerLeft;
	private final WhereabtsLocation upperRight;
	
	public BoundingBox(final LatLngBounds bounds) {
		lowerLeft = buildLowerLeft(bounds);
		upperRight = buildUpperRight(bounds);
	}
	
	public WhereabtsLocation lowerLeft() {
		return lowerLeft;
	}
	
	public WhereabtsLocation upperRight() {
		return upperRight;
	}

	private WhereabtsLocation buildUpperRight(LatLngBounds bounds) {
		return new WhereabtsLocation(bounds.northeast);
	}

	private WhereabtsLocation buildLowerLeft(LatLngBounds bounds) {
		return new WhereabtsLocation(bounds.southwest);
	}
}
