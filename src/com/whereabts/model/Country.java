package com.whereabts.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class Country implements Comparable<Country>{
	
	private final String country;
	private final String displayCountry;
	private final Locale locale;
	
	public Country(Locale locale) {
		this.country = locale.getCountry();
		this.displayCountry = locale.getDisplayCountry();
		this.locale = locale;
	}
	
	public String country() {
		return country;
	}
	
	public String displayCountry() {
		return displayCountry;
	}
	
	public Locale locale() {
		return locale;
	}
	
	public static List<Country> listOfCountries() {
		List<Country> countries = new ArrayList<Country>();
		for(String countryCode : Locale.getISOCountries()){
			countries.add(new Country(new Locale("", countryCode)));
		}
		Collections.sort(countries);
		return countries;
	}

	public static Country currentCountry() {
		return new Country(Locale.getDefault());
	}

	@Override
	public int compareTo(Country another) {
		return this.displayCountry.compareTo(another.displayCountry);
	}
	
}
