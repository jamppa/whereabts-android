package com.whereabts.model;

import java.util.HashMap;

public class CategoryIdToCategory {

	private HashMap<String, Category> categoriesMap = new HashMap<String, Category>();
	
	public CategoryIdToCategory() {
		setupCategoriesMap();
	}
	
	public Category get(final String categoryKey){
		if (categoriesMap.containsKey(categoryKey)) {
			return categoriesMap.get(categoryKey);
		}
		return Category.HAPPENINGS;
	}

	private void setupCategoriesMap() {
		categoriesMap.clear();
		categoriesMap.put(Category.TRAFFIC.categoryKey(), Category.TRAFFIC);
		categoriesMap.put(Category.FOOD_AND_DRINKS.categoryKey(), Category.FOOD_AND_DRINKS);
		categoriesMap.put(Category.HAPPENINGS.categoryKey(), Category.HAPPENINGS);
		categoriesMap.put(Category.HOME_AND_LEISURE.categoryKey(), Category.HOME_AND_LEISURE);
		categoriesMap.put(Category.NIGHTLIFE.categoryKey(), Category.NIGHTLIFE);
		categoriesMap.put(Category.ACTIVITIES.categoryKey(), Category.ACTIVITIES);
		categoriesMap.put(Category.TRAVEL.categoryKey(), Category.TRAVEL);
	}
	
}
