package com.whereabts.model;

import java.io.Serializable;
import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;

public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	public static final String PUBLIC_EMAIL = "anonymous@whereabts.com";
	public static final String PUBLIC_USER_UUID = "ae129325a4db22faab7771f10b39a8af";
	
	private final String id;
	private final String email;
	private final String userUuid;
	
	private User(String id, String email, String userUuid) {
		this.id = id;
		this.userUuid = userUuid;
		this.email = email;
	}
	
	public User(JSONObject json) throws JSONException {
		this.id = json.getString("_id");
		this.email = json.getString("email");
		this.userUuid = json.getString("user-uuid");
	}

	public String email() {
		return this.email;
	}
	
	public String userUuId() {
		return this.userUuid;
	}
		
	public String id() {
		return this.id;
	}
	
	public static User newUser(String email){
		return new User("", email, UUID.randomUUID().toString());
	}
	
	public static User newUser(String id, String userUuid, String email){
		return new User(id, email, userUuid);
	}
	
	public static User newPublicUser(){
		return new User("", PUBLIC_EMAIL, PUBLIC_USER_UUID);
	}

	public JSONObject asRegistrationJson(String country) {
		try {
			return buildJson(country);
		} catch (JSONException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private JSONObject buildJson(String country) throws JSONException {
		return new JSONObject()
			.putOpt("email", this.email)
			.putOpt("user-uuid", this.userUuid)
			.putOpt("nick", defaultNick())
			.putOpt("country", country)
			.putOpt("photo", "")
			.putOpt("description", "");
	}
	
	public boolean isPublicUser() {
		if(email.equals(PUBLIC_EMAIL) && userUuid.equals(PUBLIC_USER_UUID)){
			return true;
		}
		return false;
	}
	
	public boolean isSameAs(User user){
		if(user.email.equals(this.email) 
				&& user.userUuid.equals(this.userUuid)){
			return true;
		}
		return false;
	}

	public boolean isWhole() {
		return !id.isEmpty() && !email.isEmpty() && !userUuid.isEmpty();
	}

	public String defaultNick() {
		return email.split("@")[0];
	}
}
