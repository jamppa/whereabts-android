package com.whereabts.model;

import com.whereabts.R;

public enum Category {
	
	TRAFFIC("traffic", R.drawable.ic_map_traffic), 
	FOOD_AND_DRINKS("food_drinks", R.drawable.ic_map_food), 
	HOME_AND_LEISURE("home_leisure", R.drawable.ic_map_home), 
	TRAVEL("travel", R.drawable.ic_map_travel),
	NIGHTLIFE("nightlife", R.drawable.ic_map_nightlife),
	ACTIVITIES("activities", R.drawable.ic_map_activities), 
	HAPPENINGS("happenings", R.drawable.ic_map_happenings);
	
	private final String categoryKey;
	private final int resourceId;
	
	private Category(String key, int resourceId) {
		this.categoryKey = key;
		this.resourceId = resourceId;
	}
	
	public String categoryKey() {
		return categoryKey;
	}
	
	public int resourceId() {
		return resourceId;
	}
	
}
