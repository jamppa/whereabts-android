package com.whereabts.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;

public class CompactMessages implements Serializable {

	public static final String BUNDLE_KEY = "com.whereabts.models.CompactMessages";
	public static final long serialVersionUID = 1L;
	
	private List<CompactMessage> messages = new ArrayList<CompactMessage>();
	
	private CompactMessages() {}
	
	public CompactMessages(JSONObject asJson) throws JSONException {
		JSONArray messagesJson = asJson.getJSONArray("messages");
		messages.addAll(convertToDomain(messagesJson));
	}

	private List<CompactMessage> convertToDomain(JSONArray messagesJson) throws JSONException {
		List<CompactMessage> messages = new ArrayList<CompactMessage>();
		for(int i = 0; i < messagesJson.length(); i++){
			messages.add(toCompactMessage(messagesJson.getJSONObject(i)));
		}
		return messages;
	}

	private CompactMessage toCompactMessage(JSONObject json) throws JSONException {
		return new CompactMessage(json);
	}

	public List<CompactMessage> get() {
		return new ArrayList<CompactMessage>(this.messages);
	}

	public Bundle asBundle() {
		Bundle bundle = new Bundle();
		bundle.putSerializable(BUNDLE_KEY, this);
		return bundle;
	}

	public boolean isEmpty() {
		return this.messages.isEmpty();
	}

	public CompactMessage first() {
		return this.messages.get(0);
	}
	
	public static CompactMessages empty() {
		return new CompactMessages();
	}

	public int size() {
		return this.messages.size();
	}

	public void addAll(CompactMessages addedMessages) {
		this.messages.addAll(addedMessages.get());
	}
}
