package com.whereabts.model.newly;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class NewFeedback implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String message;
	private final int vote;
	
	public NewFeedback(String message) {
		this.message = message;
		this.vote = 0;
	}
	
	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("message", message);
		json.put("vote", vote);
		return json;
	}

	public String message() {
		return message;
	}

}
