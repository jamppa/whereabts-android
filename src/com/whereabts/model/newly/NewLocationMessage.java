package com.whereabts.model.newly;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.model.OpenGraphObject;
import com.whereabts.model.WhereabtsLocation;

public class NewLocationMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final NewMessage message;
	private final WhereabtsLocation location;
	private final boolean shareToFacebook;
	
	public NewLocationMessage(NewMessage msg, WhereabtsLocation loc, boolean shareToFacebook) {
		this.message = msg;
		this.location = loc;
		this.shareToFacebook = shareToFacebook;
	}
	
	public NewMessage message() {
		return this.message;
	}
	
	public String messageText() {
		return this.message.message();
	}
		
	public WhereabtsLocation location() {
		return this.location;
	}
	
	public boolean shareToFacebook() {
		return shareToFacebook;
	}
	
	public OpenGraphObject asOpenGraphObject() {
		OpenGraphObject og = OpenGraphObject.Factory.createForPost("whereabts:whereabouts");
		og.setTitle(messageText());
		og.setIsScraped(false);
		og.getData().setProperty("location:latitude", location().latitude());
		og.getData().setProperty("location:longitude", location().longitude());
		return og;
	}

	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = message.asJson();
		json.put("loc", location.asJson());
		return json;
	}
}
