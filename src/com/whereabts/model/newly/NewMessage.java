package com.whereabts.model.newly;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.whereabts.model.Category;

public class NewMessage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String message;
	private final Category category;
		
	public String message() {
		return this.message;
	}
	
	public Category category() {
		return this.category;
	}
	
	public NewMessage(String message, Category category) {
		this.message = message;
		this.category = category;
	}
	
	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("message", message);
		json.put("category", category.categoryKey());
		return json;
	}
	
}
