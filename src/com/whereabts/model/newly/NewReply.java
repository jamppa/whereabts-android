package com.whereabts.model.newly;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.whereabts.model.Message;

import roboguice.util.Strings;

public class NewReply implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final Message message;
	private final String reply;
	
	public NewReply(Message messageToReply, String reply) {
		this.message = messageToReply;
		this.reply = reply;
	}
	
	public Message messageToReply() {
		return message;
	}
	
	public String reply() {
		return reply;
	}
	
	public boolean hasReplyMessage() {
		return !Strings.isEmpty(reply);
	}

	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("replymessage", this.reply);
		return json;
	}
}
