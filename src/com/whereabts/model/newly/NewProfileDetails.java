package com.whereabts.model.newly;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

import com.whereabts.model.Country;

public class NewProfileDetails implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String nick;
	private final String country;
	
	private String photo;
	private String bio;
	
	public NewProfileDetails(String nick, Country country) {
		this.nick = nick;
		this.country = country.country();
		this.bio = "";
		this.photo = "";
	}
	
	public String nick() {
		return nick;
	}
	
	public String country() {
		return country;
	}
	
	public void bio(String newBio){
		this.bio = newBio;
	}
	
	public String bio() {
		return bio;
	}
	
	public void photo(String newPhoto){
		this.photo = newPhoto;
	}
	
	public String photo() {
		return photo;
	}

	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e);
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("nick", nick);
		json.put("description", bio);
		json.put("country", country);
		json.put("photo", photo);
		return new JSONObject().put("profile", json);
	}
	
}
