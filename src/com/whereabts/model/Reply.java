package com.whereabts.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

public class Reply implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private final String id;
	private final String userId;
	private final String messageId;
	
	private final String replyMessage;
	private final long createdAt;
	private final UserProfile userProfile;
	
	public Reply(JSONObject json) throws JSONException {
		this.id = json.getString("_id");
		this.userId = json.getString("user_id");
		this.messageId = json.getString("message_id");
		this.replyMessage = json.getString("replymessage");
		this.createdAt = json.getLong("created-at");
		this.userProfile = new UserProfile(json.getJSONObject("user-profile"));
	}
	
	public String id() {
		return id;
	}
	
	public String userId() {
		return userId;
	}
	
	public String messageId() {
		return messageId;
	}
	
	public String replyMessage() {
		return replyMessage;
	}
		
	public long createdAt() {
		return createdAt;
	}
	
	public UserProfile userProfile() {
		return userProfile;
	}
	
	public String prettyTimestamp() {
		PrettyTime t = new PrettyTime(new Locale(""));
		return t.format(new Date(createdAt));
	}

	public CharSequence userNick() {
		return userProfile.nick();
	}
	
}
