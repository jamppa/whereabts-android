package com.whereabts.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;
import org.ocpsoft.prettytime.PrettyTime;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap.Config;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.whereabts.R;
import com.whereabts.api.ApiRoutes;
import com.whereabts.util.time.Day;
import com.whereabts.util.time.DayTimeFormat;
import com.whereabts.util.time.Hour;
import com.whereabts.util.time.HourTimeFormat;
import com.whereabts.util.time.Minute;
import com.whereabts.util.time.MinuteTimeFormat;
import com.whereabts.util.time.TimestampFormatter;

public class Message implements Serializable {

	private static final String SHARE_ROOT_URI = "http://whereabts.com/w/";
	private static final long serialVersionUID = 1L;
	
	private final String id;
	private final String userId;
	private final String message;
	private final long createdAt;
	private final long updatedAt;
	private final long expiresAt;
	private final long views;
	private final long likes;
	private final boolean owner;
	private final boolean liked;
	private final boolean photo;
	private final WhereabtsLocation location;
	private final Replies replies;
	private final Category category;
	private final UserProfile userProfile;
	
	public Message(JSONObject json) throws JSONException {
		this.id = json.getString("_id");
		this.userId = json.getString("user_id");
		this.message = json.getString("message");
		this.createdAt = json.getLong("created-at");
		this.updatedAt = json.getLong("updated-at");
		this.expiresAt = json.getLong("expires-at");
		this.views = json.getLong("views");
		this.owner = json.getBoolean("owns");
		this.liked = json.getBoolean("liked");
		this.photo = json.getBoolean("photo");
		this.likes = json.getLong("likes");
		this.location = new WhereabtsLocation(json.getJSONArray("loc"));
		this.replies = new Replies(json.getJSONArray("replies"));
		this.category = getCategory(json.getString("category"));
		this.userProfile = new UserProfile(json.getJSONObject("user-profile"));
	}
	
	private Category getCategory(String categoryKey) {
		return new CategoryIdToCategory().get(categoryKey);
	}

	public String id() {
		return id;
	}
	
	public String message() {
		return message;
	}
	
	public long createdAt() {
		return createdAt;
	}
	
	public WhereabtsLocation location() {
		return location;
	}
	
	public String viewsAsString() {
		return String.valueOf(views);
	}
	
	public String userId() {
		return userId;
	}
	
	public boolean isOwner() {
		return owner;
	}
	
	public boolean isLiked() {
		return liked;
	}
	
	public boolean hasPhoto() {
		return photo;
	}
	
	public long likes() {
		return likes;
	}
	
	public CharSequence likesAsString() {	
		return String.valueOf(likes);
	}

	public CompactMessage asCompact() {
		return new CompactMessage(this);
	}
	
	public Replies replies() {
		return replies;
	}
	
	public Category category() {
		return category;
	}
	
	public UserProfile userProfile() {
		return userProfile;
	}

	public void addReply(Reply sentReply) {
		replies.addNewReply(sentReply);
	}
	
	public String formattedTimestamp(Context context) {
		return TimestampFormatter.formatTimestamp(context, createdAt);
	}

	public long updatedAt() {
		return updatedAt;
	}
	
	public long expiresAt() {
		return expiresAt;
	}
		
	public Intent shareIntent() {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_TEXT, uriOfMessage());
		intent.setType("text/plain");
		return intent;
	}

	private String uriOfMessage() {
		return SHARE_ROOT_URI + id;
	}

	public CharSequence userNick() {
		return userProfile.nick();
	}

	public CharSequence userCountry() {
		return userProfile.country().getDisplayCountry();
	}
	
	public String userProfilePhoto(String photoApiUrl) {
		return userProfile.profilePhotoUrl(photoApiUrl);
	}
	
	public boolean hasUserProfilePhoto() {
		return userProfile.hasPhoto();
	}

	public boolean isExpired() {
		return expiresAt < System.currentTimeMillis();
	}
	
	private CharSequence expiresAtAsPretty() {
		PrettyTime pt = new PrettyTime(new Locale(""));
		pt.registerUnit(new Day(), new DayTimeFormat());
		pt.registerUnit(new Hour(), new HourTimeFormat());
		pt.registerUnit(new Minute(), new MinuteTimeFormat());
		return pt.format(new Date(expiresAt));
	}

	public CharSequence expirationTimestamp(Context context) {
		if(isExpired()){
			return TimestampFormatter.formatTimestamp(context, expiresAt);
		}
		return expiresAtAsPretty();
	}

	public void showPhoto(ImageView photoView, String photosApi) {
		if(hasPhoto()){
			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.displayImage(messagePhotoUrl(photosApi), photoView, displayImageOptions());
		}
	}

	private DisplayImageOptions displayImageOptions() {
		return new DisplayImageOptions.Builder()
		.cacheOnDisc(true)
		.bitmapConfig(Config.RGB_565)
		.showStubImage(R.drawable.loading_image).build();
	}

	public String messagePhotoUrl(String photosApi) {
		return photosApi + ApiRoutes.MESSAGES_PHOTOS_API.replace(":id", this.id);
	}

}
