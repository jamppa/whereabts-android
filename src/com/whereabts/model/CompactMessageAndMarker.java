package com.whereabts.model;

import com.google.android.gms.maps.model.Marker;

public class CompactMessageAndMarker {

	private CompactMessage compactMessage;
	private Marker marker;
	
	public CompactMessageAndMarker(CompactMessage message, Marker marker) {
		this.compactMessage = message;
		this.marker = marker;
	}
	
	public CompactMessage compactMessage() {
		return compactMessage;
	}
	
	public Marker marker() {
		return marker;
	}

	public void remove() {
		if(marker != null){
			marker.remove();
		}
		marker = null;
		compactMessage = null;
	}
	
	public void showInfoWindow() {
		if(marker != null){
			marker.showInfoWindow();
		}
	}
	
}
