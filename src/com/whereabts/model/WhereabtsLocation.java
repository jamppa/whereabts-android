package com.whereabts.model;

import java.io.Serializable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

public class WhereabtsLocation implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private final double latitude;
	private final double longitude;
	
	public WhereabtsLocation(Location location) {
		this.latitude = location.getLatitude();
		this.longitude = location.getLongitude();
	}
	
	public WhereabtsLocation(LatLng latLng) {
		this.latitude = latLng.latitude;
		this.longitude = latLng.longitude;
	}

	public WhereabtsLocation(JSONArray json) throws JSONException {
		this.longitude = json.getDouble(0);
		this.latitude = json.getDouble(1);
	}

	public double latitude() {
		return this.latitude;
	}
	
	public double longitude() {
		return this.longitude;
	}
	
	public JSONObject asJson() {
		try {
			return buildJson();
		} catch (JSONException e) {
			throw new IllegalArgumentException(e.getMessage());
		}
	}

	private JSONObject buildJson() throws JSONException {
		JSONObject json = new JSONObject();
		json.put("lon", longitude);
		json.put("lat", latitude);
		return json;
	}
	
}
