package com.whereabts.model;

import java.io.Serializable;
import java.util.Locale;

import org.json.JSONException;
import org.json.JSONObject;

import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.whereabts.api.ApiRoutes;

public class UserProfile implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String id = "";
	private String userId = "";
	private String nick = "";
	private Locale country = Locale.getDefault();
	private String bio = "";
	private String photo = "";
	
	public UserProfile() {}
	public UserProfile(JSONObject json) throws JSONException{
		this.id = json.getString("_id");
		this.userId = json.getString("user_id");
		this.nick = json.getString("nick");
		this.country = new Locale("", json.getString("country"));
		this.bio = json.getString("description");
		this.photo = json.getString("photo");
	}
	
	public String id() {
		return id;
	}
	
	public String nick() {
		return nick;
	}
	
	public Locale country() {
		return country;
	}
	
	public String countryAsString() {
		return country.getDisplayCountry();
	}
	
	public String bio() {
		return bio;
	}
	
	public String userId() {
		return userId;
	}
	
	public String photo() {
		return photo;
	}
	
	public boolean isEmpty() {
		return !id.equals("");
	}
	
	public static UserProfile newEmpty() {
		return new UserProfile();
	}

	public boolean hasPhoto() {
		return !photo().isEmpty();
	}
	
	public void showPhoto(ImageView view, String photosApi){
		if(hasPhoto()){
			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.displayImage(profilePhotoUrl(photosApi), view);
		}
		else{
			ImageLoader imageLoader = ImageLoader.getInstance();
			imageLoader.displayImage(noUserProfilePhotoUrl(photosApi), view);
		}
		view.setScaleType(ScaleType.CENTER_CROP);
	}
	
	public String profilePhotoUrl(String photoApiUrl) {
		return photoApiUrl + ApiRoutes.USER_PROFILE_PHOTOS_API + "/" + photo;
	}
	
	public String noUserProfilePhotoUrl(String photoApiUrl) {
		return photoApiUrl + ApiRoutes.NO_USER_PROFILE_PHOTO_API;
	}
	
}
