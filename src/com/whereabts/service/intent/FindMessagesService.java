package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.api.message.FindMessagesApi;
import com.whereabts.model.BoundingBox;
import com.whereabts.model.CompactMessages;
import com.whereabts.util.StringUtils;

public class FindMessagesService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindMessageService";
	public static final String EXTRAS_BOUNDING_BOX = TAG + ".boundingBox";
	public static final String EXTRAS_CATEGORY = TAG + ".category";
	public static final String EXTRAS_RESULT_RECEIVER = TAG + ".resultReceiver";
	public static final String EXTRAS_RESULT_MESSAGES = TAG + ".messages";
	public static final String EXTRAS_RESULT_FAILREASON = TAG + ".reason";
	
	public static final int RESULT_FOUND_MESSAGES = 1;
	public static final int RESULT_ERROR = 2;
	
	@Inject
	private FindMessagesApi findMessagesApi;
	@Inject
	private StringUtils stringUtils;
	
	private ResultReceiver resultReceiver;
	private BoundingBox boundingBox;
	
	public FindMessagesService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		resultReceiver = intent.getParcelableExtra(EXTRAS_RESULT_RECEIVER);
		boundingBox = (BoundingBox) intent.getSerializableExtra(EXTRAS_BOUNDING_BOX);
		if(!hasResultReceiver() || !hasBoundingBox()){
			return;
		}
		findMessages();
	}

	private void findMessages() {
		try{
			CompactMessages messages = findMessagesApi.findMessagesByBoundingBox(boundingBox);
			sendResult(messages);
		}
		catch(Exception e){
			e.printStackTrace();
			sendError(stringUtils.getString(R.string.api_error));
		}
	}

	private void sendError(String reason) {
		if(hasResultReceiver()){
			Bundle bundle = new Bundle();
			bundle.putString(EXTRAS_RESULT_FAILREASON, reason);
			resultReceiver.send(RESULT_ERROR, bundle);
		}
	}

	private void sendResult(CompactMessages messages) {
		if(hasResultReceiver()){
			Bundle bundle = new Bundle();
			bundle.putSerializable(EXTRAS_RESULT_MESSAGES, messages);
			resultReceiver.send(RESULT_FOUND_MESSAGES, bundle);
	    }
	}
	
	private boolean hasBoundingBox() {
		return boundingBox != null;
	}
	
	private boolean hasResultReceiver() {
		return resultReceiver != null;
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		resultReceiver = null;
		boundingBox = null;
	}

}
