package com.whereabts.service.intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserProfilesSearchApi;
import com.whereabts.model.UserProfiles;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import roboguice.service.RoboIntentService;

public class FindRecentUserProfilesService extends RoboIntentService {

	private static final String TAG = "com.whereabts.service.intent.FindRecentUserProfilesService";
	public static final String EXTRAS_RESULT_RECEIVER = TAG + ".resultReceiver";

	@Inject
	private UserProfilesSearchApi api;
	
	public FindRecentUserProfilesService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ResultReceiver receiver = intent.getParcelableExtra(EXTRAS_RESULT_RECEIVER);
		if(receiver == null){
			return;
		}
		findRecentUserProfiles(receiver);
	}

	private void findRecentUserProfiles(ResultReceiver receiver) {
		try{
			UserProfiles profiles = api.findMostRecent();
			receiver.send(0, profiles.asBundle());
		}
		catch(Exception e){
			e.printStackTrace();
			receiver.send(-1, new Bundle());
		}
	}

}
