package com.whereabts.service.intent;

import static android.support.v4.content.LocalBroadcastManager.getInstance;
import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserRegistrationApi;

public class RequiredVersionService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.RequiredVersionService";
	public static final String ACTION_REQUIRED_VERSION_FOUND = TAG + ".actionRequiredVersionFound";
	public static final String ACTION_REQUIRED_VERSION_FIND_FAILED = TAG + ".actionRequiredVersionFindFailed";
	
	public static final String EXTRAS_REQUIRED_VERSION = TAG + ".requiredVersion";
	
	@Inject
	private UserRegistrationApi userRegistrationApi;
	
	public RequiredVersionService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		checkMinimumRequiredClientVersion();
	}

	private void checkMinimumRequiredClientVersion() {
		try{
			int versionCode = userRegistrationApi.findRequiredClientVersion();
			broadcastVersionCode(versionCode);
		}
		catch(Exception e){
			e.printStackTrace();
			broadcastError();
		}
	}

	private void broadcastError() {
		Intent intent = new Intent(ACTION_REQUIRED_VERSION_FIND_FAILED);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

	private void broadcastVersionCode(int versionCode) {
		Intent intent = new Intent(ACTION_REQUIRED_VERSION_FOUND);
		intent.putExtra(EXTRAS_REQUIRED_VERSION, versionCode);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

}
