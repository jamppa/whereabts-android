package com.whereabts.service.intent;

import static roboguice.util.Strings.isEmpty;
import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserRegistrationApi;
import com.whereabts.model.GCMRegistrationId;
import com.whereabts.repo.GCMRepository;
import com.whereabts.service.UserService;

public class GCMRegistrationIdSaveService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.GCMRegistrationIdSaveService";
	public static final String EXTRAS_REGISTRATION_ID = TAG + ".registrationId";
	
	@Inject
	private GCMRepository gcmRepository;
	@Inject
	private UserRegistrationApi registrationApi;
	@Inject
	private UserService userService;
	
	public GCMRegistrationIdSaveService() {
		super(TAG);
	}
	
	@Override
	protected void onHandleIntent(Intent intent) {
		String registrationId = intent.getStringExtra(EXTRAS_REGISTRATION_ID);
		if(isEmpty(registrationId)){
			return;
		}
		saveRegistrationId(registrationId);
	}

	private void saveRegistrationId(String registrationId) {
		try{
			gcmRepository.setRegistrationId(registrationId);
			registrationApi.registerGCMForUser(
					userService.findUser(), new GCMRegistrationId(registrationId));
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
