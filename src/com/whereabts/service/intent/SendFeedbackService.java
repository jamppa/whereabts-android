package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.feedback.SendFeedbackApi;
import com.whereabts.model.newly.NewFeedback;

public class SendFeedbackService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.SendFeedbackService";
	public static final String EXTRAS_FEEDBACK = TAG + ".feedback";
	
	@Inject
	private SendFeedbackApi feedbackApi;
	
	public SendFeedbackService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NewFeedback feedback = (NewFeedback) intent.getSerializableExtra(EXTRAS_FEEDBACK);
		if(feedback == null){
			return;
		}
		sendFeedback(feedback);
	}

	private void sendFeedback(NewFeedback feedback) {
		try{
			feedbackApi.sendFeedback(feedback);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
