package com.whereabts.service.intent;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;

import com.google.inject.Singleton;
import com.whereabts.model.UserWithProfile;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.util.WhereabtsResultReceiver;

@Singleton
public class ServiceLauncher {

	public void launchFindUserProfileService(Activity activity, String userId) {
		Intent intent = new Intent(activity, FindUserProfileService.class);
		intent.putExtra(FindUserProfileService.EXTRAS_USER_ID, userId);
		activity.startService(intent);
	}

	public void launchFollowUserService(Activity activity, UserWithProfile profile) {
		Intent intent = new Intent(activity, FollowUserService.class);
		intent.putExtra(FollowUserService.EXTRAS_USER_PROFILE, profile);
		activity.startService(intent);
	}

	public void launchFindUserFollowersService(
			Activity activity, String userId, WhereabtsResultReceiver receiver) {
		Intent intent = new Intent(activity, FindUserFollowersService.class);
		intent.putExtra(FindUserFollowersService.EXTRAS_USER_ID, userId);
		intent.putExtra(FindUserFollowersService.EXTRAS_RESULT_RECEIVER, receiver);
		activity.startService(intent);
	}

	public void launchfinduserFollowingService(
			Activity activity, String userId, WhereabtsResultReceiver receiver) {
		Intent intent = new Intent(activity, FindUserFollowingService.class);
		intent.putExtra(FindUserFollowingService.EXTRAS_USER_ID, userId);
		intent.putExtra(FindUserFollowingService.EXTRAS_RESULT_RECEIVER, receiver);
		activity.startService(intent);
	}
	
	public void launchFindFollowingMessagesService(Fragment fragment, int skip, WhereabtsResultReceiver receiver) {
		Intent intent = new Intent(fragment.getActivity(), FindFollowingMessagesService.class);
		intent.putExtra(FindFollowingMessagesService.EXTRAS_SKIP, skip);
		intent.putExtra(FindFollowingMessagesService.EXTRAS_RECEIVER, receiver);
		fragment.getActivity().startService(intent);
	}
	
	public void launchFindFollowingMessagesService(
			Fragment fragment, int skip, long olderThan, WhereabtsResultReceiver receiver){
		Intent intent = new Intent(fragment.getActivity(), FindFollowingMessagesService.class);
		intent.putExtra(FindFollowingMessagesService.EXTRAS_SKIP, skip);
		intent.putExtra(FindFollowingMessagesService.EXTRAS_OLDER_THAN, olderThan);
		intent.putExtra(FindFollowingMessagesService.EXTRAS_RECEIVER, receiver);
		fragment.getActivity().startService(intent);
	}
	
	public void launchFindRecentUserProfilesService(Activity activity, WhereabtsResultReceiver receiver) {
		Intent intent = new Intent(activity, FindRecentUserProfilesService.class);
		intent.putExtra(FindRecentUserProfilesService.EXTRAS_RESULT_RECEIVER, receiver);
		activity.startService(intent);
	}

	public void launchSearchUserProfiles(Activity activity, String search, WhereabtsResultReceiver receiver) {
		Intent intent = new Intent(activity, SearchUserProfilesService.class);
		activity.stopService(intent);
		intent.putExtra(SearchUserProfilesService.EXTRAS_RESULT_RECEIVER, receiver);
		intent.putExtra(SearchUserProfilesService.EXTRAS_SEARCH, search);
		activity.startService(intent);
	}
	
	public void launchShareWhereaboutsFacebookService(Activity activity, NewLocationMessage message) {
		Intent intent = new Intent(activity, ShareWhereaboutsFacebookService.class);
		intent.putExtra(ShareWhereaboutsFacebookService.EXTRAS_MESSAGE, message);
		activity.startService(intent);
	}

	public void launchSendMessageService(Activity activity, NewLocationMessage message, Uri photoUri) {
		Intent intent = new Intent(activity.getApplicationContext(), SendMessageService.class);
		intent.putExtra(SendMessageService.EXTRAS_MESSAGE, message);
		intent.putExtra(SendMessageService.EXTRAS_PHOTO, photoUri);
		activity.startService(intent);
	}
	
	public void launchFindUserMessagesService(Fragment fragment, String userId, WhereabtsResultReceiver receiver){
		Intent intent = new Intent(fragment.getActivity(), FindUserMessagesService.class);
		intent.putExtra(FindUserMessagesService.EXTRAS_RECEIVER, receiver);
		intent.putExtra(FindUserMessagesService.EXTRAS_USER_ID, userId);
		fragment.getActivity().startService(intent);
	}
	
	public void launchFindUserMessagesService(
			Fragment fragment, String userId, WhereabtsResultReceiver receiver, int skip, long olderThan){
		Intent intent = new Intent(fragment.getActivity(), FindUserMessagesService.class);
		intent.putExtra(FindUserMessagesService.EXTRAS_RECEIVER, receiver);
		intent.putExtra(FindUserMessagesService.EXTRAS_USER_ID, userId);
		intent.putExtra(FindUserMessagesService.EXTRAS_SKIP, skip);
		intent.putExtra(FindUserMessagesService.EXTRAS_OLDER_THAN, olderThan);
		fragment.getActivity().startService(intent);
	}
	
}
