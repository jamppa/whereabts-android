package com.whereabts.service.intent;

import static roboguice.util.Strings.notEmpty;
import roboguice.receiver.RoboBroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class WhereabtsGCMRegistrationReceiver extends RoboBroadcastReceiver {

	@Override
	protected void handleReceive(Context context, Intent intent) {
		if(intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")){
			handleRegistration(context, intent);
		}
	}

	private void handleRegistration(Context context, Intent intent) {
		String gcmRegistrationid = intent.getStringExtra("registration_id");
		if(notEmpty(gcmRegistrationid)){
			saveRegistrationId(gcmRegistrationid, context);
		}
	}

	private void saveRegistrationId(String gcmRegistrationid, Context context) {
		Intent intent = new Intent(context, GCMRegistrationIdSaveService.class);
		intent.putExtra(GCMRegistrationIdSaveService.EXTRAS_REGISTRATION_ID, gcmRegistrationid);
		context.startService(intent);
	}
}
