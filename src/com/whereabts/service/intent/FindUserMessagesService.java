package com.whereabts.service.intent;

import com.google.inject.Inject;
import com.whereabts.api.message.FindUserMessagesApi;
import com.whereabts.model.CompactMessages;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import roboguice.service.RoboIntentService;

public class FindUserMessagesService extends RoboIntentService {

	private static final String TAG = "com.whereabts.service.intent.FindUserMessagesService";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	public static final String EXTRAS_OLDER_THAN = TAG + ".olderThan";
	public static final String EXTRAS_SKIP = TAG + ".skip";
	public static final String EXTRAS_RECEIVER = TAG + ".receiver";
	
	@Inject
	private FindUserMessagesApi api;
	
	public FindUserMessagesService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ResultReceiver receiver = intent.getParcelableExtra(EXTRAS_RECEIVER);
		String userId = intent.getStringExtra(EXTRAS_USER_ID);
		long olderThan = intent.getLongExtra(EXTRAS_OLDER_THAN, System.currentTimeMillis());
		int skip = intent.getIntExtra(EXTRAS_SKIP, 0);
		
		if(userId == null || receiver == null){
			return;
		}
		
		if(skip >= 250){
			receiver.send(0, CompactMessages.empty().asBundle());
			return;
		}
		
		findUserMessages(userId, skip, olderThan, receiver);
	}

	private void findUserMessages(
			String userId, int skip, long olderThan, ResultReceiver receiver) {
		try{
			CompactMessages messages = api.findOlderThan(userId, skip, olderThan);
			receiver.send(0, messages.asBundle());
		} catch(Exception e) {
			e.printStackTrace();
			receiver.send(1, new Bundle());
		}
	}

}
