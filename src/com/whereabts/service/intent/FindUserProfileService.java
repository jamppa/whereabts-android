package com.whereabts.service.intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserProfilesApi;
import com.whereabts.model.UserWithProfile;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import roboguice.service.RoboIntentService;

public class FindUserProfileService extends RoboIntentService {

	private static final String TAG = "com.whereabts.service.intent.FindUserProfileService";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	public static final String EXTRAS_RESULT_USER_PROFILE = TAG + ".userWithProfile";
	public static final String EXTRAS_RESULT_ERROR = TAG + ".error";
	
	public static final String ACTION_USER_PROFILE_FOUND = TAG + ".userWithProfileFound";
	public static final String ACTION_USER_PROFILE_FIND_ERROR = TAG + ".iserWithProfileFindError";
	
	@Inject
	private UserProfilesApi userProfilesApi;

	public FindUserProfileService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String userId = intent.getStringExtra(EXTRAS_USER_ID);
		if(userId == null){
			return;
		}
		findUserProfile(userId);
	}

	private void findUserProfile(String userId) {
		try{
			UserWithProfile userWithProfile = userProfilesApi.findUserWithProfile(userId);
			broadcastUserWithProfileFound(userWithProfile);
		}
		catch(Exception e){
			e.printStackTrace();
			broadcastUserWithProfileFindError(e.getMessage());
		}
	}

	private void broadcastUserWithProfileFindError(String message) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_USER_PROFILE_FIND_ERROR);
		intent.putExtra(EXTRAS_RESULT_ERROR, message);
		broadcastManager.sendBroadcast(intent);
	}

	private void broadcastUserWithProfileFound(UserWithProfile userWithProfile) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_USER_PROFILE_FOUND);
		intent.putExtra(EXTRAS_RESULT_USER_PROFILE, userWithProfile);
		broadcastManager.sendBroadcast(intent);
	}

}
