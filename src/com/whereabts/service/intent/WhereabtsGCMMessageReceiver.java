package com.whereabts.service.intent;

import roboguice.receiver.RoboBroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.inject.Inject;
import com.whereabts.gcm.GCMMessageHandlers;

public class WhereabtsGCMMessageReceiver extends RoboBroadcastReceiver {

	@Inject
	private GCMMessageHandlers handlers;
	
	@Override
	protected void handleReceive(Context context, Intent intent) {
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(context);
		if(gcm.getMessageType(intent).equals(GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE)){
			handleMessage(intent);
		}
	}

	private void handleMessage(Intent intent) {
		String messageType = intent.getExtras().getString("type", "");
		handlers.callHandler(messageType, intent.getExtras());
	}
	
}
