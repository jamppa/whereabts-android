package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.whereabts.api.message.ReplyMessageApi;
import com.whereabts.model.Reply;
import com.whereabts.model.newly.NewReply;

public class SendReplyService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.SendReplyService";
	public static final String EXTRAS_REPLY = TAG + ".reply";
	public static final String EXTRAS_FAIL_REASON = TAG + ".failReason";
	
	public static final String ACTION_REPLY_SENT = TAG + ".actionReplySent";
	public static final String ACTION_REPLY_SEND_FAILED = TAG + ".actionReplySendFailed";
	
	@Inject
	private ReplyMessageApi api;
	
	public SendReplyService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NewReply reply = (NewReply) intent.getSerializableExtra(EXTRAS_REPLY);
		if(reply == null){
			return;
		}
		sendReply(reply);
	}

	private void sendReply(NewReply reply) {
		try{
			Reply sentReply = api.sendReply(reply);
			broadcastReplySent(sentReply);
		}
		catch(Exception e){
			broadcastReplySendFailed(e.getMessage());
		}
	}

	private void broadcastReplySendFailed(String message) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_REPLY_SEND_FAILED);
		intent.putExtra(EXTRAS_FAIL_REASON, message);
		broadcastManager.sendBroadcast(intent);
	}

	private void broadcastReplySent(Reply sentReply) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_REPLY_SENT);
		intent.putExtra(EXTRAS_REPLY, sentReply);
		broadcastManager.sendBroadcast(intent);
	}

}
