package com.whereabts.service.intent;

import static android.support.v4.content.LocalBroadcastManager.getInstance;
import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserProfilePhotosApi;
import com.whereabts.api.user.UserProfilesApi;
import com.whereabts.model.UserProfile;
import com.whereabts.model.newly.NewProfileDetails;
import com.whereabts.service.UserService;

public class SaveUserProfileService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.SaveUserProfileService";
	public static final String EXTRAS_NEW_USER_PROFILE = TAG + ".userProfile";
	public static final String ACTION_USER_PROFILE_SAVED = TAG + ".saved";
	public static final String ACTION_USER_PROFILE_SAVE_FAILED = TAG + ".saveFailed";
	
	@Inject
	private UserProfilesApi userProfilesApi;
	@Inject
	private UserProfilePhotosApi userProfilePhotosApi;
	@Inject
	private UserService userService;
	
	public SaveUserProfileService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NewProfileDetails profileDetails = (NewProfileDetails) intent.getSerializableExtra(EXTRAS_NEW_USER_PROFILE);
		if(profileDetails == null){
			return;
		}
		saveNewProfileDetails(profileDetails);
	}

	private void saveNewProfileDetails(NewProfileDetails profileDetails) {
		try{
			saveProfileDetails(profileDetails);
			broadcastUserProfileSaved();
		}
		catch(Exception e){
			e.printStackTrace();
			broadcastForFailure(e.getMessage());
		}
	}

	private void saveProfileDetails(NewProfileDetails profileDetails) {
		UserProfile savedProfile = 
				userProfilesApi.saveNewUserProfile(profileDetails);
		userService.saveUserProfile(savedProfile);
	}
	
	private void broadcastUserProfileSaved() {
		Intent intent = new Intent();
		intent.setAction(ACTION_USER_PROFILE_SAVED);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

	private void broadcastForFailure(String message) {
		Intent intent = new Intent();
		intent.setAction(ACTION_USER_PROFILE_SAVE_FAILED);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

}
