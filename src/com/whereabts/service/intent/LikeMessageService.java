package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.message.LikeMessageApi;
import com.whereabts.model.Message;

public class LikeMessageService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.LikeMessageService";
	public static final String EXTRAS_MESSAGE_TO_LIKE =TAG +  ".messageToLike";
	
	@Inject
	private LikeMessageApi likeMessageApi;
	
	public LikeMessageService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Message likedMessage = (Message) intent.getSerializableExtra(EXTRAS_MESSAGE_TO_LIKE);
		if(likedMessage == null){
			return;
		}
		likeTheMessage(likedMessage);
	}

	private void likeTheMessage(Message likedMessage) {
		try{
			likeMessageApi.like(likedMessage);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
