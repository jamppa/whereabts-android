package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.whereabts.api.exception.ApiException;
import com.whereabts.api.user.UserProfilesApi;
import com.whereabts.model.UserProfile;

public class FindCurrentUserProfileService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindCurrentUserProfileService";
	public static final String EXTRAS_RESULT_USER_PROFILE = TAG + ".userProfile";
	public static final String EXTRAS_RESULT_ERROR = TAG + ".error";
	
	public static final String ACTION_CURRENT_USER_PROFILE_FOUND = TAG + ".userProfileFound";
	public static final String ACTION_CURRENT_USER_PROFILE_FIND_ERROR = TAG + ".userProfileFindError";
	
	@Inject
	private UserProfilesApi userProfilesApi;
	
	public FindCurrentUserProfileService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		findUserProfileOfCurrentUser();
	}

	private void findUserProfileOfCurrentUser() {
		try{
			UserProfile profile = userProfilesApi.findUserProfile();
			broadcastUserProfileFound(profile);
		}
		catch(ApiException e){
			e.printStackTrace();
			broadcastUserProfileFindError(e.getMessage());
		}
	}

	private void broadcastUserProfileFindError(String message) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_CURRENT_USER_PROFILE_FIND_ERROR);
		intent.putExtra(EXTRAS_RESULT_ERROR, message);
		broadcastManager.sendBroadcast(intent);
	}

	private void broadcastUserProfileFound(UserProfile profile) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_CURRENT_USER_PROFILE_FOUND);
		intent.putExtra(EXTRAS_RESULT_USER_PROFILE, profile);
		broadcastManager.sendBroadcast(intent);
	}

}
