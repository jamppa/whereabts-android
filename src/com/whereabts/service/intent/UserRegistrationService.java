package com.whereabts.service.intent;

import static android.support.v4.content.LocalBroadcastManager.getInstance;

import java.io.IOException;

import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.inject.Inject;
import com.whereabts.api.user.UserRegistrationApi;
import com.whereabts.model.User;
import com.whereabts.repo.GCMRepository;
import com.whereabts.service.UserService;

public class UserRegistrationService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.AnonymousUserIdRegistrationService";
	public static final String EXTRAS_ANON_USER = TAG + ".anonUserId";
	public static final String ACTION_ANON_USER_REGISTERED = TAG + ".actionAnonUserRegistered";
	public static final String ACTION_ANON_USER_REGISTRATION_FAILED = TAG + ".actionAnonUserRegistrationFailed";
	
	private static final String GCM_SENDER_ID = "311487170031";
	
	@Inject
	private UserService userService;
	@Inject
	private UserRegistrationApi api;
	@Inject
	private GCMRepository gcmRepository;
	
	public UserRegistrationService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		User user = (User) intent.getSerializableExtra(EXTRAS_ANON_USER);
		if(user == null){
			return;
		}
		registerUser(user, userService.userCountry());
	}

	private void registerUser(User user, String country) {
		try{
			if(userService.hasRegisteredUser()){
				registerGCM(user);
			}
			else{
				registerUserAndGCM(user, country);
			}
			broadcastUserRegistered();
		}
		catch(Exception e){
			e.printStackTrace();
			broadcastUserRegistrationFailed();
		}
	}
	
	private void registerGCM(User user)  {
		if(!gcmRepository.hasValidRegistrationId()){
			try {
				GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getApplicationContext());
				String gcmRegistrationId = gcm.register(GCM_SENDER_ID);
				gcmRepository.setRegistrationId(gcmRegistrationId);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void registerUserAndGCM(User user, String country) throws Exception {
		User registeredUser = api.registerUser(user, country);
		userService.saveUser(registeredUser);
		registerGCM(registeredUser);
	}

	private void broadcastUserRegistrationFailed() {
		Intent intent = new Intent(ACTION_ANON_USER_REGISTRATION_FAILED);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

	private void broadcastUserRegistered() {
		Intent intent = new Intent(ACTION_ANON_USER_REGISTERED);
		getInstance(getApplicationContext()).sendBroadcast(intent);
	}

}
