package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.message.DeleteMessageApi;
import com.whereabts.model.Message;

public class DeleteMessageService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.DeleteMessageService";
	public static final String EXTRAS_MESSAGE = TAG + ".message";
	
	@Inject
	private DeleteMessageApi deleteMessageApi;
	
	public DeleteMessageService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Message messageToDelete = (Message) intent.getSerializableExtra(EXTRAS_MESSAGE);
		if(messageToDelete == null){
			return;
		}
		deleteMessage(messageToDelete);
	}

	private void deleteMessage(Message messageToDelete) {
		try{
			deleteMessageApi.deleteMessage(messageToDelete);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

}
