package com.whereabts.service.intent;

import com.google.inject.Inject;
import com.whereabts.api.user.FollowUserApi;
import com.whereabts.model.UserWithProfile;

import android.content.Intent;
import roboguice.service.RoboIntentService;

public class FollowUserService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FollowUserService";
	public static final String EXTRAS_USER_PROFILE = TAG + ".userProfile";
	
	@Inject
	private FollowUserApi api;
	
	public FollowUserService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		UserWithProfile userProfile = (UserWithProfile) intent.getSerializableExtra(EXTRAS_USER_PROFILE);
		if(userProfile == null){
			return;
		}
		try{
			followOrUnfollow(userProfile);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private void followOrUnfollow(UserWithProfile userProfile) {
		if(userProfile.isFollowing()){
			api.followUser(userProfile);
			return;
		}
		api.unFollowUser(userProfile);
	}

}
