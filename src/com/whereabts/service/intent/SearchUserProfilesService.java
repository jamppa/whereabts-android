package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import roboguice.util.Strings;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.inject.Inject;
import com.whereabts.api.user.UserProfilesSearchApi;
import com.whereabts.model.UserProfiles;

public class SearchUserProfilesService extends RoboIntentService {

	private static final String TAG = "com.whereabts.service.intent.SearchUserProfilesService";
	public static final String EXTRAS_RESULT_RECEIVER = TAG + ".resultReceiver";
	public static final String EXTRAS_SEARCH = TAG + ".search";
	
	@Inject
	private UserProfilesSearchApi api;

	public SearchUserProfilesService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		ResultReceiver receiver = intent.getParcelableExtra(EXTRAS_RESULT_RECEIVER);
		String search = intent.getStringExtra(EXTRAS_SEARCH);
		if(receiver == null ||Strings.isEmpty(search)){
			return;
		}
		searchUserProfiles(search, receiver);
	}

	private void searchUserProfiles(String search, ResultReceiver receiver) {
		try{
			UserProfiles foundProfiles = api.find(search);
			receiver.send(0, foundProfiles.asBundle());
		}
		catch(Exception e){
			e.printStackTrace();
			receiver.send(-1, new Bundle());
		}
	}

}
