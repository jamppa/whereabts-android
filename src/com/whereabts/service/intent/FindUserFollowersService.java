package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.inject.Inject;
import com.whereabts.api.user.UserFollowersApi;
import com.whereabts.model.UserProfiles;

public class FindUserFollowersService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindUserFollowersService";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	public static final String EXTRAS_RESULT_RECEIVER = TAG + ".resultReceiver";

	@Inject
	private UserFollowersApi userFollowersApi;
	
	public FindUserFollowersService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String userId = intent.getStringExtra(EXTRAS_USER_ID);
		ResultReceiver resultReceiver = intent.getParcelableExtra(EXTRAS_RESULT_RECEIVER);
		if(userId == null || resultReceiver == null){
			return;
		}
		findFollowersForUser(userId, resultReceiver);
	}

	private void findFollowersForUser(String userId, ResultReceiver resultReceiver) {
		try{
			UserProfiles followersProfiles = userFollowersApi.findFollowersOfUser(userId);
			resultReceiver.send(0, followersProfiles.asBundle());
		}
		catch(Exception e){
			e.printStackTrace();
			resultReceiver.send(1, new Bundle());
		}
	}

}
