package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.whereabts.api.message.NewMessageApi;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.service.UploadWhereaboutsPhotoService;

public class SendMessageService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.SendMessageService";
	public static final String EXTRAS_MESSAGE = TAG + ".message";
	public static final String EXTRAS_PHOTO = TAG + ".photo";
	public static final String ACTION_MESSAGE_SENT = TAG + ".messageSent";
	
	@Inject
	private NewMessageApi messagesApi;
	
	public SendMessageService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) { 
		NewLocationMessage message  = (NewLocationMessage) intent.getSerializableExtra(EXTRAS_MESSAGE);
		Uri photoUri = intent.getParcelableExtra(EXTRAS_PHOTO);
		if(message == null){
			return;
		}
		sendNewMessage(message, photoUri);
	}

	private void sendNewMessage(NewLocationMessage message, Uri photoUri) {
		try{
			CompactMessage newMessage = messagesApi.sendMessage(message);
			sendPhoto(photoUri, newMessage.id());
			broadcastNewMessageSent(newMessage);
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private void sendPhoto(Uri photoUri, String id) {
		if(photoUri != null){
			Intent intent = new Intent(getApplicationContext(), UploadWhereaboutsPhotoService.class);
			intent.putExtra(UploadWhereaboutsPhotoService.EXTRAS_PHOTO_URI, photoUri);
			intent.putExtra(UploadWhereaboutsPhotoService.EXTRAS_MESSAGE_ID, id);
			startService(intent);
		}
	}

	private void broadcastNewMessageSent(CompactMessage newMessage) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.putExtra(EXTRAS_MESSAGE, newMessage);
		intent.setAction(ACTION_MESSAGE_SENT);
		broadcastManager.sendBroadcast(intent);
	}

}
