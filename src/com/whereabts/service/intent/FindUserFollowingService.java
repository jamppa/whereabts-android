package com.whereabts.service.intent;

import com.google.inject.Inject;
import com.whereabts.api.user.UserFollowingApi;
import com.whereabts.model.UserProfiles;

import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import roboguice.service.RoboIntentService;

public class FindUserFollowingService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindUserFollowingService";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	public static final String EXTRAS_RESULT_RECEIVER = TAG + ".resultReceiver;";

	@Inject
	private UserFollowingApi api;
	
	public FindUserFollowingService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String userId = intent.getStringExtra(EXTRAS_USER_ID);
		ResultReceiver receiver = intent.getParcelableExtra(EXTRAS_RESULT_RECEIVER);
		if(userId == null || receiver == null){
			return;
		}
		findUserFollowing(userId, receiver);
	}

	private void findUserFollowing(String userId, ResultReceiver receiver) {
		try{
			UserProfiles followingProfiles = api.findFollowingOfUser(userId);
			receiver.send(0, followingProfiles.asBundle());
		}
		catch(Exception e){
			e.printStackTrace();
			receiver.send(1, new Bundle());
		}
	}

}
