package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.api.facebook.ShareWhereaboutsApi;
import com.whereabts.model.newly.NewLocationMessage;

public class ShareWhereaboutsFacebookService extends RoboIntentService {

	private static final String TAG = "com.whereabts.service.intent.ShareWhereaboutsFacebookService";
	public static final String EXTRAS_MESSAGE = TAG +  ".message";

	@Inject
	private ShareWhereaboutsApi api;
	
	public ShareWhereaboutsFacebookService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NewLocationMessage message = (NewLocationMessage) intent.getSerializableExtra(EXTRAS_MESSAGE);
		if(message == null){
			return;
		}
		shareWhereaboutsToFacebook(message);
	}

	private void shareWhereaboutsToFacebook(NewLocationMessage message) {
		if(message.shareToFacebook()){
			api.shareWhereabouts(message);
		}
	}

}
