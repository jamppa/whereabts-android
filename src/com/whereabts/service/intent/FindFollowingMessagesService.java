package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;

import com.google.inject.Inject;
import com.whereabts.api.message.FindFollowingMessagesApi;
import com.whereabts.model.CompactMessages;

public class FindFollowingMessagesService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindFollowingMessagesService";
	public static final String EXTRAS_SKIP = TAG  + ".skip";
	public static final String EXTRAS_OLDER_THAN = TAG + ".olderThan";
	public static final String EXTRAS_RECEIVER = TAG + ".receiver";
	
	@Inject
	private FindFollowingMessagesApi api;
	
	public FindFollowingMessagesService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		int skip = intent.getIntExtra(EXTRAS_SKIP, 0);
		long olderThan = intent.getLongExtra(EXTRAS_OLDER_THAN, System.currentTimeMillis());
		ResultReceiver receiver = intent.getParcelableExtra(EXTRAS_RECEIVER);
		
		if(receiver == null){
			return;
		}
		
		if(skip >= 250){
			receiver.send(0, CompactMessages.empty().asBundle());
			return;
		}
		
		findFollowingMessages(skip, olderThan, receiver);
	}

	private void findFollowingMessages(int skip, long olderThan, ResultReceiver receiver) {
		try{
			CompactMessages messages = api.findFollowingMessagesOlderThan(skip, olderThan);
			receiver.send(0, messages.asBundle());
		}
		catch(Exception e){
			e.printStackTrace();
			receiver.send(1, new Bundle());
		}
	}

}
