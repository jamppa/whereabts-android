package com.whereabts.service.intent;

import roboguice.service.RoboIntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.whereabts.api.message.FindMessageApi;
import com.whereabts.model.Message;

public class FindMessageService extends RoboIntentService {

	public static final String TAG = "com.whereabts.service.intent.FindMessageService";
	public static final String EXTRAS_MESSAGE_ID = TAG + ".messageId";
	public static final String EXTRAS_RESULT_MESSAGE = TAG + ".message";
	public static final String EXTRAS_RESULT_FAILREASON = TAG + ".reason";
	
	public static final String ACTION_MESSAGE_FOUND = TAG + ".messageFound";
	public static final String ACTION_MESSAGE_FOUND_ERROR = TAG + ".messageFoundError";
	
	@Inject
	public FindMessageApi messageApi;
	
	public FindMessageService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String messageId = intent.getStringExtra(EXTRAS_MESSAGE_ID);
		if(messageId == null){
			return;
		}
		findMessage(messageId);
	}

	private void findMessage(String messageId) {
		try {
			Message message = messageApi.findMessageBy(messageId);
			broadcastMessageFound(message);
		}
		catch(Exception e) {
			e.printStackTrace();
			broadcastForError(e.getMessage());
		}
	}

	private void broadcastForError(String reason) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_MESSAGE_FOUND_ERROR);
		intent.putExtra(EXTRAS_RESULT_FAILREASON, reason);
		broadcastManager.sendBroadcast(intent);
	}

	private void broadcastMessageFound(Message message) {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
		Intent intent = new Intent();
		intent.setAction(ACTION_MESSAGE_FOUND);
		intent.putExtra(EXTRAS_RESULT_MESSAGE, message);
		broadcastManager.sendBroadcast(intent);
	}

}
