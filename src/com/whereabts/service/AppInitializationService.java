package com.whereabts.service;

import android.content.Context;
import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.model.User;
import com.whereabts.repo.GCMRepository;
import com.whereabts.service.intent.RequiredVersionService;
import com.whereabts.service.intent.UserRegistrationService;

public class AppInitializationService {
	
	@Inject
	private UserService userService;
	@Inject
	private GCMRepository gcmRepository;
	@Inject
	private Context context;
	
	public boolean isInitialized() {
		return userService.hasRegisteredUser() && 
				gcmRepository.hasValidRegistrationId();
	}

	public void registerUser(User user) {
		Intent intent = new Intent(context, UserRegistrationService.class);
		intent.putExtra(UserRegistrationService.EXTRAS_ANON_USER, user);
		context.startService(intent);
	}

	public boolean isUserInitialized() {
		return userService.hasRegisteredUser();
	}

	public boolean isGcmInitialized() {
		return gcmRepository.hasValidRegistrationId();
	}

	public void registerCurrentUser() {
		User currentUser = userService.findUser();
		if(!currentUser.isPublicUser()){
			registerUser(currentUser);
		}
	}
	
	public boolean isThisClientSupported(int requiredClientVersion){
		if(gcmRepository.currentAppVersion() < requiredClientVersion){
			return false;
		}
		return true;
	}

	public void checkRequiredClientVersion() {
		Intent intent = new Intent(context, RequiredVersionService.class);
		context.startService(intent);
	}

}
