package com.whereabts.service;

import android.location.Location;

public class LocationDetermineService {

	private static final int THIRTY_SECONDS = 1000 * 30;
	
	public boolean isBetterLocation(
			final Location location, final Location currentBestLocation) {
		
		if (currentBestLocation == null) {
	        return true;
	    }
		if(isSignificantlyNewerOrOlder(location, currentBestLocation)){
			return false;
		}
		return isMoreAccurate(location, currentBestLocation);
	}

	private boolean isMoreAccurate(
			final Location location, final Location currentBestLocation) {
		int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
		return accuracyDelta < 0;
	}

	private boolean isSignificantlyNewerOrOlder(
			final Location location, final Location currentBestLocation) {
		long timeDelta = location.getTime() - currentBestLocation.getTime();
		boolean isSignificantlyNewer = timeDelta > THIRTY_SECONDS;
		boolean isSignificantlyOlder = timeDelta < -THIRTY_SECONDS;
		return isSignificantlyOlder || isSignificantlyNewer;
	}

	
	
}
