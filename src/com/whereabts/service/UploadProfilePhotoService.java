package com.whereabts.service;

import roboguice.service.RoboService;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.view.View;

import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.ImageSize;
import com.whereabts.api.user.UserProfilePhotosApi;

public class UploadProfilePhotoService extends RoboService {

	public static final String EXTRAS_PHOTO_URI = "com.whereabts.service.UploadProfilePhotoService.photo";
	
	@Inject
	private UserProfilePhotosApi userProfilePhotosApi;
	
	private Looper looper;
	private UploadProfilePhotoHandler handler;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		HandlerThread thread = 
				new HandlerThread("ServiceStartArguments", Process.THREAD_PRIORITY_BACKGROUND);
		thread.start();
		looper = thread.getLooper();
		handler = new UploadProfilePhotoHandler(looper);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		looper = null;
		handler = null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		startHandler(intent.getExtras());
		return START_REDELIVER_INTENT;
	}

	private void startHandler(Bundle extras) {
		Message msg = handler.obtainMessage();
		msg.setData(extras);
		handler.sendMessage(msg);
	}

	@SuppressLint("HandlerLeak")
	private final class UploadProfilePhotoHandler extends Handler implements ImageLoadingListener {
		
		public UploadProfilePhotoHandler(Looper looper) {
			super(looper);
		}
		
		@Override
		public void handleMessage(Message msg) {
			if(hasProfilePhotoUri(msg)){
				Uri profilePhotoUri = msg.getData().getParcelable(EXTRAS_PHOTO_URI);
				ImageLoader imageLoader = ImageLoader.getInstance();
				imageLoader.loadImage(profilePhotoUri.toString(), new ImageSize(120, 120), displayOptions(), this);
			}
			else {
				stopSelf();
			}
		}

		private DisplayImageOptions displayOptions() {
			return new DisplayImageOptions.Builder()
				.imageScaleType(ImageScaleType.IN_SAMPLE_INT)
				.bitmapConfig(Config.ARGB_8888).build();
		}

		private boolean hasProfilePhotoUri(Message msg) {
			return msg.getData() != null && 
					msg.getData().getParcelable(EXTRAS_PHOTO_URI) != null;
		}

		@Override
		public void onLoadingStarted(String imageUri, View view) {}

		@Override
		public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
			stopSelf();
		}

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
			try{
				userProfilePhotosApi.saveUserProfilePhoto(loadedImage);
			}
			catch(Exception e){
				e.printStackTrace();
			}
			stopSelf();
		}

		@Override
		public void onLoadingCancelled(String imageUri, View view) {
			stopSelf();
		}
	}
	
}
