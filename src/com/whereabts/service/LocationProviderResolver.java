package com.whereabts.service;

import android.location.Location;
import android.location.LocationManager;

import com.google.inject.Inject;

public class LocationProviderResolver {

	@Inject
	private LocationManager locationManager;
	
	public Location getLastKnownLocationIfPossible() {
		Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
		if(location == null){
			location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
		}
		return location;
	}
	
	public boolean hasAccessToAnyProvider() {
		return (isGpsProviderEnabled() || isNetworkProviderEnabled());
	}
	
	public boolean isGpsProviderEnabled() {
		return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
	}
	
	public boolean isNetworkProviderEnabled() {
		return locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
	}
}
