package com.whereabts.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.inject.Inject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;

public class UserAccountService {

	@Inject
	private Context context;
	
	public List<Account> googleAccounts() {
		return Arrays.asList(accountManager().getAccountsByType("com.google"));
	}
	
	public List<String> googleAccountNames() {
		List<String> accountNames = new ArrayList<String>();
		for(Account each : googleAccounts()){
			accountNames.add(each.name);
		}
		return accountNames;
	}
	
	public boolean hasMultipleGoogleAccounts() {
		return googleAccounts().size() > 1;
	}
	
	public boolean hasGoogleAccount() {
		return googleAccounts().size() > 0;
	}

	private AccountManager accountManager() {
		return AccountManager.get(context);
	}

	public Account googleAccount() {
		if(hasGoogleAccount()){
			return googleAccounts().get(0);
		}
		return null;
	}
	
}
