package com.whereabts.service;

public class NotificationIds {
	public static final int SEND_FEEDBACK_FAILED_NOTIFICATION = 2;
	public static final int MESSAGE_REPLY_NOTIFICATION = 3;
	public static final int MESSAGE_LIKE_NOTIFICATION = 4;
	public static final int NEW_FOLLOWER_NOTIFICATION = 5;
}
