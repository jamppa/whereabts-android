package com.whereabts.service;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.util.NotificationUtils;
import com.whereabts.util.StringUtils;

public class MessageLikeNotificationService {

	public static final String TAG = "com.whereabts.service.MessageLikeNotificationService";
	
	@Inject
	private NotificationManager notificationManager;
	@Inject
	private Context context;
	@Inject
	private StringUtils stringUtils;
	
	public void notify(Bundle data) {
		NotificationCompat.Builder builder = buildNotification(data);
		buildBackStack(builder, data);
		notificationManager.notify(getMessageTag(data), NotificationIds.MESSAGE_LIKE_NOTIFICATION, builder.build());
	}

	private String getMessageTag(Bundle data) {
		return TAG + ".messageId." + data.getString("message-id", "");
	}

	private void buildBackStack(Builder builder, Bundle data) {
		NotificationUtils.buildBackStackForShowMessageActivity(
				builder, data.getString("message-id", ""), context);
	}

	private Builder buildNotification(Bundle data) {
		return NotificationUtils.buildNotification(
				context, getMessageTitle(data), stringUtils.getString(R.string.notific_message_liked_content));
	}

	private String getMessageTitle(Bundle data) {
		return data.getString("nick", "Someone");
	}
	
}
