package com.whereabts.service;

import java.util.Locale;

import roboguice.util.Strings;
import android.content.Context;
import android.telephony.TelephonyManager;

import com.google.inject.Inject;
import com.whereabts.model.User;
import com.whereabts.model.UserProfile;
import com.whereabts.repo.UserRepository;

public class UserService {

	@Inject
	private UserRepository userRepository;
	@Inject
	private Context context;
	
	public User findUser() {
		if(hasRegisteredUser()){
			return userRepository.getUser();
		}
		return User.newPublicUser();
	}

	public boolean hasRegisteredUser() {
		return userRepository.getUser().isWhole();
	}

	public void saveUser(User registeredUser) {
		userRepository.saveUser(registeredUser);
	}

	public void saveUserProfile(UserProfile profile) {
		userRepository.saveUserProfile(profile);
	}

	public String userCountry() {
		TelephonyManager telephone = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		return simCountryOrDefaultLocale(telephone);
	}

	private String simCountryOrDefaultLocale(TelephonyManager telephone) {
		String simCountry = telephone.getSimCountryIso();
		if(Strings.isEmpty(simCountry)){
			return Locale.getDefault().getCountry();
		}
		return simCountry;
	}
		
}
