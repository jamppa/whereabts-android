package com.whereabts.service;

import android.app.Activity;

import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.google.inject.Singleton;

@Singleton
public class FacebookSessionService implements StatusCallback {
	
	public static final int REQUEST_CODE_PERMISSIONS = 100;

	@Override
	public void call(Session session, SessionState state, Exception exception) {
		if(session.isOpened()){
			Session.setActiveSession(session);
		}
		else if(session.isClosed()){
			session.closeAndClearTokenInformation();
		}
	}
	
	public Session getActiveSession() {
		if(Session.getActiveSession() != null && Session.getActiveSession().isOpened()){
			return Session.getActiveSession();
		}
		return null;
	}
	
	public boolean hasActiveSession() {
		return getActiveSession() != null;
	}
	
	public void requestPublishPermissions(Activity activity){
		if(hasActiveSession()){
			getActiveSession().requestNewPublishPermissions(
					new Session.NewPermissionsRequest(activity, "publish_actions").setRequestCode(REQUEST_CODE_PERMISSIONS));
		}
	}
	
}
