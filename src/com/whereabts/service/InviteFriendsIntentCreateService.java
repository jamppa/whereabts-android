package com.whereabts.service;

import android.content.Intent;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.whereabts.R;
import com.whereabts.util.StringUtils;

@Singleton
public class InviteFriendsIntentCreateService {

	@Inject
	private StringUtils stringUtils;
	
	public Intent createIntent() {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_TEXT, inviteText());
		intent.putExtra(Intent.EXTRA_SUBJECT, inviteSubject());
		intent.setType("text/plain");
		return intent;
	}

	private String inviteSubject() {
		return stringUtils.getString(R.string.invite_to_whereabts_subject);
	}

	private String inviteText() {
		StringBuffer sb = new StringBuffer();
		sb.append(stringUtils.getString(R.string.invite_to_whereabts_prefix))
			.append(" - ")
			.append("http://play.google.com/store/apps/details?id=" + stringUtils.googlePlayAppName());
		return sb.toString();
	}
	
}
