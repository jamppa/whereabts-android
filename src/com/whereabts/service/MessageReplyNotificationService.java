package com.whereabts.service;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.google.inject.Inject;
import com.whereabts.util.NotificationUtils;
import com.whereabts.util.StringUtils;

public class MessageReplyNotificationService {

	public static final String TAG = "com.whereabts.service.MessageReplyNotificationService";
	
	@Inject
	private NotificationManager notificationManager;
	@Inject
	private Context context;
	@Inject
	private StringUtils stringUtils;
	
	public void notify(Bundle data) {
		NotificationCompat.Builder builder = buildNotification(data);
		buildBackStack(builder, data);
		notificationManager.notify(getMessageTag(data), NotificationIds.MESSAGE_REPLY_NOTIFICATION, builder.build());
	}

	private void buildBackStack(Builder builder, Bundle data) {
		NotificationUtils.buildBackStackForShowMessageActivity(
				builder, data.getString("message-id", ""), context);
	}

	private String getMessageTag(Bundle data) {
		return TAG + ".messageId." + data.getString("message-id", "");
	}

	private Builder buildNotification(Bundle data) {
		return NotificationUtils.buildNotification(
				context, getMessageTitle(data), getMessageText(data));
	}

	private String getMessageTitle(Bundle data) {
		return data.getString("reply-nick", "");
	}

	private String getMessageText(Bundle data) {
		return data.getString("reply-message", "");
	}
	
}
