package com.whereabts.service;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.util.NotificationUtils;
import com.whereabts.util.StringUtils;

public class FollowNotificationService {
	
	public static final String TAG = "com.whereabts.service.FollowNotificationService";
	
	@Inject
	private NotificationManager notificationManager;
	@Inject
	private Context context;
	@Inject
	private StringUtils stringUtils;
	
	public void notify(Bundle data){
		NotificationCompat.Builder builder = buildNotification(data);
		NotificationUtils.buildBackStackForShowUserProfileActivity(builder, data.getString("follower-id", ""), context);
		notificationManager.notify(getMessageTag(data), NotificationIds.NEW_FOLLOWER_NOTIFICATION, builder.build());
	}

	private String getMessageTag(Bundle data) {
		return TAG + ".userId." + data.getString("follower-id", "");
	}

	private Builder buildNotification(Bundle data) {
		return NotificationUtils.buildNotification(
				context, notificationTitle(), notificationContent(data));
	}

	private String notificationContent(Bundle data) {
		StringBuffer sb = new StringBuffer();
		sb.append(data.getString("follower-nick", "Someone"))
			.append(" ").append(stringUtils.getString(R.string.notific_new_follower_content));
		return sb.toString();
	}

	private String notificationTitle() {
		return stringUtils.getString(R.string.notific_new_follower_title);
	}

}
