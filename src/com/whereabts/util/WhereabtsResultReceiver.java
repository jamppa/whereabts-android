package com.whereabts.util;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class WhereabtsResultReceiver extends ResultReceiver {

	private Receiver receiver;
	
	public WhereabtsResultReceiver() {
		super(new Handler());
	}
	
	public WhereabtsResultReceiver(Handler handler) {
		super(handler);
	}
	
	public void setReceiver(Receiver receiver){
		this.receiver = receiver;
	}
	
	public interface Receiver {
		public void onReceiveResult(int actionCode, Bundle result);
	}
	
	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		if(receiver != null){
			receiver.onReceiveResult(resultCode, resultData);
		}
	}
}
