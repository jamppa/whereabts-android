package com.whereabts.util;

import android.os.Bundle;

import com.whereabts.R;
import com.whereabts.widget.dialog.ReplyMessageDialog;
import com.whereabts.widget.dialog.WhereabtsOneActionDialog;
import com.whereabts.widget.dialog.WhereabtsProgressDialog;
import com.whereabts.widget.dialog.WhereabtsTwoActionDialog;

public class DialogUtils {

	public static WhereabtsTwoActionDialog getEnableLocationServicesDialog(final StringUtils stringUtils) {
		Bundle args = new Bundle();
		args.putString(WhereabtsTwoActionDialog.MSG_ARG, stringUtils.getString(R.string.enable_loc_services_msg));
		args.putString(WhereabtsTwoActionDialog.TITLE_ARG, stringUtils.getString(R.string.enable_loc_services_title));
		args.putString(WhereabtsTwoActionDialog.OK_BTN_LABEL_ARG, stringUtils.getString(R.string.enable_loc_services_ok_label));
		args.putString(WhereabtsTwoActionDialog.CANCEL_BTN_LABEL_ARG, stringUtils.getString(R.string.enable_loc_services_cancel_label));
		WhereabtsTwoActionDialog enableLocationServicesDialog = new WhereabtsTwoActionDialog();
		enableLocationServicesDialog.setArguments(args);
		return enableLocationServicesDialog;
	}
	
	public static WhereabtsProgressDialog getProgressDialog(final int textRes, final StringUtils stringUtils){
		Bundle bundle = new Bundle();
		bundle.putString(WhereabtsProgressDialog.MSG_ARG, stringUtils.getString(textRes));
		WhereabtsProgressDialog progressDialog = new WhereabtsProgressDialog();
		progressDialog.setArguments(bundle);
		return progressDialog;
	}
	
	public static WhereabtsOneActionDialog getAppInitializationFailedDialog(final StringUtils stringUtils) {
		Bundle arguments = new Bundle();
		arguments.putString(
				WhereabtsOneActionDialog.BTN_LABEL_ARG, 
				stringUtils.getString(R.string.app_initialization_failed_dialog_button_label));
		arguments.putString(
				WhereabtsOneActionDialog.TITLE_ARG, 
				stringUtils.getString(R.string.app_initialization_failed_dialog_title));
		arguments.putString(
				WhereabtsOneActionDialog.MSG_ARG, 
				stringUtils.getString(R.string.app_initialization_failed_dialog_message));
		WhereabtsOneActionDialog dialog = new WhereabtsOneActionDialog();
		dialog.setArguments(arguments);
		return dialog;
	}

	public static WhereabtsTwoActionDialog getDiscardMessageDialog(final StringUtils stringUtils) {
		Bundle args = new Bundle();
		args.putString(WhereabtsTwoActionDialog.MSG_ARG, stringUtils.getString(R.string.discard_message_dialog_message));
		args.putString(WhereabtsTwoActionDialog.TITLE_ARG, stringUtils.getString(R.string.discard_message_dialog_title));
		args.putString(WhereabtsTwoActionDialog.OK_BTN_LABEL_ARG, stringUtils.getString(R.string.discard_message_dialog_ok_button));
		args.putString(WhereabtsTwoActionDialog.CANCEL_BTN_LABEL_ARG, stringUtils.getString(R.string.discard_message_dialog_cancel_button));
		WhereabtsTwoActionDialog dialog = new WhereabtsTwoActionDialog();
		dialog.setArguments(args);
		return dialog;
	}

	public static ReplyMessageDialog getReplyMessageDialog() {
		ReplyMessageDialog dialog = new ReplyMessageDialog();
		return dialog;
	}
	
}


