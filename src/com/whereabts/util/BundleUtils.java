package com.whereabts.util;

import java.io.Serializable;

import android.os.Bundle;
import android.os.Parcelable;

public class BundleUtils {

	public static String getString(Bundle savedInstanceState, String key, String def) {
		if(savedInstanceState != null){
			return savedInstanceState.getString(key, def);
		}
		return def;
	}

	public static Serializable getSerializable(Bundle savedInstanceState, String key, Serializable def) {
		if(savedInstanceState != null){
			return savedInstanceState.getSerializable(key);
		}
		return def;
	}

	public static boolean getBoolean(Bundle savedInstanceState, String key, boolean def) {
		if(savedInstanceState != null){
			return savedInstanceState.getBoolean(key);
		}
		return def;
	}
	
	public static Parcelable getParcelable(Bundle bundle, String key, Parcelable def){
		if(bundle != null){
			return bundle.getParcelable(key);
		}
		return def;
	}

	public static int getInt(Bundle bundle, String key, int def) {
		if(bundle != null){
			return bundle.getInt(key, def);
		}
		return def;
	}
	
}
