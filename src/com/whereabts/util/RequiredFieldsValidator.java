package com.whereabts.util;

import java.util.List;

import android.widget.EditText;

public class RequiredFieldsValidator {

	public static boolean areFilled(List<EditText> requiredFields){
		boolean isFilled = true;
		for(EditText field : requiredFields){
			if(field.getText().toString().isEmpty()) {
				isFilled = false;
				break;
			}
		}
		return isFilled;
	}
	
}
