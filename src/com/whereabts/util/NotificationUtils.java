package com.whereabts.util;

import java.util.UUID;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.support.v4.app.TaskStackBuilder;

import com.whereabts.R;
import com.whereabts.activity.ShowMessageActivity;
import com.whereabts.activity.ShowUserProfileActivity;

public class NotificationUtils {

	public static NotificationCompat.Builder buildNotification(Context ctx, String title, String msg) {
		return new NotificationCompat.Builder(ctx)
			.setAutoCancel(true)
			.setSmallIcon(R.drawable.ic_notification)
			.setContentTitle(title)
			.setContentText(msg)
			.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
			.setLights(0xff00d1, 500, 500)
			.setDefaults(0);
	}
	
	public static void buildBackStackForShowMessageActivity(Builder builder, String messageId, Context context) {
		Intent intent = new Intent(context, ShowMessageActivity.class);
		intent.putExtra(ShowMessageActivity.EXTRAS_MESSAGE_ID, messageId);
		intent.setAction(randomStringToDifferentiatePendingIntent());
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(ShowMessageActivity.class);
		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);
	}
	
	public static void buildBackStackForShowUserProfileActivity(Builder builder, String userId, Context context) {
		Intent intent = new Intent(context, ShowUserProfileActivity.class);
		intent.putExtra(ShowUserProfileActivity.EXTRAS_USER_ID, userId);
		intent.setAction(randomStringToDifferentiatePendingIntent());
		TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
		stackBuilder.addParentStack(ShowUserProfileActivity.class);
		stackBuilder.addNextIntent(intent);
		PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(pendingIntent);
	}

	private static String randomStringToDifferentiatePendingIntent() {
		return UUID.randomUUID().toString();
	}
	
}
