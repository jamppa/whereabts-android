package com.whereabts.util;

import android.location.Location;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.whereabts.model.CompactMessage;

public class MapUtils {

	public static void moveCameraTo(final GoogleMap map, final Location location) {
		map.animateCamera(
				CameraUpdateFactory.newLatLngZoom(
						new LatLng(location.getLatitude(), location.getLongitude()), 10));
	}
	
	public static void setCameraTo(final GoogleMap map, final Location location) {
		if(location != null){
			map.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(location.getLatitude(), location.getLongitude()), 8));
		}
	}
	
	public static void moveCameraToMessageLocation(GoogleMap map, CompactMessage message) {
		moveCameraToMessageLocation(map, message, null);
	}
	
	public static void moveCameraToMessageLocation(
			final GoogleMap map, final CompactMessage message, final GoogleMap.CancelableCallback callback) {
		map.animateCamera(CameraUpdateFactory.zoomOut(), 150, new GoogleMap.CancelableCallback() {
			@Override
			public void onFinish() {
				CameraPosition position = new CameraPosition.Builder()
				.target(new LatLng(message.location().latitude(), message.location().longitude()))
				.zoom(15)
				.build();
				map.animateCamera(CameraUpdateFactory.newCameraPosition(position), 500, callback);
			}
			
			@Override
			public void onCancel() {}
		});
	}
	
}
