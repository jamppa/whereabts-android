package com.whereabts.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;

import com.google.inject.Inject;

public class PhotoUtils {

	public static final String PHOTOS_SUFFIX = ".jpg";
	public static final String PHOTOS_PROFILE_PREFIX = "PROFILE_IMG_";
	public static final String PHOTOS_WHEREABOUTS_PREFIX = "WHEREABTS_IMG_";
	public static final String PHOTOS_ALBUM_NAME = "Whereabts";
	
	@Inject
	private Context context;
	
	private File photoStorageDir() {
		return new File(Environment.getExternalStoragePublicDirectory(
				Environment.DIRECTORY_PICTURES), PHOTOS_ALBUM_NAME);
	}
	
	private File photosDirectory() {
		File storageDir = photoStorageDir();
		if(storageDir != null){
			if(!storageDir.mkdirs()){
				if(!storageDir.exists()){
					return null;
				}
			}
		}
		return storageDir;
	}
	
	public File setupProfilePhotoFile() throws IOException {
		File photosDir = photosDirectory();
		String profilePhotoName = profilePhotoName();
		return File.createTempFile(profilePhotoName, PHOTOS_SUFFIX, photosDir);
	}
	
	public Uri setupWhereaboutsMessagePhotoFile() throws IOException {
		File photosDir = photosDirectory();
		String profilePhotoName = whereaboutsPhotoName();
		return Uri.fromFile(File.createTempFile(profilePhotoName, PHOTOS_SUFFIX, photosDir));
	}
			
	public void addPhotoToGallery(Uri photoUri){
		Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
		mediaScanIntent.setData(photoUri);
		context.sendBroadcast(mediaScanIntent);
	}
	
	private String profilePhotoName() {
		String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		return PHOTOS_PROFILE_PREFIX + timestamp;
	}
	
	private String whereaboutsPhotoName() {
		String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
		return PHOTOS_WHEREABOUTS_PREFIX + timestamp;
	}
	
}
