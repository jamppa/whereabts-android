package com.whereabts.util.time;

import java.util.Date;

import android.content.Context;
import android.text.format.DateFormat;

public class TimestampFormatter {

	public static String formatTimestamp(Context context, long time){
		java.text.DateFormat df = DateFormat.getDateFormat(context);
		java.text.DateFormat tf = DateFormat.getTimeFormat(context);
		return new StringBuffer()
			.append(df.format(new Date(time)))
			.append(" ")
			.append(tf.format(new Date(time))).toString();
	}
	
}
