package com.whereabts.util.time;

import org.ocpsoft.prettytime.TimeUnit;

public class Day implements TimeUnit {
	
	@Override
	public long getMaxQuantity() {
		return 0;
	}

	@Override
	public long getMillisPerUnit() {
		return 1000L * 60L * 60L * 24L;
	}

}
