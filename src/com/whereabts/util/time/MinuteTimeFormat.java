package com.whereabts.util.time;

import org.ocpsoft.prettytime.Duration;
import org.ocpsoft.prettytime.TimeFormat;

public class MinuteTimeFormat implements TimeFormat {

	@Override
	public String decorate(Duration duration, String time) {
		return time;
	}

	@Override
	public String decorateUnrounded(Duration duration, String time) {
		return decorate(duration, time);
	}

	@Override
	public String format(Duration duration) {
		if(duration.getQuantity() == 1){
			return duration.getQuantity() + " minute";
		}
		return duration.getQuantity() + " minutes";
	}

	@Override
	public String formatUnrounded(Duration duration) {
		return format(duration);
	}

}
