package com.whereabts.util;

import android.app.Application;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.whereabts.api.ApiSettings;

@Singleton
public class StringUtils {

	@Inject
	private Application context;
	@Inject
	private ApiSettings apiSettings;
	
	public String getString(final int resId){
		return context.getResources().getText(resId).toString();
	}
	
	public String googlePlayAppName() {
			return context.getPackageName();
	}
	
	public String photosApiRoot() {
		return apiSettings.photosApiRoot();
	}
	
}
