package com.whereabts.util;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessageAndMarker;
import com.whereabts.model.CompactMessageAndMarkerOptions;
import com.whereabts.model.CompactMessages;
import com.whereabts.model.CompactMessagesAndMarkers;

public class MapMarkerDrawer {
	
	public CompactMessagesAndMarkers drawCompactMessages(final GoogleMap map, final CompactMessages messages) {
		List<CompactMessageAndMarkerOptions> messagesAndMarkerOptions = messagesToMarkerOptions(messages);
		return drawMarkersOnTheMap(map, messagesAndMarkerOptions);
	}
	
	public CompactMessageAndMarker drawCompactMessage(final GoogleMap map, final CompactMessage message){
		MarkerOptions markerOptions = toMarkerOptions(message);
		return new CompactMessageAndMarker(message, map.addMarker(markerOptions));
	}

	private CompactMessagesAndMarkers drawMarkersOnTheMap(GoogleMap map, List<CompactMessageAndMarkerOptions> messagesAndMarkerOptions) {
		CompactMessagesAndMarkers messagesAndMarkers = new CompactMessagesAndMarkers();
		for(CompactMessageAndMarkerOptions messageAndMarkerOptions : messagesAndMarkerOptions){
			messagesAndMarkers.add(
					new CompactMessageAndMarker(
							messageAndMarkerOptions.compactMessage(), map.addMarker(messageAndMarkerOptions.markerOptions())));
		}
		return messagesAndMarkers;
	}

	private List<CompactMessageAndMarkerOptions> messagesToMarkerOptions(CompactMessages messages) {
		List<CompactMessageAndMarkerOptions> messagesAndMarkerOptions = new ArrayList<CompactMessageAndMarkerOptions>();
		for(CompactMessage message : messages.get()){
			MarkerOptions markerOptions = toMarkerOptions(message);
			messagesAndMarkerOptions.add(new CompactMessageAndMarkerOptions(message, markerOptions));
		}
		return messagesAndMarkerOptions;
	}

	public MarkerOptions toMarkerOptions(CompactMessage message) {
		return new MarkerOptions()
			.position(message.locationAsLatLng())
			.title(message.shortMessage())
			.icon(BitmapDescriptorFactory.fromResource(message.category().resourceId()))
			.snippet(message.timestampAsPretty())
			.flat(true);
	}

	private void clearMarkersFromMap(List<Marker> markers) {
		for(Marker each : markers){
			each.remove();
		}
	}

	public void clearMarkersFromMap(
			CompactMessagesAndMarkers currentMarkersAndMessages,
			CompactMessagesAndMarkers newMarkersAndMessages) {
		for(CompactMessageAndMarker each : currentMarkersAndMessages){
			if(each.marker().isInfoWindowShown()){
				if(!showInfoWindowOnNewMarkerIfExists(newMarkersAndMessages, each)){
					currentMarkersAndMessages.remove(each);
				}
			}
		}
		clearMarkersFromMap(currentMarkersAndMessages.getMarkers());
	}

	private boolean showInfoWindowOnNewMarkerIfExists(
			CompactMessagesAndMarkers newMarkersAndMessages, CompactMessageAndMarker each) {
		Marker marker = newMarkersAndMessages.getMarkerOfMessage(each.compactMessage());
		if(marker != null){
			marker.showInfoWindow();
			return true;
		}
		newMarkersAndMessages.add(each);
		each.marker().showInfoWindow();
		return false;
	}

	public void closeOpenMarkers(List<Marker> markers) {
		for(Marker each : markers){
			if(each.isInfoWindowShown()){
				each.hideInfoWindow();
			}
		}
	}

}
