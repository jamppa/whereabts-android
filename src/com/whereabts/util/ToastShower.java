package com.whereabts.util;

import static android.widget.Toast.makeText;
import android.app.Application;
import android.widget.Toast;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.whereabts.R;

@Singleton
public class ToastShower {

	@Inject
	private Application context;
	
	public void showFollowingToast() {
		makeText(context, R.string.toast_following, Toast.LENGTH_SHORT).show();
	}
	
	public void showUnfollowingToast() {
		makeText(context, R.string.toast_not_following, Toast.LENGTH_SHORT).show();
	}
	
	public void showLoadingUserFollowersFailed() {
		makeText(context, R.string.toast_loading_followers_failed, Toast.LENGTH_SHORT).show();
	}
	
	public void showWhereaboutsPhotoFileFailed() {
		makeText(context, R.string.toast_whereabouts_photo_failed, Toast.LENGTH_SHORT).show();
	}

	public void showCameraNotSupported() {
		makeText(context, R.string.choose_photo_action_camera_not_supported, Toast.LENGTH_SHORT).show();
	}
	
}
