package com.whereabts.controller;

import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.activity.PeopleActivity;
import com.whereabts.model.UserProfiles;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

public class PeopleController implements Receiver {

	@Inject
	private WhereabtsResultReceiver recentUsersReceiver;
	@Inject
	private WhereabtsResultReceiver searchUsersReceiver;
	@Inject
	private ServiceLauncher serviceLauncher;
	
	private PeopleActivity activity = null;
	private UserProfiles currentProfiles = null;
	private boolean isSearch = false;
	
	public void onCreate(PeopleActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		this.currentProfiles = (UserProfiles) BundleUtils.getSerializable(savedInstanceState, "currentProfiles", currentProfiles);
		this.isSearch = BundleUtils.getBoolean(savedInstanceState, "isSearch", isSearch);
	}

	public void onResume() {
		recentUsersReceiver.setReceiver(this);
		searchUsersReceiver.setReceiver(this);
	}
	
	public void onCreateOptionsMenu() {
		loadOrShowUserProfiles();
	}
	
	public void onStop() {
		recentUsersReceiver.setReceiver(null);
		searchUsersReceiver.setReceiver(null);
	}

	private void loadOrShowUserProfiles() {
		if(currentProfiles != null){
			showUserProfiles(currentProfiles);
			return;
		}
		loadRecentUserProfiles();
	}
	
	private void showUserProfiles(UserProfiles userProfiles) {
		activity.showUserProfiles(userProfiles, isSearch);
		activity.showMenuItems();
	}

	private void loadRecentUserProfiles() {
		serviceLauncher.launchFindRecentUserProfilesService(activity, recentUsersReceiver);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		if(actionCode == 0){
			this.currentProfiles = (UserProfiles) result.getSerializable(UserProfiles.BUNDLE_KEY);
			showUserProfiles(currentProfiles);
		}
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("currentProfiles", currentProfiles);
		outState.putBoolean("isSearch", isSearch);
	}

	public void onSearch(String search) {
		isSearch = true;
		activity.showLoader();
		serviceLauncher.launchSearchUserProfiles(activity, search, searchUsersReceiver);
	}

	public void onDestroy() {
		this.activity = null;
		this.currentProfiles = null;
	}

}
