package com.whereabts.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.inject.Inject;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.activity.ShowUserProfileActivity;
import com.whereabts.model.UserWithProfile;
import com.whereabts.service.intent.FindUserProfileService;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;

public class ShowUserProfileController {

	@Inject
	private ServiceLauncher serviceLauncher;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private Context context;
	
	private ShowUserProfileActivity activity = null;
	private UserWithProfile profile = null;
	private LocalBroadcastManager broadcastManager = null;
	
	private BroadcastReceiver userProfileReceiver = initUserProfileReceiver();
	
	public void onCreate(ShowUserProfileActivity showUserProfileActivity, Bundle savedInstanceState) {
		this.broadcastManager = LocalBroadcastManager.getInstance(context);
		this.activity = showUserProfileActivity;
		this.profile = (UserWithProfile) BundleUtils.getSerializable(savedInstanceState, "profile", profile);
	}

	private BroadcastReceiver initUserProfileReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(FindUserProfileService.ACTION_USER_PROFILE_FOUND)) {
					if(activity != null){
						userProfileLoaded((UserWithProfile)intent.getSerializableExtra(FindUserProfileService.EXTRAS_RESULT_USER_PROFILE));
					}
				}
			}
		};
	}

	public void onResume() {
		registerUserProfileReceiver();
		showOrLoadProfile();
	}
	
	public void onStop() {
		broadcastManager.unregisterReceiver(userProfileReceiver);
	}
	
	public void onDestroy() {
		this.activity = null;
		this.profile = null;
		this.broadcastManager = null;
	}
	
	private void registerUserProfileReceiver() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(FindUserProfileService.ACTION_USER_PROFILE_FIND_ERROR);
		intentFilter.addAction(FindUserProfileService.ACTION_USER_PROFILE_FOUND);
		broadcastManager.registerReceiver(userProfileReceiver, intentFilter);
	}
	
	private void showOrLoadProfile() {
		if(hasProfile()){
			userProfileLoaded(profile);
			return;
		}
		loadProfile(activity.getUserId());
	}

	private void loadProfile(String userId) {
		activity.showProfileLoader();
		serviceLauncher.launchFindUserProfileService(activity, userId);
	}

	private boolean hasProfile() {
		return profile != null;
	}

	private void userProfileLoaded(UserWithProfile loadedProfile) {
		this.profile = loadedProfile;
		if(profile.isOwnProfile()){
			activity.changeTitleToMe();
		}
		activity.fillProfile(profile);
		activity.showMenuItems(profile);
		activity.showProfile();
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("profile", profile);
	}

	public void followUser(boolean isFollowing) {
		profile.isFollowing(isFollowing);
		serviceLauncher.launchFollowUserService(activity, profile);
	}

	public void onFollowersClicked() {
		if(profile.hasFollowers()){
			activityLauncher.launchUserFollowersActivity(profile.user().id());
		}
	}

	public void onFollowingClicked() {
		if(profile.hasFollowings()){
			activityLauncher.launchUserFollowingActivity(profile.user().id());
		}
	}

	public void onWhereaboutsClicked() {
		if(profile.hasWhereabouts()){
			activityLauncher.launchUserWhereaboutsActivity(profile.user().id());
		}
	}

}
