package com.whereabts.controller;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.Marker;
import com.google.inject.Inject;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.activity.UserStreamActivity;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessageAndMarker;
import com.whereabts.util.MapMarkerDrawer;
import com.whereabts.util.MapUtils;

public class UserStreamController implements OnInfoWindowClickListener {

	@Inject
	private MapMarkerDrawer mapMarkerDrawer;
	@Inject
	private ActivityLauncher activityLauncher;
	
	private UserStreamActivity activity;
	private CompactMessageAndMarker selectedMessage;
	
	public void onCreate(UserStreamActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
	}
	
	public void onMessageSelected(CompactMessage message) {
		moveMapToMessageLocation(message);
	}
	
	private void removePreviousMessage(CompactMessageAndMarker message) {
		if(message != null){
			message.remove();
		}
	}

	private CompactMessageAndMarker drawMessageToMap(CompactMessage message) {
		GoogleMap map = activity.getMap();
		CompactMessageAndMarker messageAndMarker = mapMarkerDrawer.drawCompactMessage(map, message);
		messageAndMarker.showInfoWindow();
		return messageAndMarker;
	}

	private void moveMapToMessageLocation(final CompactMessage message) {
		GoogleMap map = activity.getMap();
		MapUtils.moveCameraToMessageLocation(map, message, new GoogleMap.CancelableCallback() {
			@Override
			public void onFinish() {
				CompactMessageAndMarker previous = selectedMessage;
				selectedMessage = drawMessageToMap(message);
				removePreviousMessage(previous);
			}
			@Override
			public void onCancel() {}
		});
	}

	@Override
	public void onInfoWindowClick(Marker marker) {
		if(selectedMessage != null && selectedMessage.marker().equals(marker)){
			activityLauncher.launchShowMessageActivity(selectedMessage.compactMessage().id(), activity);
		}
	}

	public CompactMessage selectedMessage() {
		if(selectedMessage != null){
			return selectedMessage.compactMessage();
		}
		return null;
	}

	public void onDestroy() {
		if(selectedMessage != null){
			selectedMessage.remove();
		}
		selectedMessage = null;
		activity = null;
	}

}
