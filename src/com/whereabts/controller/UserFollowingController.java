package com.whereabts.controller;

import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.activity.UserFollowingActivity;
import com.whereabts.model.UserProfiles;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

public class UserFollowingController implements Receiver {

	private UserFollowingActivity activity = null;
	private UserProfiles followingProfiles = null;
	
	@Inject
	private WhereabtsResultReceiver userFollowingReceiver;
	@Inject
	private ServiceLauncher serviceLauncher;
	
	public void onCreate(UserFollowingActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		this.followingProfiles = (UserProfiles) BundleUtils.getSerializable(savedInstanceState, "followingProfiles", followingProfiles);
	}

	public void onResume() {
		userFollowingReceiver.setReceiver(this);
		loadOrShowFollowingProfiles();
	}

	private void loadOrShowFollowingProfiles() {
		if(followingProfiles != null){
			activity.showFollowingProfiles(followingProfiles);
			return;
		}
		loadFollowingProfiles();
	}

	private void loadFollowingProfiles() {
		serviceLauncher.launchfinduserFollowingService(
				activity, activity.getUserId(), userFollowingReceiver);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("followingProfiles", followingProfiles);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		if(actionCode == 0){
			this.followingProfiles = (UserProfiles) result.getSerializable(UserProfiles.BUNDLE_KEY);
			activity.showFollowingProfiles(followingProfiles);
		}
	}

	public void onDestroy() {
		this.activity = null;
		this.followingProfiles = null;
	}

	public void onPause() {
		userFollowingReceiver.setReceiver(null);
	}
	
}
