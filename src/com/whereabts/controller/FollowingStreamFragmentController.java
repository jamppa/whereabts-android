package com.whereabts.controller;

import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.fragment.FollowingStreamFragment;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

public class FollowingStreamFragmentController implements Receiver {

	@Inject
	private ServiceLauncher serviceLauncher;
	@Inject
	private WhereabtsResultReceiver followingStreamReceiver;
	
	private CompactMessages currentMessages;
	private CompactMessage selectedMessage;
	private int selectedMessagesPosition;
	private FollowingStreamFragment fragment;
	
	public void onCreate(FollowingStreamFragment fragment, Bundle savedInstanceState) {
		this.fragment = fragment;
		this.currentMessages = (CompactMessages) BundleUtils.getSerializable(savedInstanceState, "currentMessages", currentMessages);
		this.selectedMessage = (CompactMessage) BundleUtils.getSerializable(savedInstanceState, "selectedMessage", selectedMessage);
		this.selectedMessagesPosition = BundleUtils.getInt(savedInstanceState, "selectedMessagePosition", -1);
	}

	public void onResume() {
		followingStreamReceiver.setReceiver(this);
		loadFollowingMessages();
	}
	
	public void onPause() {
		followingStreamReceiver.setReceiver(null);
	}
	
	private void loadFollowingMessages() {
		if(currentMessages != null){
			showMessages(currentMessages);
			return;
		}
		serviceLauncher
			.launchFindFollowingMessagesService(fragment, 0, followingStreamReceiver);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		if(actionCode == 0){
			messagesLoaded((CompactMessages) result.getSerializable(CompactMessages.BUNDLE_KEY));
		}
	}

	private void messagesLoaded(CompactMessages messages) {
		if(currentMessages == null){
			currentMessages = messages;
			showMessages(messages);
		}
		else{
			refreshMessages(messages);
		}
	}

	private void showMessages(CompactMessages messages) {
		if(!messages.isEmpty()){
			fragment.showMessagesList(currentMessages);
			fragment.setMessagesListPosition(selectedMessagesPosition);
			notifyFragmentListener(currentMessages);
		}
		else{
			fragment.showNoMessagesText();
		}
	}

	private void refreshMessages(CompactMessages messages) {
		currentMessages.addAll(messages);
		fragment.refreshMessagesList(messages);
	}

	private void notifyFragmentListener(CompactMessages messages) {
		if(selectedMessage != null){
			fragment.notifyListenerSelectedMessage(selectedMessage);
			return;
		}
		fragment.notifyListenerMessagesLoaded(messages.first());
	}

	public void onPullUpToRefresh() {
		long olderThan = currentMessages.first().createdAt();
		serviceLauncher.launchFindFollowingMessagesService(
				fragment, currentMessages.size(), olderThan, followingStreamReceiver);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("currentMessages", currentMessages);
		outState.putSerializable("selectedMessage", selectedMessage);
		outState.putInt("selectedMessagePosition", selectedMessagesPosition);
	}

	public void onMessageSelected(CompactMessage message, int position) {
		this.selectedMessage = message;
		this.selectedMessagesPosition = position;
		fragment.notifyListenerSelectedMessage(selectedMessage);
	}

}
