package com.whereabts.controller;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.MapActivity;
import com.whereabts.activity.ShowMessageActivity;
import com.whereabts.model.BoundingBox;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;
import com.whereabts.model.CompactMessagesAndMarkers;
import com.whereabts.model.Message;
import com.whereabts.service.LocationProviderResolver;
import com.whereabts.service.UserService;
import com.whereabts.service.intent.FindMessagesService;
import com.whereabts.service.intent.SendMessageService;
import com.whereabts.util.MapMarkerDrawer;
import com.whereabts.util.MapUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

public class MapActivityController implements 
	OnMyLocationChangeListener, OnCameraChangeListener, Receiver, OnInfoWindowClickListener {

	private static final int NUM_OF_CAMERA_CHANGES_TILL_MAP_POSITIONED = 2;
	private static final String ENABLE_LOC_SERVICES_DIALOG_VISIBLE = "enableLocServicesDialogVisible";
	private static final String MAP_POINTED_TO_CURRENT_LOC = "mapPointedToCurrentLoc";
	private static final String CURRENT_LOCATION = "currentLocation";
	
	@Inject
	private LocationProviderResolver locationProviderResolver;
	@Inject
	private WhereabtsResultReceiver resultReceiver;
	@Inject
	private MapMarkerDrawer mapMarkerDrawer;
	@Inject
	private CompactMessagesAndMarkers currentMarkersAndMessages;
	@Inject
	private UserService userService;
	
	private MapActivity activity;
	private Location currentLocation;
	private boolean mapPositioned = false;
	private boolean enableLocationServicesDialogVisible = false;
	private int numOfCameraChanges = 0;
	
	private BroadcastReceiver messageSentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			CompactMessage message = (CompactMessage) intent.getSerializableExtra(SendMessageService.EXTRAS_MESSAGE);
			currentMarkersAndMessages.add(mapMarkerDrawer.drawCompactMessage(activity.getMap(), message));
		}
	};
		
	public void initView(MapActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		enableLocationServicesDialogVisible = isEnableLocationServicesDialogVisible(savedInstanceState);
		mapPositioned = isMapPointedToCurrentLocation(savedInstanceState);
		currentLocation = currentLocation(savedInstanceState);
		moveCameraToLastKnownLocation();
		showEnableLocationServicesDialogIfNecessary(isLocationServicesAvailable());
	}

	private void moveCameraToLastKnownLocation() {
		if(!mapPositioned){
			MapUtils.setCameraTo(
					activity.getMap(), locationProviderResolver.getLastKnownLocationIfPossible());
		}
	}

	private Location currentLocation(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getParcelable(CURRENT_LOCATION);
		}
		return null;
	}

	private boolean isMapPointedToCurrentLocation(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getBoolean(MAP_POINTED_TO_CURRENT_LOC, false);
		}
		return false;
	}

	private boolean isEnableLocationServicesDialogVisible(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getBoolean(ENABLE_LOC_SERVICES_DIALOG_VISIBLE, false);
		}
		return false;
	}

	private void showEnableLocationServicesDialogIfNecessary(boolean isLocationServicesAvailable) {
		if(!isLocationServicesAvailable){
			showEnableLocationServicesDialog();
		}
	}

	private void showEnableLocationServicesDialog() {
		if(enableLocationServicesDialogVisible){
			return;
		}
		enableLocationServicesDialogVisible = true;
		activity.showEnableLocationServicesDialog();
	}

	public boolean isLocationServicesAvailable() {
		return locationProviderResolver.hasAccessToAnyProvider();
	}

	public void onMenuNewMessageButtonClicked() {
		launchNewMessageActivityIfCurrentLocationAvailable();
	}

	private void launchNewMessageActivityIfCurrentLocationAvailable() {
		if(currentLocation != null){
			activity.launchNewMessageActivityWithLocation(currentLocation);
		} else {
			activity.showToastWithMessage(R.string.no_current_location);
		}
	}

	@Override
	public void onMyLocationChange(Location location) {
		if(!mapPositioned){
			mapPositioned = true;
			activity.moveCameraToLocation(location);
		}
		this.currentLocation = location;
	}

	public void saveViewState(Bundle outState) {
		outState.putBoolean(ENABLE_LOC_SERVICES_DIALOG_VISIBLE, enableLocationServicesDialogVisible);
		outState.putBoolean(MAP_POINTED_TO_CURRENT_LOC, mapPositioned);
		outState.putParcelable(CURRENT_LOCATION, currentLocation);
	}

	public void listenForCameraActions(final GoogleMap map) {
		resultReceiver.setReceiver(this);
		map.setOnMyLocationChangeListener(this);
		map.setOnCameraChangeListener(this);
		map.setOnInfoWindowClickListener(this);
	}
	
	public void stopListeningForCameraActions(final GoogleMap map) {
		resultReceiver.setReceiver(null);
		map.setOnMyLocationChangeListener(null);
		map.setOnCameraChangeListener(null);
		map.setOnInfoWindowClickListener(null);
	}

	@Override
	public void onCameraChange(CameraPosition pos) {
		checkNumOfCameraChanges();
		activity.showActionbarSpinner();
		VisibleRegion region = activity.getMap().getProjection().getVisibleRegion();
		fetchMessagesForMap(new BoundingBox(region.latLngBounds));
	}

	private void checkNumOfCameraChanges() {
		if(numOfCameraChanges >= NUM_OF_CAMERA_CHANGES_TILL_MAP_POSITIONED){
			mapPositioned = true;
			return;
		}
		else{
			numOfCameraChanges++;
		}
	}

	private void fetchMessagesForMap(BoundingBox bbox) {
		Intent intent = new Intent(activity, FindMessagesService.class);
		activity.stopService(intent);
		intent.putExtra(FindMessagesService.EXTRAS_BOUNDING_BOX, bbox);
		intent.putExtra(FindMessagesService.EXTRAS_RESULT_RECEIVER, resultReceiver);
		activity.startService(intent);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		activity.hideActionbarSpinner();
		if(actionCode == FindMessagesService.RESULT_FOUND_MESSAGES){
			CompactMessages messages = (CompactMessages) result.getSerializable(FindMessagesService.EXTRAS_RESULT_MESSAGES);
			CompactMessagesAndMarkers newMarkersAndMessages = mapMarkerDrawer.drawCompactMessages(activity.getMap(), messages);
			mapMarkerDrawer.clearMarkersFromMap(currentMarkersAndMessages, newMarkersAndMessages);
			currentMarkersAndMessages.clearAndAddAll(newMarkersAndMessages);
		}
		else if(actionCode == FindMessagesService.RESULT_ERROR){
			String reason = result.getString(FindMessagesService.EXTRAS_RESULT_FAILREASON);
			activity.showToastForError(reason);
		}
	}
	
	@Override
	public void onInfoWindowClick(Marker marker) {
		if(currentMarkersAndMessages != null){
			activity.launchShowMessageActivity(currentMarkersAndMessages.get(marker));
		}
	}

	public void listenForSentMessages() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SendMessageService.ACTION_MESSAGE_SENT);
		broadcastManager.registerReceiver(messageSentReceiver, intentFilter);
	}

	public void stopListeningForSentMessages() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		broadcastManager.unregisterReceiver(messageSentReceiver);
	}

	public void onEnableLocationServicesDialogOkClicked() {
		enableLocationServicesDialogVisible = false;
		activity.showLocationSettings();
	}

	public void onEnableLocationServicesDialogCancelClicked() {
		enableLocationServicesDialogVisible = false;
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == ShowMessageActivity.REQUEST_MESSAGE_DELETED){
			if(resultCode == Activity.RESULT_OK){
				Message deletedMessage = (Message) data.getSerializableExtra(ShowMessageActivity.EXTRAS_MESSAGE);
				removeDeletedMessageAndMarkerFromMap(deletedMessage);
			}
		}
	}

	private void removeDeletedMessageAndMarkerFromMap(Message deletedMessage) {
		Marker marker = currentMarkersAndMessages.getMarkerOfMessage(deletedMessage.asCompact());
		if(marker != null){
			marker.remove();
		}
	}

	public void clearMap() {
		if(currentMarkersAndMessages != null){
			currentMarkersAndMessages.clearAll();
		}
	}


}
