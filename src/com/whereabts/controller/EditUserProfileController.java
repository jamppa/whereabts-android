package com.whereabts.controller;

import java.io.File;
import java.io.IOException;

import roboguice.util.Strings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.EditUserProfileActivity;
import com.whereabts.api.user.UserProfilePhotosApi;
import com.whereabts.model.Country;
import com.whereabts.model.UserProfile;
import com.whereabts.model.newly.NewProfileDetails;
import com.whereabts.service.UploadProfilePhotoService;
import com.whereabts.service.intent.FindCurrentUserProfileService;
import com.whereabts.service.intent.SaveUserProfileService;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.PhotoUtils;
import com.whereabts.util.RequiredFieldsValidator;
import com.whereabts.util.StringUtils;

public class EditUserProfileController implements TextWatcher {
	
	private EditUserProfileActivity activity;
	
	@Inject
	private PhotoUtils photoUtils;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private Context context;
	@Inject
	private UserProfilePhotosApi profilePhotosApi;
	
	private Uri newProfilePhotoUri = null;
	private Bitmap newProfilePhotoBitmap = null;
	private UserProfile currentProfile = null;
	private LocalBroadcastManager broadcastManager = null;
	private boolean saveMenuItemEnabled = false;
	private boolean savingProfileDetails = false;
	
	private BroadcastReceiver userProfileFoundReceiver = initUserProfileFoundReceiver();
	private BroadcastReceiver userProfileSaveReceiver = initUserProfileSaveReceiver();
	
	private BroadcastReceiver initUserProfileSaveReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(SaveUserProfileService.ACTION_USER_PROFILE_SAVED)){
					if(activity != null){
						userProfileSaved();
					}
				}
				else if(intent.getAction().equals(SaveUserProfileService.ACTION_USER_PROFILE_SAVE_FAILED)){
					if(activity != null){
						userProfileSaveFailed();
					}
				}
			}
		};
	}

	private BroadcastReceiver initUserProfileFoundReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(FindCurrentUserProfileService.ACTION_CURRENT_USER_PROFILE_FOUND)){
					if(activity != null){
						userProfileLoaded((UserProfile) intent.getSerializableExtra(FindCurrentUserProfileService.EXTRAS_RESULT_USER_PROFILE));
					}
				}
				else if(intent.getAction().equals(FindCurrentUserProfileService.ACTION_CURRENT_USER_PROFILE_FIND_ERROR)){
					if(activity != null){
						userProfileLoadError(intent.getStringExtra(FindCurrentUserProfileService.EXTRAS_RESULT_ERROR));
					}
				}
			}
		};
	}

	public void onCreate(EditUserProfileActivity editUserProfileActivity, Bundle savedInstanceState) {
		activity = editUserProfileActivity;
		broadcastManager = LocalBroadcastManager.getInstance(context);
		currentProfile = (UserProfile) BundleUtils.getSerializable(savedInstanceState, "currentProfile", currentProfile);
		saveMenuItemEnabled = BundleUtils.getBoolean(savedInstanceState, "saveMenuItemEnabled", false);
		savingProfileDetails = BundleUtils.getBoolean(savedInstanceState, "savingProfileDetails", false);
		newProfilePhotoUri = (Uri) BundleUtils.getParcelable(savedInstanceState, "newProfilePhotoUri", newProfilePhotoUri);
		newProfilePhotoBitmap = (Bitmap) BundleUtils.getParcelable(savedInstanceState, "newProfilePhotoBitmap", newProfilePhotoBitmap);
	}

	private void showOrLoadUserProfile() {
		if(hasCurrentProfile() && !savingProfileDetails){
			activity.showUserProfile();
			activity.enableSaveProfileMenuItem(saveMenuItemEnabled);
			showProfilePhoto();
			return;
		}
		loadingUserProfile();
	}

	private void showProfilePhoto() {
		if(newProfilePhotoBitmap != null){
			activity.showProfilePhoto(newProfilePhotoBitmap);
			return;
		}
		activity.showProfilePhoto(currentProfile);
	}

	private void loadingUserProfile() {
		activity.showUserProfileLoader();
		if(!savingProfileDetails){
			Intent intent = new Intent(activity.getApplicationContext(), FindCurrentUserProfileService.class);
			activity.startService(intent);
		}
	}

	private boolean hasCurrentProfile() {
		return currentProfile != null;
	}

	public void onCreateOptionsMenu() {
		initFields();
		showOrLoadUserProfile();
	}
	
	public void onResume() {
		listenForFoundUserProfile();
		listenForSavedProfile();
	}
	
	public void onDestroy() {
		this.newProfilePhotoUri = null;
		this.currentProfile = null;
		this.newProfilePhotoBitmap = null;
		this.activity = null;
		this.broadcastManager = null;
	}

	private void initFields() {
		for(EditText each : activity.requiredFields()){
			each.addTextChangedListener(this);
		}
	}

	public void onStop() {
		broadcastManager.unregisterReceiver(userProfileFoundReceiver);
		broadcastManager.unregisterReceiver(userProfileSaveReceiver);
	}

	private void listenForSavedProfile() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(SaveUserProfileService.ACTION_USER_PROFILE_SAVE_FAILED);
		filter.addAction(SaveUserProfileService.ACTION_USER_PROFILE_SAVED);
		broadcastManager.registerReceiver(userProfileSaveReceiver, filter);
	}
	
	private void listenForFoundUserProfile() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(FindCurrentUserProfileService.ACTION_CURRENT_USER_PROFILE_FOUND);
		intentFilter.addAction(FindCurrentUserProfileService.ACTION_CURRENT_USER_PROFILE_FIND_ERROR);
		broadcastManager.registerReceiver(userProfileFoundReceiver, intentFilter);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("currentProfile", currentProfile);
		outState.putBoolean("saveMenuItemEnabled", saveMenuItemEnabled);
		outState.putParcelable("newProfilePhotoUri", newProfilePhotoUri);
		outState.putBoolean("savingProfileDetails", savingProfileDetails);
		outState.putParcelable("newProfilePhotoBitmap", newProfilePhotoBitmap);
	}

	private void userProfileLoaded(UserProfile profile) {
		currentProfile = profile;
		activity.fillUserProfileDetails(profile);
		activity.showProfilePhoto(profile);
		activity.showUserProfile();
	}
	
	private void userProfileLoadError(String message) {
		activity.showLongToastWithText(message);
		activity.finish();
	}
	
	@Override
	public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		if(RequiredFieldsValidator.areFilled(activity.requiredFields())){
			saveMenuItemEnabled = activity.enableSaveProfileMenuItem(true);
			return;
		}
		saveMenuItemEnabled = activity.enableSaveProfileMenuItem(false);
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {}
	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

	public void saveProfileDetails() {
		saveNewProfileDetails(getNewProfileDetails());
		savingProfileDetails = true;
		activity.showUserProfileLoader();
	}

	private NewProfileDetails getNewProfileDetails() {
		NewProfileDetails newProfile = 
				new NewProfileDetails(activity.nickField.getText().toString(), 
						(Country) activity.countrySpinner.getSelectedItem());
		setBioForNewProfile(newProfile);
		setPhotoForNewProfile(newProfile, currentProfile);
		return newProfile;
	}

	private void setPhotoForNewProfile(NewProfileDetails newProfile, UserProfile currentProfile) {
		newProfile.photo(currentProfile.photo());
	}

	private void setBioForNewProfile(NewProfileDetails newProfile) {
		String bio = activity.bioField.getText().toString();
		if(!Strings.isEmpty(bio)){
			newProfile.bio(bio);
		}
	}

	private void saveNewProfileDetails(NewProfileDetails profileDetails) {
		Intent intent = new Intent(context, SaveUserProfileService.class);
		intent.putExtra(SaveUserProfileService.EXTRAS_NEW_USER_PROFILE, profileDetails);
		activity.startService(intent);
		
		intent = new Intent(context, UploadProfilePhotoService.class);
		intent.putExtra(UploadProfilePhotoService.EXTRAS_PHOTO_URI, newProfilePhotoUri);
		activity.startService(intent);
	}

	public void cameraLaunchFailed() {
		activity.showLongToastWithText(stringUtils
				.getString(R.string.choose_photo_action_camera_not_supported));
	}

	public void profilePhotoTaken(Uri photoUri, Bitmap loadedImage) {
		if(photoUri != null && loadedImage != null){
			photoUtils.addPhotoToGallery(photoUri);
			newProfilePhotoUri = photoUri;
			newProfilePhotoBitmap = loadedImage;
		}
	}

	public void profilePhotoSelected(Uri profilePhotoUri) {
		newProfilePhotoUri = profilePhotoUri;
	}

	public void onCameraActionSelected() {
		try {
			File profilePhotoFile = photoUtils.setupProfilePhotoFile();
			activity.launchCamera(Uri.fromFile(profilePhotoFile));
		} catch (IOException e) {
			e.printStackTrace();
			activity.showLongToastWithText(stringUtils.getString(R.string.profile_photo_file_failure));
		}
	}
	
	private void userProfileSaved() {
		savingProfileDetails = false;
		activity.finish();
	}
	
	private void userProfileSaveFailed() {
		savingProfileDetails = false;
		activity.showLongToastWithText(stringUtils.getString(R.string.profile_toast_save_failed));
		activity.showProfilePhoto(currentProfile);
		activity.showUserProfile();
	}

}
