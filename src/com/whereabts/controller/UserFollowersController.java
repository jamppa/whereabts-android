package com.whereabts.controller;

import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.activity.UserFollowersActivity;
import com.whereabts.model.UserProfiles;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

public class UserFollowersController implements Receiver {

	private UserFollowersActivity activity = null;
	private UserProfiles followerProfiles = null;
	
	@Inject
	private WhereabtsResultReceiver userFollowersReceiver;
	@Inject
	private ServiceLauncher serviceLauncher;
	
	public void onCreate(UserFollowersActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		this.followerProfiles = (UserProfiles) BundleUtils.getSerializable(savedInstanceState, "followerProfiles", followerProfiles);
	}

	public void onResume() {
		userFollowersReceiver.setReceiver(this);
		loadOrShowFollowerProfiles();
	}

	private void loadOrShowFollowerProfiles() {
		if(followerProfiles != null){
			activity.showFollowerProfiles(followerProfiles);
			return;
		}
		loadFollowerProfiles();
	}

	private void loadFollowerProfiles() {
		serviceLauncher.launchFindUserFollowersService(
				activity,
				activity.getUserId(), 
				userFollowersReceiver);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		if(actionCode == 0){
			this.followerProfiles = (UserProfiles) result.getSerializable(UserProfiles.BUNDLE_KEY);
			activity.showFollowerProfiles(followerProfiles);
		}
		else {
			activity.closeActivityAndShowToastForFail();
		}
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("followerProfiles", followerProfiles);
	}

	public void onDestroy() {
		this.activity = null;
		this.followerProfiles = null;
	}

	public void onPause() {
		userFollowersReceiver.setReceiver(null);
	}

}
