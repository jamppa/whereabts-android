package com.whereabts.controller;

import static com.whereabts.util.RequiredFieldsValidator.areFilled;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.inject.Inject;
import com.whereabts.activity.SendMessageActivity;
import com.whereabts.model.WhereabtsLocation;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.model.newly.NewMessage;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.PhotoUtils;

public class SendMessageController implements TextWatcher {

	@Inject
	private ServiceLauncher serviceLauncher;
	@Inject
	private PhotoUtils photoUtils;
	
	private SendMessageActivity activity = null;
	private NewLocationMessage message = null;
	private Uri photoUri = null;
	private Bitmap photoBitmap = null;
	
	public void onCreate(SendMessageActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		this.message = (NewLocationMessage) BundleUtils.getSerializable(savedInstanceState, "message", message);
		this.photoUri = (Uri) BundleUtils.getParcelable(savedInstanceState, "photoUri", photoUri);
		this.photoBitmap = (Bitmap) BundleUtils.getParcelable(savedInstanceState, "photoBitmap", photoBitmap);
	}
	
	public void onResume() {
		initRequiredFields();
		showCurrentMessage();
		showCurrentPhoto();
	}

	private void showCurrentPhoto() {
		if(photoUri != null && photoBitmap != null){
			activity.showPhoto(photoBitmap);
		}
	}

	private void showCurrentMessage() {
		if(message != null){
			activity.showMessage(message);
			activity.showButtons(message);
		}
	}

	public void sendButtonClicked(Location messageLocation) {
		serviceLauncher.launchSendMessageService(activity, buildMessage(messageLocation), photoUri);
		serviceLauncher.launchShareWhereaboutsFacebookService(activity, buildMessage(messageLocation));
		activity.finishAndShowToastForSendingMessage();
	}
	
	private NewLocationMessage buildMessage(Location messageLocation) {
		WhereabtsLocation location = new WhereabtsLocation(messageLocation);
		NewMessage message = new NewMessage(
				activity.getMessageField().getText().toString(), 
				activity.getSelectedCategory());
		return new NewLocationMessage(message, location, activity.shareToFacebook());
	}

	public void initRequiredFields() {
		for(EditText field : activity.requiredFields()) {
			field.addTextChangedListener(this);
		}
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		if(areFilled(activity.requiredFields())){
			activity.enableSendButton();
			return;
		}
		activity.disableSendButton();
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {}
	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,int arg3) {}

	public void onSaveInstanceState(Bundle outState, Location messageLocation) {
		outState.putSerializable("message", buildMessage(messageLocation));
		outState.putParcelable("photoUri", photoUri);
		outState.putParcelable("photoBitmap", photoBitmap);
	}

	public void photoAdded(Uri messagePhotoUri, Bitmap bitmap) {
		if(messagePhotoUri != null && bitmap != null){
			photoUri = messagePhotoUri;
			photoUtils.addPhotoToGallery(photoUri);
			photoBitmap = bitmap;
		}
	}

	public void onDestroy() {
		this.activity = null;
		this.message = null;
		this.photoUri = null;
		this.photoBitmap = null;
	}
	
}
