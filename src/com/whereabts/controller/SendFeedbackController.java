package com.whereabts.controller;

import static com.whereabts.util.RequiredFieldsValidator.areFilled;

import android.content.Intent;
import android.os.Bundle;

import com.whereabts.fragment.SendFeedbackFragment;
import com.whereabts.model.newly.NewFeedback;
import com.whereabts.service.intent.SendFeedbackService;

public class SendFeedbackController {

	public static final String SEND_BUTTON_ENABLED = "sendButtonEnabled";
	
	private SendFeedbackFragment fragment;
	private boolean sendButtonEnabled = false;
	
	public void onCreate(SendFeedbackFragment sendFeedbackFragment, Bundle savedInstanceState) {
		this.fragment = sendFeedbackFragment;
		fragment.setHasOptionsMenu(true);
		sendButtonEnabled = isSendButtonEnabled(savedInstanceState);
	}
	
	public void onResume() {
		initRequiredFields();
	}

	private boolean isSendButtonEnabled(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getBoolean(SEND_BUTTON_ENABLED, false);
		}
		return false;
	}

	private void initRequiredFields() {
		fragment.listenForTextChangesInRequiredFields();
	}

	public void textChangedOnRequiredField(CharSequence s) {
		if(areFilled(fragment.requiredFields())){
			enableSendButton();
			return;
		}
		disableSendButton();
	}

	private void disableSendButton() {
		fragment.disableSendButton();
		sendButtonEnabled = false;
	}

	private void enableSendButton() {
		fragment.enableSendButton();
		sendButtonEnabled = true;
	}

	public void initSendButton() {
		if(sendButtonEnabled){
			fragment.enableSendButton();
		}
		fragment.listenForSendButtonClicks();
	}

	public void sendButtonClicked() {
		sendFeedbackMessage(new NewFeedback(fragment.getFeedbackMessage()));
		fragment.notifyListenerSendButtonClicked();
	}

	private void sendFeedbackMessage(NewFeedback feedback) {
		Intent intent = new Intent(fragment.getActivity(), SendFeedbackService.class);
		intent.putExtra(SendFeedbackService.EXTRAS_FEEDBACK, feedback);
		fragment.getActivity().startService(intent);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putBoolean(SEND_BUTTON_ENABLED, sendButtonEnabled);
	}

	public void onDestroy() {
		this.fragment = null;
	}

}
