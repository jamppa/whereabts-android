package com.whereabts.controller;

import com.google.inject.Inject;
import com.whereabts.fragment.UserStreamFragment;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;
import com.whereabts.service.intent.ServiceLauncher;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.WhereabtsResultReceiver;
import com.whereabts.util.WhereabtsResultReceiver.Receiver;

import android.os.Bundle;

public class UserStreamFragmentController implements Receiver {

	@Inject
	private ServiceLauncher serviceLauncher;
	@Inject
	private WhereabtsResultReceiver userStreamReceiver;
	
	private UserStreamFragment fragment;
	private String userId;
	private CompactMessages currentMessages;
	private CompactMessage selectedMessage;
	private int selectedMessagesPosition;
	
	public void onCreate(UserStreamFragment userStreamFragment, Bundle savedInstanceState) {
		this.fragment = userStreamFragment;
		this.userId = userStreamFragment.getUserId();
		this.currentMessages = (CompactMessages) BundleUtils.getSerializable(savedInstanceState, "currentMessages", currentMessages);
		this.selectedMessage = (CompactMessage) BundleUtils.getSerializable(savedInstanceState, "selectedMessage", selectedMessage);
		this.selectedMessagesPosition = BundleUtils.getInt(savedInstanceState, "selectedMessagePosition", -1);
	}

	public void onResume() {
		userStreamReceiver.setReceiver(this);
		loadUserMessages();
	}
	
	public void onPause() {
		userStreamReceiver.setReceiver(null);
	}
	
	private void loadUserMessages() {
		if(currentMessages != null){
			showCurrentMessages();
			return;
		}
		serviceLauncher.launchFindUserMessagesService(fragment, userId, userStreamReceiver);
	}

	@Override
	public void onReceiveResult(int actionCode, Bundle result) {
		if(actionCode == 0){
			messagesLoaded((CompactMessages) result.getSerializable(CompactMessages.BUNDLE_KEY));
		}
	}

	private void messagesLoaded(CompactMessages messages) {
		if(currentMessages == null){
			currentMessages = messages;
			showCurrentMessages();
		}
		else{
			refreshMessages(messages);
		}
	}

	private void showCurrentMessages() {
		if(!currentMessages.isEmpty()){
			fragment.showMessagesList(currentMessages);
			fragment.setMessagesListPosition(selectedMessagesPosition);
			notifyFragmentListener(currentMessages);
		}
		else{
			fragment.showNoMessagesText();
		}
	}

	private void refreshMessages(CompactMessages messages) {
		currentMessages.addAll(messages);
		fragment.refreshMessagesList(messages);
	}

	private void notifyFragmentListener(CompactMessages messages) {
		if(selectedMessage != null){
			fragment.notifyListenerSelectedMessage(selectedMessage);
			return;
		}
		fragment.notifyListenerMessagesLoaded(messages.first());
	}

	public void onPullUpToRefresh() {
		long olderThan = currentMessages.first().createdAt();
		serviceLauncher.launchFindUserMessagesService(
				fragment, userId, userStreamReceiver, currentMessages.size(), olderThan);
	}

	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("currentMessages", currentMessages);
		outState.putSerializable("selectedMessage", selectedMessage);
		outState.putInt("selectedMessagePosition", selectedMessagesPosition);
	}

	public void onMessageSelected(CompactMessage message, int position) {
		this.selectedMessage = message;
		this.selectedMessagesPosition = position;
		fragment.notifyListenerSelectedMessage(selectedMessage);
	}

	public void removeMessage(CompactMessage message) {
		if(currentMessages.get().remove(message)){
			selectedMessage = null;
			selectedMessagesPosition = -1;
		}
	}


}
