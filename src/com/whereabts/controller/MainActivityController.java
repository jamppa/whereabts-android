package com.whereabts.controller;

import static android.support.v4.content.LocalBroadcastManager.getInstance;
import android.accounts.Account;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.whereabts.activity.MainActivity;
import com.whereabts.api.ApiRoutes;
import com.whereabts.model.User;
import com.whereabts.service.AppInitializationService;
import com.whereabts.service.UserAccountService;
import com.whereabts.service.intent.RequiredVersionService;
import com.whereabts.service.intent.UserRegistrationService;
import com.whereabts.util.StringUtils;

public class MainActivityController {

	@Inject
	private AppInitializationService initializationService;
	@Inject
	private UserAccountService userAccountService;
	@Inject
	private StringUtils stringUtils;
	
	private BroadcastReceiver userRegisteredReceiver = initUserRegistrationBroadcastReceiver();
	private BroadcastReceiver versionRecevier = initRequiredVersionBroadcastReceiver();
	
	private boolean appInitialized = false;
	private boolean googlePlayAvailable = false;
	private MainActivity activity = null;
	
	public void onCreate(MainActivity mainActivity) {
		this.activity = mainActivity;
		appInitialized = initializationService.isInitialized();
		googlePlayAvailable = isGooglePlayAvailable();
	}
	
	public void onResume() {
		preloadDefaultUserProfilePhoto();
		listenForRequiredClientVersion();
		listenForUserRegistration();
		initializationService.checkRequiredClientVersion();
	}

	private void preloadDefaultUserProfilePhoto() {
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.loadImage(stringUtils.photosApiRoot() + ApiRoutes.NO_USER_PROFILE_PHOTO_API, null);
	}

	private void listenForRequiredClientVersion() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(RequiredVersionService.ACTION_REQUIRED_VERSION_FIND_FAILED);
		intentFilter.addAction(RequiredVersionService.ACTION_REQUIRED_VERSION_FOUND);
		getInstance(activity).registerReceiver(versionRecevier, intentFilter);
	}

	public void onPause() {
		unlistenForAnonUserRegistered();
		unlistenForRequiredClientVersion();
		closeDialogsIfVisible();
	}

	private void unlistenForRequiredClientVersion() {
		getInstance(activity).unregisterReceiver(versionRecevier);
	}

	private void closeDialogsIfVisible() {
		activity.dismissDialogs();
	}

	private void unlistenForAnonUserRegistered() {
		getInstance(activity).unregisterReceiver(userRegisteredReceiver);
	}

	public void onInitializationFailed() {
		activity.showDialogForError();
	}

	public void onInitializationFinished() {
		boolean userInitialized = initializationService.isUserInitialized();
		googlePlayAvailable = isGooglePlayAvailable();
		showEnableGooglePlayDialogIfNecessary(userInitialized, googlePlayAvailable);
		activity.launchMapActivity(userInitialized, googlePlayAvailable);
	}
	
	private void initApplicationIfNecessary(boolean initialized) {
		if(!initialized){
			if(!initializationService.isUserInitialized()){
				initializeUser();
				return;
			}
			if(!initializationService.isGcmInitialized()){
				initializationService.registerCurrentUser();
			}
		}
	}
	
	private void initializeUser() {
		if(userAccountService.hasMultipleGoogleAccounts()){
			activity.showChooseAccountDialog(userAccountService.googleAccounts());
			return;
		}
		registerUser(userAccountService.googleAccount());
	}

	private void listenForUserRegistration() {
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(UserRegistrationService.ACTION_ANON_USER_REGISTERED);
		intentFilter.addAction(UserRegistrationService.ACTION_ANON_USER_REGISTRATION_FAILED);
		getInstance(activity).registerReceiver(userRegisteredReceiver, intentFilter);
	}
	
	private void showEnableGooglePlayDialogIfNecessary(boolean appInitialized, boolean googlePlayAvailable) {
		if(appInitialized && !googlePlayAvailable){
			activity.showGooglePlayDialog();
		}
	}

	private boolean isGooglePlayAvailable() {
		int serviceAvailabilityCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
		return serviceAvailabilityCode == ConnectionResult.SUCCESS;
	}
	
	private BroadcastReceiver initUserRegistrationBroadcastReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(UserRegistrationService.ACTION_ANON_USER_REGISTRATION_FAILED)){
					onInitializationFailed();
					return;
				}
				onInitializationFinished();
			}
		};
	}
	
	private BroadcastReceiver initRequiredVersionBroadcastReceiver() {
		return new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				if(intent.getAction().equals(RequiredVersionService.ACTION_REQUIRED_VERSION_FOUND)){
					startAppWithVersion(intent.getIntExtra(RequiredVersionService.EXTRAS_REQUIRED_VERSION, 0));
				}
				else if(intent.getAction().equals(RequiredVersionService.ACTION_REQUIRED_VERSION_FIND_FAILED)){
					onInitializationFailed();
				}
			}
		};
	}

	private void startAppWithVersion(int requiredAppVersion) {
		if(initializationService.isThisClientSupported(requiredAppVersion)){
			initApplicationIfNecessary(appInitialized);
			showEnableGooglePlayDialogIfNecessary(appInitialized, googlePlayAvailable);
			activity.launchMapActivity(appInitialized, googlePlayAvailable);
		}
		else{
			activity.showNewVersionRequiredDialog();
		}
	}
	
	public void registerUser(Account account) {
		if(account != null){
			initializationService.registerUser(User.newUser(account.name));
			return;
		}
		activity.showDialogForError();
	}

	public void onDestroy() {
		this.activity = null;
	}

}
