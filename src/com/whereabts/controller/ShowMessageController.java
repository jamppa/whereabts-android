package com.whereabts.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.MenuItem;

import com.google.inject.Inject;
import com.whereabts.activity.ShowMessageActivity;
import com.whereabts.model.Message;
import com.whereabts.model.Reply;
import com.whereabts.model.newly.NewReply;
import com.whereabts.service.UserService;
import com.whereabts.service.intent.DeleteMessageService;
import com.whereabts.service.intent.FindMessageService;
import com.whereabts.service.intent.LikeMessageService;
import com.whereabts.service.intent.SendReplyService;
import com.whereabts.util.BundleUtils;
import com.whereabts.widget.dialog.ReplyMessageDialog;

public class ShowMessageController {

	public static final String TAG = "com.whereabts.controller.ShowMessageController";
	private static final String MESSAGE = TAG + ".message";
	private static final String IS_LOADING = TAG + ".isLoading";
	private static final String IS_DISCARD_MENU_ITEM_SHOWING = TAG + ".discardMessageMenuItemShowing";
	private static final String IS_SENDING_REPLY = TAG + ".isSendingReply";
	private static final String IS_LIKE_MENU_ITEM_ENABLED = TAG + ".likeButtonEnabled";
	
	private ShowMessageActivity activity = null;
	private Message currentMessage = null;
	
	private boolean isLoading = false;
	private boolean isSendingReply = false;
	private boolean discardMessageMenuItemShowing = false;
	private boolean likeButtonEnabled = true;
	
	@Inject
	private UserService userService;
	
	private BroadcastReceiver replySentReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(SendReplyService.ACTION_REPLY_SENT)){
				Reply sentReply = (Reply)intent.getSerializableExtra(SendReplyService.EXTRAS_REPLY);
				if(currentMessage != null){
					currentMessage.addReply(sentReply);
					replySent(currentMessage);
				}
			}
			else if(intent.getAction().equals(SendReplyService.ACTION_REPLY_SEND_FAILED)){
				if(currentMessage != null){
					replySent(currentMessage);
					activity.showToastForError(intent.getStringExtra(SendReplyService.EXTRAS_FAIL_REASON));
				}
			}
		}
	};
	
	private BroadcastReceiver messageFoundReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(FindMessageService.ACTION_MESSAGE_FOUND)){
				if(activity != null){
					messageLoaded((Message)intent.getSerializableExtra(FindMessageService.EXTRAS_RESULT_MESSAGE));
				}
			}
			else if(intent.getAction().equals(FindMessageService.ACTION_MESSAGE_FOUND_ERROR)){
				if(activity != null){
					messageLoadFailed(intent.getExtras());
				}
			}
		}
	};
	
	public void onCreate(ShowMessageActivity activity, Bundle savedInstanceState) {
		this.activity = activity;
		initActionBar(savedInstanceState);
		showMessage(savedInstanceState);
		showLoaderIfSendingReply(savedInstanceState);
	}
	
	private void showLoaderIfSendingReply(Bundle savedInstanceState) {
		isSendingReply = BundleUtils.getBoolean(savedInstanceState, IS_SENDING_REPLY, false);
		if(isSendingReply){
			activity.showMessageDetailsLoader();
		}
	}

	private void initActionBar(Bundle savedInstanceState) {
		discardMessageMenuItemShowing = BundleUtils.getBoolean(savedInstanceState, IS_DISCARD_MENU_ITEM_SHOWING, false);
		likeButtonEnabled = BundleUtils.getBoolean(savedInstanceState, IS_LIKE_MENU_ITEM_ENABLED, true);
		activity.showHomeButton();
	}

	private void showMessage(Bundle savedInstanceState) {
		if(hasMessage(savedInstanceState)){
			messageLoaded((Message)savedInstanceState.getSerializable(MESSAGE));
			return;
		}
		loadMessage(savedInstanceState);
	}

	private boolean hasMessage(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getSerializable(MESSAGE) != null;
		}
		return false;
	}

	private void loadMessage(Bundle savedInstanceState) {
		activity.showMessageDetailsLoader();
		if(!isAlreadyLoading(savedInstanceState)){
			startLoadingMessage();
		}
	}

	private void startLoadingMessage() {
		isLoading = true;
		findMessage(activity.getMessageId());
	}

	private boolean isAlreadyLoading(Bundle savedInstanceState) {
		if(savedInstanceState != null){
			return savedInstanceState.getBoolean(IS_LOADING);
		}
		return false;
	}

	private void findMessage(String messageId) {
		Intent intent = new Intent(activity, FindMessageService.class);
		intent.putExtra(FindMessageService.EXTRAS_MESSAGE_ID, messageId);
		activity.startService(intent);
	}

	private void replySent(Message message) {
		isSendingReply = false;
		currentMessage = message;
		activity.showMessageDetails(currentMessage);
	}

	private void messageLoadFailed(Bundle result) {
		isLoading = false;
		activity.showToastForError(result.getString(FindMessageService.EXTRAS_RESULT_FAILREASON));
		activity.finish();
	}

	private void messageLoaded(Message message) {
		isLoading = false;
		currentMessage = message;
		activity.showMessageDetails(message);
		showDiscardMessageMenuItemIfOwner(message);
		disableLikeMessageMenuItemIfLiked(message);
	}

	private void disableLikeMessageMenuItemIfLiked(Message message) {
		if(message.isLiked()){
			likeButtonEnabled = false;
			activity.disableLikeMenuItem();
		}
	}

	private void showDiscardMessageMenuItemIfOwner(Message message) {
		if(message.isOwner()){
			discardMessageMenuItemShowing = true;
			activity.showDiscardMessageMenuItem();
		}
	}

	public void saveViewState(Bundle outState) {
		outState.putSerializable(MESSAGE, currentMessage);
		outState.putBoolean(IS_LOADING, isLoading);
		outState.putBoolean(IS_DISCARD_MENU_ITEM_SHOWING, discardMessageMenuItemShowing);
		outState.putBoolean(IS_SENDING_REPLY, isSendingReply);
		outState.putBoolean(IS_LIKE_MENU_ITEM_ENABLED, likeButtonEnabled);
	}

	public void onStop() {
		stopListeningForMessageFindResults();
		stopListeningForSentReplies();
	}
	
	public void onResume() {
		listenForMessageFindResults();
		listenForSentReplies();
	}
	
	private void listenForMessageFindResults() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(FindMessageService.ACTION_MESSAGE_FOUND);
		intentFilter.addAction(FindMessageService.ACTION_MESSAGE_FOUND_ERROR);
		broadcastManager.registerReceiver(messageFoundReceiver, intentFilter);
	}
	
	private void stopListeningForMessageFindResults() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		broadcastManager.unregisterReceiver(messageFoundReceiver);
	}
	
	private void stopListeningForSentReplies() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		broadcastManager.unregisterReceiver(replySentReceiver);
	}

	private void listenForSentReplies() {
		LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(activity);
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(SendReplyService.ACTION_REPLY_SENT);
		intentFilter.addAction(SendReplyService.ACTION_REPLY_SEND_FAILED);
		broadcastManager.registerReceiver(replySentReceiver, intentFilter);
	}

	public void onCreateOptionsMenu() {
		if(discardMessageMenuItemShowing){
			activity.showDiscardMessageMenuItem();
		}
		if(!likeButtonEnabled){
			activity.disableLikeMenuItem();
		}
		activity.setMenuItemsEnabled(!(isLoading || isSendingReply));
	}

	public void onDiscardMessageMenuItemClicked() {
		activity.showDiscardMessageDialog();
	}

	public void onDiscardMessageDialogOkClicked() {
		deleteCurrentMessage();
		activity.finishWithMessageDeletedResult(currentMessage);
	}

	private void deleteCurrentMessage() {
		Intent intent = new Intent(activity, DeleteMessageService.class);
		intent.putExtra(DeleteMessageService.EXTRAS_MESSAGE, currentMessage);
		activity.startService(intent);
	}

	public void onReplyMessageMenuItemClicked() {
		launchReplyMessageDialog();
	}

	private void launchReplyMessageDialog() {
		Bundle bundle = new Bundle();
		bundle.putSerializable(ReplyMessageDialog.MSG_TO_REPLY_ARG, currentMessage);
		activity.showReplyMessageDialog(bundle);
	}

	public void onReplyMessageDialogSendButtonClicked(NewReply reply) {
		isSendingReply = true;
		activity.showMessageDetailsLoader();
		sendReplyMessage(reply);
	}

	private void sendReplyMessage(NewReply reply) {
		Intent intent = new Intent(activity, SendReplyService.class);
		intent.putExtra(SendReplyService.EXTRAS_REPLY, reply);
		activity.startService(intent);
	}

	public void onUserClicked() {
		if(currentMessage != null){
			activity.launchShowUserProfileActivity(currentMessage.userId());
		}
	}

	public void onLikeMenuClicked(MenuItem item) {
		if(!isLoading) {
			activity.disableLikeMenuItem();
			likeButtonEnabled = false;
			likeMessage(currentMessage);
		}
	}

	private void likeMessage(Message message) {
		Intent intent = new Intent(activity, LikeMessageService.class);
		intent.putExtra(LikeMessageService.EXTRAS_MESSAGE_TO_LIKE, message);
		activity.startService(intent);
	}

	public void onDestroy() {
		this.activity = null;
		this.currentMessage = null;
	}

}
