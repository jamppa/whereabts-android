package com.whereabts.fragment;

import java.util.ArrayList;
import java.util.List;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.SendFeedbackController;
import com.whereabts.model.newly.NewFeedback;

public class SendFeedbackFragment extends RoboFragment implements TextWatcher, OnMenuItemClickListener {

	@Inject
	private SendFeedbackController controller;
	
	@InjectView(R.id.send_feedback_message)
	private EditText feedbackMessage;
	
	private MenuItem sendButton;
	private SendFeedbackFragmentListener listener;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_send_feedback, container, false);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.fragment_send_feedback, menu);
		sendButton = menu.findItem(R.id.fragment_send_feedback_menu_sendbtn);
		controller.initSendButton();
		notifyListenerFragmentStarted();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}

	public void listenForTextChangesInRequiredFields() {
		feedbackMessage.addTextChangedListener(this);
	}
	
	public void listenForSendButtonClicks() {
		sendButton.setOnMenuItemClickListener(this);
	}
	
	@Override
	public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
		controller.textChangedOnRequiredField(s);
	}

	public List<EditText> requiredFields() {
		List<EditText> fields = new ArrayList<EditText>();
		fields.add(feedbackMessage);
		return fields;
	}

	public void enableSendButton() {
		if(!sendButton.isEnabled()){
			sendButton.setEnabled(true);
		}
	}

	public void disableSendButton() {
		if(sendButton.isEnabled()){
			sendButton.setEnabled(false);
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		listener = (SendFeedbackFragmentListener) activity;
	}

	public void notifyListenerSendButtonClicked() {
		if(listener != null){
			listener.onFeedbackSendButtonClicked();
		}
	}
	
	public void notifyListenerFragmentStarted() {
		if(listener != null){
			listener.onFeedbackFragmentStarted();
		}
	}
	
	@Override
	public void afterTextChanged(Editable arg0) {}
	@Override
	public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {}
	
	public String getFeedbackMessage() {
		return feedbackMessage.getText().toString();
	}
	
	public void showFeedbackMessage(NewFeedback failedFeedback) {
		feedbackMessage.setText(failedFeedback.message());
	}

	@Override
	public boolean onMenuItemClick(MenuItem menuItem) {
		controller.sendButtonClicked();
		return true;
	}
	
	public interface SendFeedbackFragmentListener {
		void onFeedbackSendButtonClicked();
		void onFeedbackFragmentStarted();
	}

}
