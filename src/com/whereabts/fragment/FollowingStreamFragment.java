package com.whereabts.fragment;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.inject.Inject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.whereabts.R;
import com.whereabts.adapter.CompactMessageAdapter;
import com.whereabts.controller.FollowingStreamFragmentController;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;
import com.whereabts.util.StringUtils;

public class FollowingStreamFragment extends RoboFragment implements OnItemClickListener, OnRefreshListener2<ListView> {
	
	public interface FollowingStreamFragmentListener {
		void onMessagesLoaded(CompactMessage first);
		void onMessageSelected(CompactMessage message);
	}
	
	@Inject
	private FollowingStreamFragmentController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private Context context;
	
	@InjectView(R.id.fragment_following_stream_list)
	private PullToRefreshListView messagesList;
	@InjectView(R.id.fragment_following_stream_loader_container)
	private View loaderContainer;
	@InjectView(R.id.fragment_following_stream_loader)
	private ProgressBar loader;
	@InjectView(R.id.fragment_following_stream_no_messages)
	private TextView noMessagesText;
	
	private ArrayAdapter<CompactMessage> messagesListAdapter;
	private FollowingStreamFragmentListener listener;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		messagesList.setOnItemClickListener(null);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_following_stream, container, false);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		initMessagesList();
		controller.onResume();
	}

	private void initMessagesList() {
		messagesList.setMode(Mode.PULL_FROM_END);
		messagesList.setOnRefreshListener(this);
		messagesList.getRefreshableView().setOverScrollMode(ListView.OVER_SCROLL_NEVER);
		messagesList.setVisibility(View.GONE);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		controller.onPause();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		listener = (FollowingStreamFragmentListener) activity;
	}

	public void showMessagesList(CompactMessages currentMessages) {
		messagesListAdapter = new CompactMessageAdapter(
				context, currentMessages, stringUtils.photosApiRoot());
		messagesList.setAdapter(messagesListAdapter);
		messagesList.setOnItemClickListener(this);
		hideLoader();
	}

	private void hideLoader() {
		loaderContainer.setVisibility(View.GONE);
		messagesList.setVisibility(View.VISIBLE);
	}

	public void notifyListenerMessagesLoaded(CompactMessage first) {
		listener.onMessagesLoaded(first);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		CompactMessage message = (CompactMessage) adapter.getItemAtPosition(pos);
		if(message != null){
			controller.onMessageSelected(message, pos);
		}
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		controller.onPullUpToRefresh();
	}

	public void refreshMessagesList(CompactMessages newMessages) {
		messagesListAdapter.addAll(newMessages.get());
		messagesListAdapter.notifyDataSetChanged();
		messagesList.onRefreshComplete();
	}

	public void showNoMessagesText() {
		loader.setVisibility(View.GONE);
		noMessagesText.setVisibility(View.VISIBLE);
	}

	public void notifyListenerSelectedMessage(CompactMessage selectedMessage) {
		listener.onMessageSelected(selectedMessage);
	}

	public void setMessagesListPosition(int selectedMessagesPosition) {
		if(selectedMessagesPosition > 0){
			messagesList.getRefreshableView().setSelection(selectedMessagesPosition);
		}
	}
	
}
