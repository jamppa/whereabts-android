package com.whereabts.fragment;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener2;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.whereabts.R;
import com.whereabts.adapter.CompactMessageAdapter;
import com.whereabts.controller.UserStreamFragmentController;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;
import com.whereabts.util.StringUtils;

public class UserStreamFragment extends RoboFragment implements OnRefreshListener2<ListView>, OnItemClickListener {

	public interface UserStreamFragmentListener {
		void onMessagesLoaded(CompactMessage first);
		void onMessageSelected(CompactMessage message);
		String getUserId();
	}
	
	@Inject
	private UserStreamFragmentController controller;
	@Inject
	private Context context;
	@Inject
	private StringUtils stringUtils;
	
	@InjectView(R.id.fragment_user_stream_list)
	private PullToRefreshListView messagesList;
	@InjectView(R.id.fragment_user_stream_loader_container)
	private View loaderContainer;
	@InjectView(R.id.fragment_user_stream_loader)
	private View loader;
	@InjectView(R.id.fragment_user_stream_empty)
	private TextView noMessagesText;
	
	private ArrayAdapter<CompactMessage> messagesListAdapter;
	private UserStreamFragmentListener listener;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	public void onResume() {
		super.onResume();
		initMessagesList();
		controller.onResume();
	}
	
	public void removeMessage(CompactMessage message) {
		controller.removeMessage(message);
	}
	
	private void initMessagesList() {
		messagesList.setMode(Mode.PULL_FROM_END);
		messagesList.setOnRefreshListener(this);
		messagesList.getRefreshableView().setOverScrollMode(ListView.OVER_SCROLL_NEVER);
		messagesList.setVisibility(View.GONE);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.fragment_user_stream, container, false);
	}

	@Override
	public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {}

	@Override
	public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
		controller.onPullUpToRefresh();
	}
	
	@Override
	public void onPause() {
		super.onPause();
		controller.onPause();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		listener = (UserStreamFragmentListener) activity;
	}

	public void showMessagesList(CompactMessages currentMessages) {
		messagesListAdapter = new CompactMessageAdapter(
				context, currentMessages, stringUtils.photosApiRoot());
		messagesList.setAdapter(messagesListAdapter);
		messagesList.setOnItemClickListener(this);
		hideLoader();
	}

	private void hideLoader() {
		loaderContainer.setVisibility(View.GONE);
		messagesList.setVisibility(View.VISIBLE);
	}

	public void notifyListenerMessagesLoaded(CompactMessage first) {
		listener.onMessagesLoaded(first);
	}
	
	public void notifyListenerSelectedMessage(CompactMessage selectedMessage) {
		listener.onMessageSelected(selectedMessage);
	}
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		CompactMessage message = (CompactMessage) adapter.getItemAtPosition(pos);
		if(message != null){
			controller.onMessageSelected(message, pos);
		}
	}

	public void refreshMessagesList(CompactMessages newMessages) {
		messagesListAdapter.addAll(newMessages.get());
		messagesListAdapter.notifyDataSetChanged();
		messagesList.onRefreshComplete();
	}

	public void setMessagesListPosition(int selectedMessagesPosition) {
		if(selectedMessagesPosition > 0){
			messagesList.getRefreshableView().setSelection(selectedMessagesPosition);
		}
	}
	
	public String getUserId() {
		return listener.getUserId();
	}

	public void showNoMessagesText() {
		loader.setVisibility(View.GONE);
		loaderContainer.setVisibility(View.VISIBLE);
		noMessagesText.setVisibility(View.VISIBLE);
	}
	
}
