package com.whereabts;

import android.app.Application;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class WhereabtsApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		try{
			setupUniversalImageLoader();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	private void setupUniversalImageLoader() {
		ImageLoaderConfiguration config = 
				new ImageLoaderConfiguration.Builder(getApplicationContext())
				.defaultDisplayImageOptions(displayImageOptions()).build();
		ImageLoader.getInstance().init(config);
	}

	private DisplayImageOptions displayImageOptions() {
		return new DisplayImageOptions.Builder()
			.cacheOnDisc(true)
			.cacheInMemory(true)
			.resetViewBeforeLoading(true).build();
	}
	
}
