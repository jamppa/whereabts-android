package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.adapter.UserProfileAdapter;
import com.whereabts.controller.UserFollowingController;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.userprofile.UserProfileItemClickListener;

@ContentView(R.layout.activity_user_followers)
public class UserFollowingActivity extends RoboActivity {

	public static final String TAG = "com.whereabts.activity.UserFollowingActivity";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	
	@InjectView(R.id.user_followers_list)
	private ListView userFollowingList;
	@InjectView(R.id.user_followers_loader)
	private View userFollowingLoader;
	@InjectExtra(value = EXTRAS_USER_ID)
	private String userId;
	
	@Inject
	private UserFollowingController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private UserProfileItemClickListener userProfileClickListener;
	
	public String getUserId() {
		return this.userId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		controller.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}

	public void showFollowingProfiles(UserProfiles followingProfiles) {
		userFollowingList.setAdapter(new UserProfileAdapter(
				getApplicationContext(), followingProfiles, stringUtils.photosApiRoot()));
		userFollowingList.setOnItemClickListener(userProfileClickListener);
		userFollowingLoader.setVisibility(View.GONE);
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
}
