package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.adapter.UserProfileAdapter;
import com.whereabts.controller.PeopleController;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.people.SearchPeopleMenu;
import com.whereabts.widget.userprofile.UserProfileItemClickListener;

@ContentView(R.layout.activity_people)
public class PeopleActivity extends RoboActivity {
	
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private PeopleController controller;
	@Inject
	private SearchPeopleMenu searchPeopleMenu;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private UserProfileItemClickListener userProfileItemClickListener;
	
	@InjectView(R.id.people_list_loader)
	private View peopleListLoader;
	@InjectView(R.id.people_list)
	private ListView peopleList;
	@InjectView(R.id.people_list_empty)
	private TextView emptyPeoplesList;
		
	private TextView recentProfilesSeparator = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initPeoplesList();
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
		recentProfilesSeparator = null;
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		if(Intent.ACTION_SEARCH.equals(intent.getAction())){
			controller.onSearch(intent.getStringExtra(SearchManager.QUERY));
		}
	}

	private void initPeoplesList() {
		recentProfilesSeparator = (TextView) getLayoutInflater().inflate(R.layout.list_separator, null);
		recentProfilesSeparator.setText(R.string.people_recent_label);
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_people, menu);
		searchPeopleMenu.setMenu(menu, this.getComponentName());
		controller.onCreateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		controller.onStop();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}
	
	public void showUserProfiles(UserProfiles profiles, boolean isSearch) {
		handleHeaderView(isSearch);
		peopleList.setEmptyView(emptyPeoplesList);
		peopleList.setAdapter(new UserProfileAdapter(
				getApplicationContext(), profiles, stringUtils.photosApiRoot()));
		peopleList.setOnItemClickListener(userProfileItemClickListener);
		showList();
	}

	private void handleHeaderView(boolean isSearch) {
		if(isSearch){
			peopleList.removeHeaderView(recentProfilesSeparator);
		}
		else{
			peopleList.addHeaderView(recentProfilesSeparator, null, false);
		}
	}
	
	public void showMenuItems() {
		searchPeopleMenu.showMenuItems();
	}

	public void showLoader() {
		peopleList.setVisibility(View.GONE);
		peopleListLoader.setVisibility(View.VISIBLE);
	}
	
	private void showList() {
		peopleListLoader.setVisibility(View.GONE);
		peopleList.setVisibility(View.VISIBLE);
	}

}
