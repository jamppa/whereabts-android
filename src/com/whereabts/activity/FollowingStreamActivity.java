package com.whereabts.activity;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.FollowingStreamController;
import com.whereabts.fragment.FollowingStreamFragment.FollowingStreamFragmentListener;
import com.whereabts.model.CompactMessage;

@ContentView(R.layout.activity_following_stream)
public class FollowingStreamActivity extends RoboFragmentActivity implements FollowingStreamFragmentListener {

	@Inject
	private FollowingStreamController controller;
	@Inject
	private ActivityLauncher activityLauncher;
	
	@InjectFragment(R.id.following_stream_map)
	private SupportMapFragment mapFragment;
	
	public GoogleMap getMap() {
		return mapFragment.getMap();
	}	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		initMap();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		GoogleMap map = getMap();
		map.setOnInfoWindowClickListener(null);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	private void initMap() {
		GoogleMap map = getMap();
		map.setPadding(0, 100, 0, 0);
		map.setOnInfoWindowClickListener(controller);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			activityLauncher.launchHomeActivity();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onMessagesLoaded(CompactMessage first) {
		controller.onMessageSelected(first);
	}

	@Override
	public void onMessageSelected(CompactMessage message) {
		controller.onMessageSelected(message);
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

}
