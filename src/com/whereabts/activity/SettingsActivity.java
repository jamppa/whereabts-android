package com.whereabts.activity;

import roboguice.activity.RoboPreferenceActivity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceCategory;

import com.facebook.UiLifecycleHelper;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.service.FacebookSessionService;

public class SettingsActivity extends RoboPreferenceActivity {
	
	@Inject
	private FacebookSessionService fbSessionService;
	
	private UiLifecycleHelper fbUiHelper;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.layout.fragment_settings);
		fbUiHelper = new UiLifecycleHelper(this, fbSessionService);
		fbUiHelper.onCreate(savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		fbUiHelper.onResume();
		checkFacebookSession();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fbUiHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		fbUiHelper.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		fbUiHelper.onSaveInstanceState(outState);
	}

	@SuppressWarnings("deprecation")
	private void checkFacebookSession() {
		if(!fbSessionService.hasActiveSession()){
			PreferenceCategory facebookSettings = (PreferenceCategory) findPreference("pref_key_facebook_settings");
			facebookSettings.setEnabled(false);
		}
	}

}
