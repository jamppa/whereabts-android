package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.ShowUserProfileController;
import com.whereabts.model.UserWithProfile;
import com.whereabts.service.FacebookSessionService;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.userprofile.FacebookConnectButton;
import com.whereabts.widget.userprofile.UserProfileMenu;

@ContentView(R.layout.activity_show_userprofile)
public class ShowUserProfileActivity extends RoboActivity {

	private static final String TAG = "com.whereabts.activity.ShowUserProfileActivity";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	
	@InjectExtra(value = EXTRAS_USER_ID)
	private String userId;
	
	@InjectView(R.id.userprofile_container)
	private View profile;
	@InjectView(R.id.userprofile_loader)
	private View profileLoader;
	@InjectView(R.id.userprofile_pic)
	private ImageView userPic;
	@InjectView(R.id.userprofile_nick)
	private TextView userNick;
	@InjectView(R.id.userprofile_country)
	private TextView userCountry;
	@InjectView(R.id.userprofile_bio)
	private TextView userBio;
	@InjectView(R.id.userprofile_following_count)
	private TextView followingCount;
	@InjectView(R.id.userprofile_followers_count)
	private TextView followersCount;
	@InjectView(R.id.userprofile_whereabouts_count)
	private TextView whereaboutsCount;
	@InjectView(R.id.userprofile_fb_connect)
	private LoginButton fbButton;
	
	@Inject
	private ShowUserProfileController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private UserProfileMenu userProfileMenu;
	@Inject
	private FacebookConnectButton fbConnectButton;
	@Inject
	private FacebookSessionService facebookSessionService;
	
	private UiLifecycleHelper fbUiHelper;
	
	public String getUserId() {
		return userId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fbUiHelper = new UiLifecycleHelper(this, facebookSessionService);
		fbUiHelper.onCreate(savedInstanceState);
		fbConnectButton.setButton(fbButton);
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		fbUiHelper.onStop();
		controller.onStop();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		fbUiHelper.onPause();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		fbUiHelper.onResume();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		fbUiHelper.onActivityResult(requestCode, resultCode, data);
		if(requestCode != FacebookSessionService.REQUEST_CODE_PERMISSIONS){
			facebookSessionService.requestPublishPermissions(this);
		}
		if(requestCode == FacebookSessionService.REQUEST_CODE_PERMISSIONS && resultCode == 0){
			facebookSessionService.getActiveSession().closeAndClearTokenInformation();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		fbUiHelper.onDestroy();
		controller.onDestroy();
		userProfileMenu.onDestroy();
		fbConnectButton.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_show_userprofile, menu);
		userProfileMenu.setMenu(menu);
		controller.onResume();
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		fbUiHelper.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}

	public void showProfile() {
		profileLoader.setVisibility(View.GONE);
		profile.setVisibility(View.VISIBLE);
	}

	public void showProfileLoader() {
		profile.setVisibility(View.GONE);
		profileLoader.setVisibility(View.VISIBLE);
	}

	public void fillProfile(UserWithProfile profile) {
		userNick.setText(profile.userNick());
		userCountry.setText(profile.userCountry());
		userBio.setText(profile.userBio());
		followingCount.setText(profile.followingAsString());
		followersCount.setText(profile.followersAsString());
		whereaboutsCount.setText(profile.whereabouts());
		profile.userProfile().showPhoto(userPic, stringUtils.photosApiRoot());
	}

	public void changeTitleToMe() {
		getActionBar().setTitle(R.string.show_own_profile_label);
	}
	
	public void onEditProfileMenuItemClicked(MenuItem item){
		activityLauncher.launchEditUserProfileActivity();
		finish();
	}
	
	public void onFollowMenuItemClicked(MenuItem item){
		controller.followUser(
				userProfileMenu.toggleFollowMenuItem());
	}

	public void showMenuItems(UserWithProfile profile) {
		userProfileMenu.showMenuItems(profile);
		fbConnectButton.showIfOwnProfile(profile);
	}

	public void onFollowersClicked(View view){
		controller.onFollowersClicked();
	}
	
	public void onFollowingClicked(View view){
		controller.onFollowingClicked();
	}
	
	public void onWhereaboutsClicked(View view){
		controller.onWhereaboutsClicked();
	}
	
}
