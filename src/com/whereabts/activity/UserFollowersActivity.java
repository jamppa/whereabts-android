package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.adapter.UserProfileAdapter;
import com.whereabts.controller.UserFollowersController;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.StringUtils;
import com.whereabts.util.ToastShower;
import com.whereabts.widget.userprofile.UserProfileItemClickListener;

@ContentView(R.layout.activity_user_followers)
public class UserFollowersActivity extends RoboActivity {

	public static final String TAG = "com.whereabts.activity.UserFollowersActivity";
	public static final String EXTRAS_USER_ID = TAG + ".userId";
	
	@InjectView(R.id.user_followers_list)
	private ListView userFollowersList;
	@InjectView(R.id.user_followers_loader)
	private View userFollowersLoader;
	
	@InjectExtra(value = EXTRAS_USER_ID)
	private String userId;
	@Inject
	private UserFollowersController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private UserProfileItemClickListener userProfileItemClickListener;
	@Inject
	private ToastShower toastShower;
	@Inject
	private ActivityLauncher activityLauncher;
	
	public String getUserId() {
		return userId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		controller.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
	}

	public void showFollowerProfiles(UserProfiles followerProfiles) {
		userFollowersList.setAdapter(new UserProfileAdapter(
				getApplicationContext(), followerProfiles, stringUtils.photosApiRoot()));
		userFollowersList.setOnItemClickListener(userProfileItemClickListener);
		userFollowersLoader.setVisibility(View.GONE);
	}

	public void closeActivityAndShowToastForFail() {
		toastShower.showLoadingUserFollowersFailed();
		finish();
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
	
}
