package com.whereabts.activity;

import java.io.Serializable;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import android.accounts.Account;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.MainActivityController;
import com.whereabts.util.DialogUtils;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.dialog.ChooseAccountDialog;
import com.whereabts.widget.dialog.NewVersionRequiredDialog;
import com.whereabts.widget.dialog.WhereabtsOneActionDialog;
import com.whereabts.widget.dialog.ChooseAccountDialog.ChooseAccountDialogListener;
import com.whereabts.widget.dialog.NewVersionRequiredDialog.NewVersionRequiredDialogListener;
import com.whereabts.widget.dialog.WhereabtsOneActionDialog.WhereabtsOneActionDialogListener;

@ContentView(R.layout.activity_main)
public class MainActivity extends RoboActivity 
	implements WhereabtsOneActionDialogListener, ChooseAccountDialogListener, NewVersionRequiredDialogListener {

	@Inject
	private MainActivityController controller;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private StringUtils stringUtils;
	
	private DialogFragment errorDialog;
	private DialogFragment chooseAccountDialog;
	private DialogFragment updateRequiredDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller.onCreate(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		controller.onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	public void launchMapActivity(boolean appInitialized, boolean googlePlayIsAvailable) {
		if(appInitialized && googlePlayIsAvailable){
			activityLauncher.launchMapActivityToTop();
			finish();
			overridePendingTransition(R.anim.slideout, R.anim.slideup);
		}
	}

	public void showGooglePlayDialog() {
		int serviceAvailabilityCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
		Dialog dialog = GooglePlayServicesUtil.getErrorDialog(serviceAvailabilityCode, this, 0);
		if(dialog != null){
			dialog.show();
		}
	}
	
	public void showDialogForError() {
		errorDialog = DialogUtils.getAppInitializationFailedDialog(stringUtils);
		errorDialog.show(getFragmentManager(), WhereabtsOneActionDialog.TAG);
	}
	
	public void showNewVersionRequiredDialog() {
		updateRequiredDialog = new NewVersionRequiredDialog();
		updateRequiredDialog.show(getFragmentManager(), NewVersionRequiredDialog.TAG);
	}

	@Override
	public void onWhereabtsOneActionDialogButtonClicked() {
		if(errorDialog != null){
			errorDialog.dismiss();
		}
		finish();
	}

	@Override
	public void onWhereabtsOneActionDialogCancel() {
		finish();
	}

	public void showChooseAccountDialog(List<Account> accounts) {
		chooseAccountDialog = new ChooseAccountDialog();
		Bundle args = new Bundle();
		args.putSerializable(ChooseAccountDialog.ITEMS_ARG, (Serializable) accounts);
		chooseAccountDialog.setArguments(args);
		chooseAccountDialog.show(getFragmentManager(), ChooseAccountDialog.TAG);
	}

	public void dismissDialogs() {
		if(chooseAccountDialog != null){
			chooseAccountDialog.dismiss();
		}
		if(updateRequiredDialog != null){
			updateRequiredDialog.dismiss();
		}
	}

	@Override
	public void onAccountSelected(ChooseAccountDialog dialog, Account account) {
		dialog.dismiss();
		controller.registerUser(account);
	}

	@Override
	public void onAccountSelectionCanceled(ChooseAccountDialog dialog) {
		dialog.dismiss();
		finish();
	}

	@Override
	public void onNewVersionSelected(NewVersionRequiredDialog dialog) {
		dialog.dismiss();
		finish();
		activityLauncher.launchWhereabtsGooglePlayStoreSite();
	}

	@Override
	public void onNewVersionCanceled(NewVersionRequiredDialog dialog) {
		dialog.dismiss();
		finish();
	}
		
}
