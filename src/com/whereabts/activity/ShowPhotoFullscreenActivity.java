package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.os.Bundle;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.whereabts.R;

@ContentView(R.layout.activity_show_photo_fullscreen)
public class ShowPhotoFullscreenActivity extends RoboActivity {

	private static final String TAG = "com.whereabts.activity.ShowPhotoFullscreenActivity";
	public static final String EXTRAS_PHOTO_URI = TAG + ".photoUri";
	
	@InjectExtra(value = EXTRAS_PHOTO_URI)
	private String photoUri;
	
	@InjectView(R.id.photo_fullscreen_image)
	private ImageView photoView;
	
	private ImageLoader imageLoader;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		imageLoader = ImageLoader.getInstance();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		loadPhotoToView();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		imageLoader = null;
	}

	private void loadPhotoToView() {
		imageLoader.displayImage(photoUri, photoView, imageOptions());
	}

	private DisplayImageOptions imageOptions() {
		return new DisplayImageOptions.Builder()
			.cacheOnDisc(true)
			.showStubImage(R.drawable.loading_image).build();
	}
	
}
