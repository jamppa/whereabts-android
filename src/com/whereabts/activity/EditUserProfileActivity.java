package com.whereabts.activity;

import java.util.Arrays;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.whereabts.R;
import com.whereabts.adapter.CountryAdapter;
import com.whereabts.controller.EditUserProfileController;
import com.whereabts.model.Country;
import com.whereabts.model.UserProfile;
import com.whereabts.util.BundleUtils;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.dialog.ChoosePhotoDialog;
import com.whereabts.widget.dialog.ChoosePhotoDialog.ChoosePhotoDialogListener;

@ContentView(R.layout.activity_profile_details)
public class EditUserProfileActivity extends RoboActivity implements ChoosePhotoDialogListener {

	@Inject
	private EditUserProfileController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ActivityLauncher activityLauncher;
	
	@InjectView(R.id.profile_details_container)
	private View profileDetailsContainer;
	
	@InjectView(R.id.profile_details_loader)
	private View profileDetailsLoader;
	
	@InjectView(R.id.profile_pic)
	public ImageView profilePic;
	
	@InjectView(R.id.profile_nick)
	public EditText nickField;
	
	@InjectView(R.id.profile_bio)
	public EditText bioField;
	
	@InjectView(R.id.profile_country)
	public Spinner countrySpinner;
	
	private MenuItem saveProfileMenuItem = null;
	private CountryAdapter countryAdapter = null;
	private Uri profilePhotoUri = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		profilePhotoUri = (Uri) BundleUtils.getParcelable(savedInstanceState, "profilePhotoUri", profilePhotoUri);
		enableHomeButton();
		setupCountrySpinner();
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		profilePhotoUri = null;
		countrySpinner.setAdapter(null);
		countryAdapter = null;
		saveProfileMenuItem = null;
		controller.onDestroy();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		controller.onStop();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState);
		outState.putParcelable("profilePhotoUri", profilePhotoUri);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_profile_details, menu);
		saveProfileMenuItem = menu.findItem(R.id.activity_profile_details_save);
		controller.onCreateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}
	
	public void onProfileDetailsSaveClicked(MenuItem item){
		controller.saveProfileDetails();
	}
	
	public void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public void setupCountrySpinner() {
		countryAdapter = new CountryAdapter(getApplicationContext(), Country.listOfCountries());
		countrySpinner.setAdapter(countryAdapter);
	}

	public void showUserProfile() {
		profileDetailsLoader.setVisibility(View.GONE);
		profileDetailsContainer.setVisibility(View.VISIBLE);
		enableSaveProfileMenuItem(true);
	}

	public void fillUserProfileDetails(UserProfile profile) {
		countrySpinner.setSelection(countryAdapter.getPositionByLocale(profile.country()));
		nickField.setText(profile.nick());
		bioField.setText(profile.bio());
	}
	
	public void showUserProfileLoader() {
		hideKeyboard();
		profileDetailsLoader.setVisibility(View.VISIBLE);
		profileDetailsContainer.setVisibility(View.GONE);
		enableSaveProfileMenuItem(false);
	}

	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		if(getCurrentFocus() != null){
			imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
		}
	}

	public List<EditText> requiredFields() {
		return Arrays.asList(nickField);
	}

	public boolean enableSaveProfileMenuItem(boolean enabled) {
		if(saveProfileMenuItem != null){
			saveProfileMenuItem.setEnabled(enabled);
		}
		return enabled;
	}

	public void showLongToastWithText(String text) {
		Toast.makeText(getApplicationContext(), text, Toast.LENGTH_LONG).show();
	}
	
	public void onProfilePicClicked(View view){
		showChoosePhotoDialog();
	}

	public void showChoosePhotoDialog() {
		ChoosePhotoDialog dialog = new ChoosePhotoDialog();
		dialog.show(getFragmentManager(), ChoosePhotoDialog.TAG);
	}

	@Override
	public void onCameraActionSelected(ChoosePhotoDialog dialog) {
		dialog.dismiss();
		controller.onCameraActionSelected();
	}

	public void launchCamera(Uri profilePhotoUri) {
		this.profilePhotoUri = profilePhotoUri;
		activityLauncher.launchCameraActivity(profilePhotoUri, this);
	}

	@Override
	public void onGalleryActionSelected(ChoosePhotoDialog dialog) {
		dialog.dismiss();
		activityLauncher.launchGalleryActivity(this);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			if(requestCode == 0){
				showProfilePhoto(profilePhotoUri);
			}
			else if(requestCode == 1){
				showProfilePhoto(data.getData());
			}
		}
	}

	private void showProfilePhoto(final Uri profilePhotoUri) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.displayImage(profilePhotoUri.toString(), profilePic, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				controller.profilePhotoTaken(profilePhotoUri, loadedImage);
			}
		});
		profilePic.setScaleType(ScaleType.CENTER_CROP);
	}

	public void showProfilePhoto(Bitmap profilePhoto) {
		profilePic.setImageBitmap(profilePhoto);
		profilePic.setScaleType(ScaleType.CENTER_CROP);
	}
	
	public void showProfilePhoto(UserProfile profile) {
		profile.showPhoto(profilePic, stringUtils.photosApiRoot());
	}
}
