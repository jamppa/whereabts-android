package com.whereabts.activity;

import roboguice.activity.RoboActivity;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.adapter.ReplyItemAdapter;
import com.whereabts.controller.ShowMessageController;
import com.whereabts.model.Message;
import com.whereabts.model.Reply;
import com.whereabts.model.newly.NewReply;
import com.whereabts.util.DialogUtils;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.MessagePhotoClickListener;
import com.whereabts.widget.dialog.ReplyMessageDialog;
import com.whereabts.widget.dialog.ReplyMessageDialog.ReplyMessageDialogListener;
import com.whereabts.widget.dialog.WhereabtsTwoActionDialog;
import com.whereabts.widget.dialog.WhereabtsTwoActionDialog.WhereabtsTwoActionDialogListener;

public class ShowMessageActivity extends RoboActivity 
	implements WhereabtsTwoActionDialogListener, ReplyMessageDialogListener, OnClickListener, OnItemClickListener {

	public static final String TAG = "com.whereabts.activity.ShowMessageActivity";
	public static final String EXTRAS_MESSAGE_ID = TAG + ".messageId";
	public static final String EXTRAS_MESSAGE = TAG + ".message";
	public static final int REQUEST_MESSAGE_DELETED = 1;
	
	@Inject
	private ShowMessageController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private MessagePhotoClickListener messagePhotoClickListener;
	
	@InjectView(R.id.msg_details_loader)
	private View messageDetailsLoader;
	@InjectView(R.id.msg_details_list)
	private ListView messageDetailsList;
	
	@InjectExtra(value = EXTRAS_MESSAGE_ID)
	private String messageId;
	
	private View messageDetails;
	private ImageView messagePhotoView;
	private MenuItem discardMessageMenuItem;
	private MenuItem replyMessageMenuItem;
	private MenuItem likeMessageMenuItem;
	private WhereabtsTwoActionDialog discardMessageDialog;
	private ReplyMessageDialog replyMessageDialog;
	private ReplyItemAdapter replyItemAdapter;
	
	public String getMessageId() {
		return messageId;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_message);
		messageDetails = getLayoutInflater().inflate(R.layout.message_details, null);
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.onResume();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		controller.onStop();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_show_message, menu);
		discardMessageMenuItem = menu.findItem(R.id.menu_show_message_discard);
		replyMessageMenuItem = menu.findItem(R.id.menu_show_message_reply);
		likeMessageMenuItem = menu.findItem(R.id.menu_show_message_like);
		controller.onCreateOptionsMenu();
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.saveViewState(outState);
	}

	public void showMessageDetailsLoader() {
		setMenuItemsEnabled(false);
		messageDetailsList.setVisibility(View.GONE);
		messageDetailsLoader.setVisibility(View.VISIBLE);
	}

	public void setMenuItemsEnabled(boolean isEnabled) {
		if(discardMessageMenuItem != null){
			discardMessageMenuItem.setEnabled(isEnabled);
		}
		if(replyMessageMenuItem != null){
			replyMessageMenuItem.setEnabled(isEnabled);
		}
	}

	public void showMessageDetails(Message message) {
		setMenuItemsEnabled(true);
		messageDetailsLoader.setVisibility(View.GONE);
		messageDetailsList.setVisibility(View.VISIBLE);
		showMessage(message);
	}
	
	private void showMessage(Message message) {
		messageDetailsList.removeHeaderView(messageDetails);
		showUserProfilePhoto(message);
		showMessageContent(message);
		showMessagePhoto(message);
		messageDetailsList.addHeaderView(messageDetails);
		replyItemAdapter = new ReplyItemAdapter(getApplicationContext(), message.replies(), stringUtils.photosApiRoot());
		messageDetailsList.setAdapter(replyItemAdapter);
		messageDetailsList.setOnItemClickListener(this);
	}

	private void showMessagePhoto(Message message) {
		if(message.hasPhoto()){
			messagePhotoView = (ImageView)findViewFromMessageDetails(R.id.msg_details_photo);
			messagePhotoView.setOnClickListener(messagePhotoClickListener);
			messagePhotoClickListener.setMessage(message);
			messagePhotoView.setVisibility(View.VISIBLE);
			message.showPhoto(messagePhotoView, stringUtils.photosApiRoot());
		}
	}

	private void showMessageContent(Message message) {
		findViewFromMessageDetails(R.id.msg_details_user_nick).setOnClickListener(this);
		((TextView)findViewFromMessageDetails(R.id.msg_details_user_nick)).setText(message.userNick());
		((TextView)findViewFromMessageDetails(R.id.msg_content)).setText(message.message());
		((TextView)findViewFromMessageDetails(R.id.msg_details_time)).setText(message.formattedTimestamp(getApplicationContext()));
		((TextView)findViewFromMessageDetails(R.id.msg_details_viewed_count)).setText(message.viewsAsString());
		((TextView)findViewFromMessageDetails(R.id.msg_details_liked_count)).setText(message.likesAsString());
		((TextView)findViewFromMessageDetails(R.id.msg_details_expires_at)).setText(message.expirationTimestamp(getApplicationContext()));
	}

	private void showUserProfilePhoto(Message message) {
		ImageView profilePhoto = (ImageView) findViewFromMessageDetails(R.id.msg_details_profile_pic);
		profilePhoto.setOnClickListener(this);
		message.userProfile().showPhoto(profilePhoto, stringUtils.photosApiRoot());
	}

	public void showHomeButton() {
		getActionBar().setDisplayHomeAsUpEnabled(true);
	}

	public void showToastForError(String reason) {
		Toast.makeText(getApplicationContext(), reason, Toast.LENGTH_LONG).show();
	}

	public void showDiscardMessageMenuItem() {
		if(discardMessageMenuItem != null){
			discardMessageMenuItem.setVisible(true);
		}
	}
	
	public void onDiscardMessageMenuItemClicked(MenuItem item){
		controller.onDiscardMessageMenuItemClicked();
	}
	
	public void onReplyMessageMenuItemClicked(MenuItem item) {
		controller.onReplyMessageMenuItemClicked();
	}
	
	public void onLikeMenuItemClicked(MenuItem item){
		controller.onLikeMenuClicked(item);
	}

	public void showDiscardMessageDialog() {
		discardMessageDialog = DialogUtils.getDiscardMessageDialog(stringUtils);
		discardMessageDialog.show(getFragmentManager(), WhereabtsTwoActionDialog.TAG);
	}

	@Override
	public void onWhereabtsAlertDialogOkClicked() {
		controller.onDiscardMessageDialogOkClicked();
	}

	@Override
	public void onWhereabtsAlertDialogCancelClicked() {}

	public void finishWithMessageDeletedResult(Message currentMessage) {
		Intent intent = new Intent();
		intent.putExtra(EXTRAS_MESSAGE, currentMessage);
		setResult(RESULT_OK, intent);
		finish();
	}

	public void showReplyMessageDialog(Bundle args) {
		replyMessageDialog = DialogUtils.getReplyMessageDialog();
		replyMessageDialog.setArguments(args);
		replyMessageDialog.show(getFragmentManager(), ReplyMessageDialog.TAG);
	}

	@Override
	public void onReplyButtonClicked(ReplyMessageDialog dialog, NewReply reply) {
		dialog.dismiss();
		controller.onReplyMessageDialogSendButtonClicked(reply);
	}

	@Override
	public void onCancelButtonClicked(ReplyMessageDialog dialog) {
		dialog.dismiss();
	}
	
	private View findViewFromMessageDetails(int id){
		return messageDetails.findViewById(id);
	}

	@Override
	public void onClick(View v) {
		controller.onUserClicked();
	}

	public void launchShowUserProfileActivity(String userId) {
		activityLauncher.launchShowUserProfileActivity(userId);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int pos, long id) {
		Reply reply = replyItemAdapter.getItem(pos-1);
		launchShowUserProfileActivity(reply.userId());
	}

	public void disableLikeMenuItem() {
		if(likeMessageMenuItem != null){
			likeMessageMenuItem.setEnabled(false);
		}
	}
	
}
