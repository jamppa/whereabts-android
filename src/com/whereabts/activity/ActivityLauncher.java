package com.whereabts.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.widget.Toast;

import com.google.inject.Inject;
import com.whereabts.util.StringUtils;
import com.whereabts.util.ToastShower;

public class ActivityLauncher {

	@Inject
	private Context context;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ToastShower toastShower;
	
	public void launchShowUserProfileActivity(String userId){
		Intent intent = new Intent(context, ShowUserProfileActivity.class);
		intent.putExtra(ShowUserProfileActivity.EXTRAS_USER_ID, userId);
		context.startActivity(intent);
	}
	
	public void launchEditUserProfileActivity() {
		Intent intent = new Intent(context, EditUserProfileActivity.class);
		context.startActivity(intent);
	}
	
	public void launchEditUserProfileActivityAndShowToast(int resId) {
		launchEditUserProfileActivity();
		Toast.makeText(context, resId, Toast.LENGTH_SHORT).show();
	}
	
	public void launchSendFeedbackActivity() {
		Intent intent = new Intent(context, SendFeedbackActivity.class);
		context.startActivity(intent);
	}
	
	public void launchSettingsActivity() {
		Intent intent = new Intent(context, SettingsActivity.class);
		context.startActivity(intent);
	}
	
	public void launchSystemLocationSettings() {
		Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
		context.startActivity(intent);
	}
	
	public void launchMapActivityToTop() {
		Intent intent = new Intent(context, MapActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		context.startActivity(intent);
	}
	
	public void launchShowMessageActivity(String messageId, Activity launchingActivity) {
		Intent intent = new Intent(launchingActivity, ShowMessageActivity.class);
		intent.putExtra(ShowMessageActivity.EXTRAS_MESSAGE_ID, messageId);
		launchingActivity.startActivityForResult(intent, ShowMessageActivity.REQUEST_MESSAGE_DELETED);
	}
	
	public void launchNewMessageActivity(Location location) {
		Intent intent = new Intent(context, SendMessageActivity.class);
		intent.putExtra(SendMessageActivity.MESSAGE_LOCATION, location);
		context.startActivity(intent);
	}
	
	public void launchCameraActivity(Uri profilePhotoUri, Activity launchingActivity) {
		try{
			Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, profilePhotoUri);
			launchingActivity.startActivityForResult(cameraIntent, 0);
		}
		catch(ActivityNotFoundException e){
			toastShower.showCameraNotSupported();
		}
	}
	
	public void launchGalleryActivity(Activity launchingActivity) {
		Intent pickPhotoIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		pickPhotoIntent.setType("image/*");
		launchingActivity.startActivityForResult(pickPhotoIntent, 1);
	}
	
	public void launchWhereabtsGooglePlayStoreSite() {
		try {
		    context.startActivity(new Intent(Intent.ACTION_VIEW, 
		    		Uri.parse("market://details?id=" + stringUtils.googlePlayAppName())));
		} catch (android.content.ActivityNotFoundException e) {
		    context.startActivity(new Intent(Intent.ACTION_VIEW, 
		    		Uri.parse("http://play.google.com/store/apps/details?id=" + stringUtils.googlePlayAppName())));
		}
	}
	
	public void launchUserFollowersActivity(String userId){
		Intent intent = new Intent(context, UserFollowersActivity.class);
		intent.putExtra(UserFollowersActivity.EXTRAS_USER_ID, userId);
		context.startActivity(intent);
	}

	public void launchUserFollowingActivity(String userId) {
		Intent intent = new Intent(context, UserFollowingActivity.class);
		intent.putExtra(UserFollowingActivity.EXTRAS_USER_ID, userId);
		context.startActivity(intent);
	}
	
	public void launchHomeActivity() {
		Intent intent = new Intent(context, MapActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		context.startActivity(intent);
	}
	
	public void launchFollowingStreamActivity() {
		Intent intent = new Intent(context, FollowingStreamActivity.class);
		context.startActivity(intent);
	}
	
	public void launchPeopleActivity() {
		Intent intent = new Intent(context, PeopleActivity.class);
		context.startActivity(intent);
	}

	public void launchUserWhereaboutsActivity(String userId) {
		Intent intent = new Intent(context, UserStreamActivity.class);
		intent.putExtra(UserStreamActivity.EXTRAS_USER_ID, userId);
		context.startActivity(intent);
	}
	
	public void launchInviteFriendsChooser(Intent inviteIntent, String title) {
		context.startActivity(Intent.createChooser(inviteIntent, title));
	}

	public void launchShowPhotoFullscreenActivity(String photoUri) {
		Intent intent = new Intent(context, ShowPhotoFullscreenActivity.class);
		intent.putExtra(ShowPhotoFullscreenActivity.EXTRAS_PHOTO_URI, photoUri);
		context.startActivity(intent);
	}
}
