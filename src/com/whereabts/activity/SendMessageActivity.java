package com.whereabts.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import roboguice.activity.RoboActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectView;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.Spinner;
import android.widget.Toast;

import com.facebook.UiLifecycleHelper;
import com.google.inject.Inject;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.whereabts.R;
import com.whereabts.adapter.CategoryAdapter;
import com.whereabts.controller.SendMessageController;
import com.whereabts.model.Category;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.service.FacebookSessionService;
import com.whereabts.util.BundleUtils;
import com.whereabts.widget.SendMessageMenu;
import com.whereabts.widget.ShareFacebookButton;

@ContentView(R.layout.activity_new_message)
public class SendMessageActivity extends RoboActivity {

	public static final String MESSAGE_LOCATION = "messageLocation";
	
	@InjectView(R.id.new_msg_message_field)
	private EditText messageField;
	@InjectView(R.id.new_msg_category)
	private Spinner categorySpinner;
	@InjectView(R.id.new_msg_share_fb_btn)
	private Button shareFacebookBtn;
	@InjectView(R.id.new_msg_message_photo)
	private ImageView photoView;
	
	@InjectExtra(value = MESSAGE_LOCATION)
	private Location messageLocation;
	private Uri messagePhotoUri = null;
	
	@Inject
	private SendMessageController controller;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private SendMessageMenu sendMessageMenu;
	@Inject
	private ShareFacebookButton shareFacebookButton;
	@Inject
	private FacebookSessionService facebookSession;
	
	private UiLifecycleHelper uiLifecycleHelper;
	private CategoryAdapter adapter;
	
	public EditText getMessageField() {
		return messageField;
	}
	
	public boolean shareToFacebook() {
		return shareFacebookButton.shareToFacebook();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
		setupCategorySpinner();
		messagePhotoUri = (Uri) BundleUtils.getParcelable(savedInstanceState, "messagePhotoUri", messagePhotoUri);
		uiLifecycleHelper = new UiLifecycleHelper(this, facebookSession);
		uiLifecycleHelper.onCreate(savedInstanceState);
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		uiLifecycleHelper.onResume();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiLifecycleHelper.onDestroy();
		controller.onDestroy();
		messagePhotoUri = null;
		messageLocation = null;
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiLifecycleHelper.onSaveInstanceState(outState);
		controller.onSaveInstanceState(outState, messageLocation);
		outState.putParcelable("messagePhotoUri", messagePhotoUri);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_send_message, menu);
		sendMessageMenu.setMenu(menu);
		shareFacebookButton.setButton(shareFacebookBtn);
		controller.onResume();
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
			activityLauncher.launchHomeActivity();
            return true;
        default:
            return super.onOptionsItemSelected(item);
		}
	}

	public void onSendButtonClicked(MenuItem menuItem) {
		controller.sendButtonClicked(messageLocation);
	}
	
	public void onShareFacebookButtonClicked(View button){
		shareFacebookButton.toggle();
	}
	
	public void onCameraButtonClicked(MenuItem item){
		messagePhotoUri = sendMessageMenu.onCameraClicked(this);
	}

	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	public List<EditText> requiredFields() {
		List<EditText> fields = new ArrayList<EditText>();
		fields.add(messageField);
		return fields;
	}
	
	public List<Spinner> requiredSpinners() {
		List<Spinner> spinners = new ArrayList<Spinner>();
		spinners.add(categorySpinner);
		return spinners;
	}

	public void finishAndShowToastForSendingMessage() {
		Toast.makeText(this, R.string.new_message_sending, Toast.LENGTH_LONG).show();
		finish();
	}

	public void requestMessageFieldFocus() {
		messageField.requestFocus();
	}
	
	private void setupCategorySpinner() {
		adapter = new CategoryAdapter(getApplicationContext(), Arrays.asList(Category.values()));
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		categorySpinner.setAdapter(adapter);
		categorySpinner.setSelection(6);
	}

	public Category getSelectedCategory() {
		return (Category) categorySpinner.getSelectedItem();
	}

	public void enableSendButton() {
		sendMessageMenu.enableSendButton();
	}

	public void disableSendButton() {
		sendMessageMenu.disableSendButton();
	}

	public void showMessage(NewLocationMessage message) {
		messageField.setText(message.messageText());
		categorySpinner.setSelection(adapter.getPosition(message.message().category()));
	}

	public void showButtons(NewLocationMessage message) {
		sendMessageMenu.showItems(message);
		shareFacebookButton.toggleButton(message.shareToFacebook());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK){
			if(requestCode == 0){
				showPhoto(messagePhotoUri);
			}
		}
	}

	public void showPhoto(Uri photoUri) {
		ImageLoader imageLoader = ImageLoader.getInstance();
		imageLoader.displayImage(photoUri.toString(), photoView, new SimpleImageLoadingListener() {
			@Override
			public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
				controller.photoAdded(messagePhotoUri, loadedImage);
			}
		});
		photoView.setScaleType(ScaleType.CENTER_CROP);
	}

	public void showPhoto(Bitmap photoBitmap) {
		photoView.setImageBitmap(photoBitmap);
		photoView.setScaleType(ScaleType.CENTER_CROP);
	}
}
