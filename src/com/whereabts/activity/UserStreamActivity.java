package com.whereabts.activity;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectExtra;
import roboguice.inject.InjectFragment;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.UserStreamController;
import com.whereabts.fragment.UserStreamFragment;
import com.whereabts.fragment.UserStreamFragment.UserStreamFragmentListener;
import com.whereabts.model.CompactMessage;

@ContentView(R.layout.activity_user_stream)
public class UserStreamActivity extends RoboFragmentActivity implements UserStreamFragmentListener {
	
	public static final String TAG = "com.whereabts.activity.UserStreamActivity";
	public static final String EXTRAS_USER_ID = TAG + ".userId";

	@InjectExtra(value = EXTRAS_USER_ID)
	private String userId;
	
	@InjectFragment(R.id.user_stream_map)
	private SupportMapFragment mapFragment;
	@InjectFragment(R.id.user_stream_fragment)
	private UserStreamFragment userStreamFragment;
	
	@Inject
	private UserStreamController controller;
	@Inject
	private ActivityLauncher activityLauncher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
		controller.onCreate(this, savedInstanceState);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		initMap();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		GoogleMap map = getMap();
		map.setOnInfoWindowClickListener(null);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.onDestroy();
	}
	
	private void initMap() {
		GoogleMap map = getMap();
		map.setPadding(0, 100, 0, 0);
		map.setOnInfoWindowClickListener(controller);
	}

	public GoogleMap getMap() {
		return mapFragment.getMap();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			activityLauncher.launchHomeActivity();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(requestCode == ShowMessageActivity.REQUEST_MESSAGE_DELETED){
			if(resultCode == Activity.RESULT_OK){
				userStreamFragment.removeMessage(controller.selectedMessage());
			}
		}
	}
	
	@Override
	public void onMessagesLoaded(CompactMessage first) {
		controller.onMessageSelected(first);
	}

	@Override
	public void onMessageSelected(CompactMessage message) {
		controller.onMessageSelected(message);
	}
	
	@Override
	public String getUserId() {
		return this.userId;
	}
	
	private void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}
}
