package com.whereabts.activity;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import android.app.ActionBar;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.fragment.SendFeedbackFragment;
import com.whereabts.fragment.SendFeedbackFragment.SendFeedbackFragmentListener;

@ContentView(R.layout.activity_send_feedback)
public class SendFeedbackActivity extends RoboFragmentActivity implements SendFeedbackFragmentListener {

	@InjectFragment(R.id.send_feedback_fragment)
	private SendFeedbackFragment sendFeedbackFragment;
	@Inject
	private ActivityLauncher activityLauncher;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		enableHomeButton();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				activityLauncher.launchHomeActivity();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onFeedbackSendButtonClicked() {
		showSendingFeedbackToast();
		finish();
	}
	
	public void enableHomeButton() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
	}

	private void showSendingFeedbackToast() {
		Toast.makeText(this, R.string.send_feedback_sending, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onFeedbackFragmentStarted() {}
	
}
