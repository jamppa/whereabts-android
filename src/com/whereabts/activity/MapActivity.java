package com.whereabts.activity;

import roboguice.activity.RoboFragmentActivity;
import roboguice.inject.ContentView;
import roboguice.inject.InjectFragment;
import roboguice.inject.InjectView;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.controller.MapActivityController;
import com.whereabts.model.CompactMessage;
import com.whereabts.util.DialogUtils;
import com.whereabts.util.MapUtils;
import com.whereabts.util.StringUtils;
import com.whereabts.widget.dialog.WhereabtsTwoActionDialog;
import com.whereabts.widget.dialog.WhereabtsTwoActionDialog.WhereabtsTwoActionDialogListener;
import com.whereabts.widget.drawer.WhereabtsDrawer;

@ContentView(R.layout.activity_map)
public class MapActivity extends RoboFragmentActivity implements WhereabtsTwoActionDialogListener {

	@Inject
	private MapActivityController controller;
	@Inject
	private StringUtils stringUtils;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private WhereabtsDrawer drawer;
	
	@InjectFragment(R.id.map)
	private SupportMapFragment mapFragment;
	@InjectView(R.id.map_progress_bar)
	private ProgressBar progressBar;
	@InjectView(R.id.map_drawer_layout)
	private DrawerLayout drawerLayout;
	@InjectView(R.id.map_left_drawer)
	private ListView drawerView;
	
	private GoogleMap map;
	private WhereabtsTwoActionDialog enableLocationServicesDialog;
	
	public GoogleMap getMap() {
		return map;
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setupMap();
		initDrawer();
		controller.initView(this, savedInstanceState);
	}
	
	private void setupMap() {
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
	}
	
	private void initDrawer() {
		drawer.initDrawer(this, drawerLayout, drawerView);
		getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		drawer.syncState();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		drawer.onConfigurationChanged(newConfig);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(drawer.onOptionsItemSelected(item)){
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean isDrawerOpen  = drawer.isDrawerOpen();
		menu.findItem(R.id.menu_new_message_btn).setVisible(!isDrawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		controller.stopListeningForCameraActions(map);
		controller.stopListeningForSentMessages();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		controller.clearMap();
		drawer.destroyDrawer();
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		controller.listenForCameraActions(map);
		controller.listenForSentMessages();
		drawer.openDrawerIfUpdated();
	}

	public void showEnableLocationServicesDialog() {
		enableLocationServicesDialog = DialogUtils.getEnableLocationServicesDialog(stringUtils);
		enableLocationServicesDialog.show(getFragmentManager(), WhereabtsTwoActionDialog.TAG);
	}

	public void showLocationSettings() {
		activityLauncher.launchSystemLocationSettings();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	public void onMenuNewMessageButtonClicked(MenuItem item) {
		controller.onMenuNewMessageButtonClicked();
	}
	
	public void onMenuFeedbackClicked(MenuItem item) {
		activityLauncher.launchSendFeedbackActivity();
	}
			
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		controller.saveViewState(outState);
	}
	
	@Override
	public void onWhereabtsAlertDialogOkClicked() {
		controller.onEnableLocationServicesDialogOkClicked();
	}

	@Override
	public void onWhereabtsAlertDialogCancelClicked() {
		controller.onEnableLocationServicesDialogCancelClicked();
	}

	public void launchNewMessageActivityWithLocation(Location currentLocation) {
		activityLauncher.launchNewMessageActivity(currentLocation);
	}

	public void showToastWithMessage(int resId) {
		Toast.makeText(this, resId, Toast.LENGTH_LONG).show();
	}

	public void moveCameraToLocation(Location location) {
		MapUtils.moveCameraTo(map, location);
	}
	
	public void launchShowMessageActivity(CompactMessage compactMessage) {
		activityLauncher.launchShowMessageActivity(compactMessage.id(), this);
	}
	
	public void showToastForError(String reason) {
		Toast.makeText(this, reason, Toast.LENGTH_LONG).show();
	}

	public void showActionbarSpinner() {
		progressBar.setVisibility(View.VISIBLE);
	}
	
	public void hideActionbarSpinner() {
		progressBar.setVisibility(View.INVISIBLE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		controller.onActivityResult(requestCode, resultCode, data);
	}
	
}
