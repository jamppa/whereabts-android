package com.whereabts.widget.userprofile;

import android.view.View;

import com.facebook.widget.LoginButton;
import com.google.inject.Inject;
import com.whereabts.model.UserWithProfile;
import com.whereabts.service.FacebookSessionService;

public class FacebookConnectButton {

	@Inject
	private FacebookSessionService facebookSessionService;
	
	private LoginButton fbButton;
	
	public void setButton(LoginButton button){
		this.fbButton = button;
		this.fbButton.setReadPermissions("basic_info");
	}

	public void showIfOwnProfile(UserWithProfile profile) {
		if(profile.isOwnProfile()){
			fbButton.setVisibility(View.VISIBLE);
		}
	}
	
	public void onDestroy() {
		fbButton = null;
	}

}
