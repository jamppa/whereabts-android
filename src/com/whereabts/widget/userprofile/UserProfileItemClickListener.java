package com.whereabts.widget.userprofile;

import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import com.google.inject.Inject;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.model.UserProfile;

public class UserProfileItemClickListener implements OnItemClickListener {

	@Inject
	private ActivityLauncher activityLauncher;
	
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		UserProfile profile = (UserProfile) adapter.getItemAtPosition(pos);
		if(profile != null){
			activityLauncher.launchShowUserProfileActivity(profile.userId());
		}
	}

}
