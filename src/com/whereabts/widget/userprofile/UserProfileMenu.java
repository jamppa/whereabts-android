package com.whereabts.widget.userprofile;

import android.view.Menu;
import android.view.MenuItem;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.model.UserWithProfile;
import com.whereabts.util.ToastShower;

public class UserProfileMenu {

	@Inject
	private ToastShower toastShower;
	
	private MenuItem followMenuItem;
	private MenuItem editMenuItem;
	
	private boolean following = false;
	
	public void setMenu(Menu menu) {
		followMenuItem = menu.findItem(R.id.activity_show_userprofile_follow);
		editMenuItem = menu.findItem(R.id.activity_show_userprofile_edit);
	}
	
	public void onDestroy() {
		followMenuItem = null;
		editMenuItem = null;
	}

	public void showMenuItems(UserWithProfile profile) {
		if(profile.isFollowing()){
			showUnfollowMenuItem();
		}
		else{
			showFollowMenuItem();
		}
		showOwnerControls(profile);
	}
	
	public boolean toggleFollowMenuItem() {
		if(!following){
			showUnfollowMenuItemWithToast();
			return following;
		}
		showFollowMenuItemWithToast();
		return following;
	}

	private void showFollowMenuItemWithToast() {
		showFollowMenuItem();
		toastShower.showUnfollowingToast();
	}

	private void showUnfollowMenuItemWithToast() {
		showUnfollowMenuItem();
		toastShower.showFollowingToast();
	}

	private void showOwnerControls(UserWithProfile profile) {
		if(profile.isOwnProfile()){
			editMenuItem.setVisible(true);
			followMenuItem.setVisible(false);
		}
	}

	private void showFollowMenuItem() {
		following = false;
		followMenuItem.setIcon(R.drawable.ic_follow_person);
		followMenuItem.setTitle(R.string.menu_show_profile_follow);
		followMenuItem.setVisible(true);
	}

	private void showUnfollowMenuItem() {
		following = true;
		followMenuItem.setIcon(R.drawable.ic_unfollow_person);
		followMenuItem.setTitle(R.string.menu_show_profile_unfollow);
		followMenuItem.setVisible(true);
	}
	
	
}
