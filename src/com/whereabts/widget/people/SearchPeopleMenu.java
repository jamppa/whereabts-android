package com.whereabts.widget.people;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.SearchView;

import com.google.inject.Inject;
import com.whereabts.R;

public class SearchPeopleMenu {
	
	@Inject
	private Context context;
	
	private MenuItem searchMenuItem;
	private SearchView searchView;

	public void setMenu(Menu menu, ComponentName componentName){
		this.searchMenuItem = menu.findItem(R.id.menu_search_people);
		this.searchView = (SearchView) searchMenuItem.getActionView();
		configureSearch(componentName);
	}

	private void configureSearch(ComponentName componentName) {
		SearchManager searchManager = (SearchManager) context.getSystemService(Context.SEARCH_SERVICE);
		searchView.setSearchableInfo(searchManager.getSearchableInfo(componentName));
		searchView.setSubmitButtonEnabled(false);
		setBackgroundResource();
	}

	private void setBackgroundResource() {
		int searchPlateId = searchView.getContext().getResources().getIdentifier("android:id/search_plate", null, null);
		if(searchView.findViewById(searchPlateId) != null){
			searchView.findViewById(searchPlateId).setBackgroundResource(R.drawable.textfield_default_holo_dark);
		}
	}

	public void showMenuItems() {
		searchMenuItem.setVisible(true);
	}
	
}
