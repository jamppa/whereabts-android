package com.whereabts.widget;

import com.google.inject.Inject;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.model.Message;
import com.whereabts.util.StringUtils;

import android.view.View;
import android.view.View.OnClickListener;

public class MessagePhotoClickListener implements OnClickListener {

	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private StringUtils stringUtils;
	
	private Message message;
	
	@Override
	public void onClick(View view) {
		if(message != null && message.hasPhoto()) {
			activityLauncher.launchShowPhotoFullscreenActivity(photoUrl());
		}
	}

	private String photoUrl() {
		return message.messagePhotoUrl(stringUtils.photosApiRoot());
	}

	public void setMessage(Message message) {
		this.message = message;
	}

}
