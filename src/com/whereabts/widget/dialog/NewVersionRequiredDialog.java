package com.whereabts.widget.dialog;

import com.whereabts.R;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class NewVersionRequiredDialog extends DialogFragment {
	
	public static final String TAG = "com.whereabts.widget.NewVersionRequiredDialog";
	
	private NewVersionRequiredDialogListener listener;
	private Button upgradeButton;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = initLayout();
		return builder.create();
	}
	
	private Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view = layoutInflater.inflate(R.layout.dialog_new_version_required, null);
		builder.setView(view);
		initView(view);
		return builder;
	}

	private void initView(View view) {
		upgradeButton = (Button) view.findViewById(R.id.new_version_required_dialog_btn);
		upgradeButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onNewVersionSelected(NewVersionRequiredDialog.this);
			}
		});
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		listener.onNewVersionCanceled(NewVersionRequiredDialog.this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (NewVersionRequiredDialogListener) activity;
	}
	
	public interface NewVersionRequiredDialogListener {
		void onNewVersionSelected(NewVersionRequiredDialog dialog);
		void onNewVersionCanceled(NewVersionRequiredDialog dialog);
	}
	
}
