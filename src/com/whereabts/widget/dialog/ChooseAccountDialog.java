package com.whereabts.widget.dialog;

import java.util.List;

import android.accounts.Account;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.whereabts.R;
import com.whereabts.adapter.AccountAdapter;

public class ChooseAccountDialog extends DialogFragment implements OnItemClickListener {

	public static final String TAG = "com.whereabts.widget.ChooseAccountDialog";
	public static final String ITEMS_ARG = TAG + ".selectableAccounts";
	
	private List<Account> selectableAccounts;
	private AccountAdapter accountAdapter;
	private ChooseAccountDialogListener listener;
	
	private ListView list = null;
	
	@SuppressWarnings("unchecked")
	@Override
	public void setArguments(Bundle args) {
		selectableAccounts = (List<Account>) args.getSerializable(ITEMS_ARG);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		accountAdapter = new AccountAdapter(getActivity().getApplicationContext(), selectableAccounts);
		AlertDialog.Builder builder = initLayout();
		return builder.create();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	private Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View v = layoutInflater.inflate(R.layout.dialog_choose_account, null);
		initItems(v);
		builder.setView(v);
		return builder;
	}

	private void initItems(View view) {
		list = (ListView) view.findViewById(R.id.dialog_select_item_list);
		list.setAdapter(accountAdapter);
		list.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
		listener.onAccountSelected(ChooseAccountDialog.this, accountAdapter.getItem(pos));
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		listener.onAccountSelectionCanceled(ChooseAccountDialog.this);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (ChooseAccountDialogListener) activity;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		list.setOnItemClickListener(null);
		accountAdapter = null;
	}
	
	public interface ChooseAccountDialogListener {
		void onAccountSelected(ChooseAccountDialog dialog, Account account);
		void onAccountSelectionCanceled(ChooseAccountDialog dialog);
	}

}
