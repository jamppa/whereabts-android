package com.whereabts.widget.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.whereabts.R;

public class WhereabtsOneActionDialog extends DialogFragment {

	public static final String TAG = "com.whereabts.widget.WhereabtsOneActionDialog";
	public static final String MSG_ARG = TAG + ".message";
	public static final String TITLE_ARG = TAG + ".title";
	public static final String BTN_LABEL_ARG = TAG + ".okButtonLabel";
	
	private Button dialogButton;
	private TextView title;
	private TextView message;
	
	private String dialogButtonLabel = "";
	private String titleText = "";
	private String messageText = "";
	
	private WhereabtsOneActionDialogListener listener;
	
	@Override
	public void setArguments(Bundle args) {
		super.setArguments(args);
		dialogButtonLabel = args.getString(BTN_LABEL_ARG, "Ok");
		titleText = args.getString(TITLE_ARG, "");
		messageText = args.getString(MSG_ARG, "");
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = initLayout();
		initListeners();
		initTextsAndLabels();
		return builder.create();
	}
	
	private void initTextsAndLabels() {
		dialogButton.setText(dialogButtonLabel);
		title.setText(titleText);
		message.setText(messageText);
	}

	private void initListeners() {
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onWhereabtsOneActionDialogButtonClicked();
			}
		});
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		listener.onWhereabtsOneActionDialogCancel();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		listener = (WhereabtsOneActionDialogListener) activity;
	}
	
	private Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view = layoutInflater.inflate(R.layout.alert_dialog_single_button, null);
		builder.setView(view);
		initViews(view);
		return builder;
	}

	private void initViews(View view) {
		dialogButton = (Button) view.findViewById(R.id.alertDialogSingleButtonButton);
		title = (TextView) view.findViewById(R.id.alertDialogSingleButtonTitle);
		message = (TextView) view.findViewById(R.id.alertDialogSingleButtonMessage);
	}
	
	public interface WhereabtsOneActionDialogListener {
		public void onWhereabtsOneActionDialogButtonClicked();
		public void onWhereabtsOneActionDialogCancel();
	}

}
