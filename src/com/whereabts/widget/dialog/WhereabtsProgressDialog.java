package com.whereabts.widget.dialog;

import com.whereabts.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

public class WhereabtsProgressDialog extends DialogFragment {
	
	public static final String TAG = "com.whereabts.widget.WhereabtsProgressDialog";
	public static final String MSG_ARG = TAG + ".progressMessage";
	private String progressMessage;
	
	private ProgressDialogListener listener;
	
	@Override
	public void setArguments(Bundle args) {
		this.progressMessage = args.getString(MSG_ARG);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
		AlertDialog dialog = buildDialog(dialogBuilder);
		dialog.setCanceledOnTouchOutside(false);
		return dialog;
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (ProgressDialogListener) activity;
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		listener.onProgressDialogCanceled(WhereabtsProgressDialog.this);
	}
	
	private AlertDialog buildDialog(AlertDialog.Builder dialogBuilder) {
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.progress_dialog, null);
		dialogBuilder.setView(view);
		TextView msgView = (TextView) view.findViewById(R.id.progressDialogMessage);
		msgView.setText(this.progressMessage);
		return dialogBuilder.create();
	}
	
	public interface ProgressDialogListener {
		void onProgressDialogCanceled(DialogFragment dialog);
	}
	
}
