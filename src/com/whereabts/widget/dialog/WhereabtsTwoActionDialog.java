package com.whereabts.widget.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.whereabts.R;

public class WhereabtsTwoActionDialog extends DialogFragment {
	
	public static final String TAG = "com.whereabts.widget.WhereabtsAlertDialog";
	
	public static final String MSG_ARG = TAG + ".message";
	public static final String TITLE_ARG = TAG + ".title";
	public static final String OK_BTN_LABEL_ARG = TAG + ".okButtonLabel";
	public static final String CANCEL_BTN_LABEL_ARG = TAG + ".cancelButtonLabel";
	
	private WhereabtsTwoActionDialogListener listener;

	private Button okButton;
	private Button cancelButton;
	private String title = "";
	private String message = "";
	private String okButtonLabel = "";
	private String cancelButtonLabel = "";
	
	@Override
	public void setArguments(Bundle args) {
		this.title = args.getString(TITLE_ARG);
		this.message = args.getString(MSG_ARG);
		this.okButtonLabel = args.getString(OK_BTN_LABEL_ARG, "Ok");
		this.cancelButtonLabel = args.getString(CANCEL_BTN_LABEL_ARG, "Nope");
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		restoreLabelsAndTexts(savedInstanceState);
		AlertDialog.Builder builder = initLayout();
		initButtonListeners();
		return builder.create();
	}

	private void restoreLabelsAndTexts(Bundle savedInstanceState) {
		title = getString(savedInstanceState, TITLE_ARG, title);
		message = getString(savedInstanceState, MSG_ARG ,message);
		cancelButtonLabel = getString(savedInstanceState, CANCEL_BTN_LABEL_ARG, cancelButtonLabel);
		okButtonLabel = getString(savedInstanceState, OK_BTN_LABEL_ARG, okButtonLabel);
	}

	private String getString(Bundle savedInstanceState, String key, String def) {
		if(savedInstanceState != null){
			return savedInstanceState.getString(key, def);
		}
		return def;
	}

	private AlertDialog.Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view = layoutInflater.inflate(R.layout.alert_dialog, null);
		builder.setView(view);
		initButtons(view);
		initTexts(view);
		return builder;
	}
	
	private void initTexts(View view) {
		TextView titleView = (TextView) view.findViewById(R.id.alertDialogTitle);
		TextView messageView = (TextView) view.findViewById(R.id.alertDialogMessage);
		titleView.setText(title);
		messageView.setText(message);
	}
	private void initButtons(View view) {
		okButton = (Button) view.findViewById(R.id.alertDialogButtonOk);
		okButton.setText(this.okButtonLabel);
		cancelButton = (Button) view.findViewById(R.id.alertDialogButtonCancel);
		cancelButton.setText(this.cancelButtonLabel);
	}

	private void initButtonListeners() {
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onWhereabtsAlertDialogOkClicked();
				dismiss();
			}
		});
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onWhereabtsAlertDialogCancelClicked();
				dismiss();
			}
		});
	}
	
	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		listener.onWhereabtsAlertDialogCancelClicked();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putString(TITLE_ARG, title);
		outState.putString(MSG_ARG, message);
		outState.putString(OK_BTN_LABEL_ARG, okButtonLabel);
		outState.putString(CANCEL_BTN_LABEL_ARG, cancelButtonLabel);
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (WhereabtsTwoActionDialogListener) activity;
	}
	
	public interface WhereabtsTwoActionDialogListener {
		void onWhereabtsAlertDialogOkClicked();
		void onWhereabtsAlertDialogCancelClicked();
	}
}
