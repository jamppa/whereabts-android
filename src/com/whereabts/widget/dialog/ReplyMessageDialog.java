package com.whereabts.widget.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.Button;
import android.widget.EditText;

import com.whereabts.R;
import com.whereabts.model.Message;
import com.whereabts.model.newly.NewReply;
import com.whereabts.util.BundleUtils;

public class ReplyMessageDialog extends DialogFragment {

	public static final String TAG = "com.whereabts.widget.ReplyMessageDialog";
	public static final String MSG_TO_REPLY_ARG = ".messageToReply";
	
	private ReplyMessageDialogListener listener;
	private Button cancelButton;
	private Button sendButton;
	private EditText replyField;
	
	private Message messageToReply;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		restoreState(savedInstanceState);
		AlertDialog.Builder builder = initLayout();
		return builder.create();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_VISIBLE);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	private void restoreState(Bundle savedInstanceState) {
		this.messageToReply = (Message) BundleUtils.getSerializable(savedInstanceState, MSG_TO_REPLY_ARG, messageToReply);
	}

	@Override
	public void setArguments(Bundle args) {
		this.messageToReply = (Message) args.getSerializable(MSG_TO_REPLY_ARG);
	}
	
	private Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View view = layoutInflater.inflate(R.layout.reply_message_dialog, null);
		builder.setView(view);
		initButtons(view);
		initForm(view);
		return builder;
	}
	
	private void initForm(View view) {
		replyField = (EditText) view.findViewById(R.id.replyMessageDialogReplyField);
		replyField.requestFocus();
	}

	private void initButtons(View view) {
		cancelButton = (Button) view.findViewById(R.id.replyMessageDialogCancelButton);
		sendButton = (Button) view.findViewById(R.id.replyMessageDialogOkButton);
		initButtonListeners();
	}

	private void initButtonListeners() {
		cancelButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				listener.onCancelButtonClicked(ReplyMessageDialog.this);
			}
		});
		
		sendButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				NewReply reply = buildReply();
				if(reply.hasReplyMessage()){
					listener.onReplyButtonClicked(ReplyMessageDialog.this, reply);
				}
			}
		});
	}

	private NewReply buildReply() {
		return new NewReply(
				messageToReply, replyField.getText().toString());
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		listener = (ReplyMessageDialogListener) activity;
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(MSG_TO_REPLY_ARG, messageToReply);
	}

	public interface ReplyMessageDialogListener {
		void onReplyButtonClicked(ReplyMessageDialog dialog, NewReply reply);
		void onCancelButtonClicked(ReplyMessageDialog dialog);
	}
	
}
