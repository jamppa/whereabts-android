package com.whereabts.widget.dialog;

import com.whereabts.R;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ChoosePhotoDialog extends DialogFragment implements OnItemClickListener{
	
	public static final String TAG = "com.whereabts.widget.ChoosePhotoDialog";
	private static final int POS_CAMERA_ACTION = 0;
	private static final int POS_GALLERY_ACTION = 1;
	private ChoosePhotoDialogListener listener;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = initLayout();
		return builder.create();
	}
	
	private Builder initLayout() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater layoutInflater = getActivity().getLayoutInflater();
		View v = layoutInflater.inflate(R.layout.dialog_choose_photo, null);
		initItems(v);
		builder.setView(v);
		return builder;
	}

	private void initItems(View view) {
		ListView list = (ListView) view.findViewById(R.id.dialog_select_photo_action_list);
		list.setAdapter(ArrayAdapter.createFromResource(getActivity(), 
				R.array.photo_actions_array, R.layout.simple_list_item));
		list.setOnItemClickListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		getDialog().setCanceledOnTouchOutside(false);
		return super.onCreateView(inflater, container, savedInstanceState);
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		this.listener = (ChoosePhotoDialogListener) activity;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int pos, long id) {
		if(pos == POS_CAMERA_ACTION){
			listener.onCameraActionSelected(ChoosePhotoDialog.this);
		}
		else if(pos == POS_GALLERY_ACTION){
			listener.onGalleryActionSelected(ChoosePhotoDialog.this);
		}
	}
	
	public interface ChoosePhotoDialogListener {
		void onCameraActionSelected(ChoosePhotoDialog dialog);
		void onGalleryActionSelected(ChoosePhotoDialog dialog);
	}
	
}
