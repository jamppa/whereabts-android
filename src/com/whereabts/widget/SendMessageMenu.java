package com.whereabts.widget;

import java.io.IOException;

import roboguice.util.Strings;
import android.net.Uri;
import android.view.Menu;
import android.view.MenuItem;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.activity.SendMessageActivity;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.util.PhotoUtils;
import com.whereabts.util.ToastShower;

@Singleton
public class SendMessageMenu {

	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private PhotoUtils photoUtils;
	@Inject
	private ToastShower toastShower;
	
	private MenuItem sendMessageMenuItem;
	
	public void setMenu(Menu menu){
		sendMessageMenuItem = menu.findItem(R.id.menu_new_message_send);
	}
	
	public void enableSendButton() {
		sendMessageMenuItem.setEnabled(true);
	}

	public void disableSendButton() {
		sendMessageMenuItem.setEnabled(false);
	}

	public void showItems(NewLocationMessage message) {
		if(!Strings.isEmpty(message.messageText())){
			enableSendButton();
			return;
		}
		disableSendButton();
	}

	public Uri onCameraClicked(SendMessageActivity activity) {
		Uri photoUri = null;
		try {
			photoUri = photoUtils.setupWhereaboutsMessagePhotoFile();
			activityLauncher.launchCameraActivity(photoUri, activity);
		} catch (IOException e) {
			e.printStackTrace();
			toastShower.showWhereaboutsPhotoFileFailed();
		}
		return photoUri;
	}
	
}
