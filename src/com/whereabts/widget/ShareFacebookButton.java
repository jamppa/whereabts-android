package com.whereabts.widget;

import android.widget.Button;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.FacebookSessionService;
import com.whereabts.service.UserService;

public class ShareFacebookButton {

	@Inject
	private FacebookSessionService session;
	@Inject
	private SettingsPreferencesRepository settingsRepository;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private UserService userService;
	
	private Button shareFacebookButton;
	private boolean shareToFacebook = false;
	
	public boolean shareToFacebook() {
		return shareToFacebook;
	}
	
	public void setButton(Button button) {
		this.shareFacebookButton = button;
		setButtonState();
	}

	private void setButtonState() {
		if(!session.hasActiveSession()){
			disableButton();
			return;
		}
		toggleButton(settingsRepository.isShareToFacebookEnabled());
	}

	public void toggleButton(boolean enable) {
		if(enable){
			enableButton();
			return;
		}
		disableButton();
	}

	private void disableButton() {
		shareFacebookButton.setBackgroundResource(R.drawable.fb_disabled);
		shareToFacebook = false;
	}

	private void enableButton() {
		shareFacebookButton.setBackgroundResource(R.drawable.fb_enabled);
		shareToFacebook = true;
	}

	public void toggle() {
		if(!session.hasActiveSession()){
			activityLauncher.launchShowUserProfileActivity(userService.findUser().id());
			return;
		}
		toggleButton(!shareToFacebook);
	}
	
}
