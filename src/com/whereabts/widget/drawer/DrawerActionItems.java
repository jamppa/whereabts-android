package com.whereabts.widget.drawer;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Inject;

public class DrawerActionItems {

	@Inject
	private MeDrawerActionItem meDrawerActionItem;
	@Inject
	private FollowingDrawerActionItem followingDrawerActionItem;
	@Inject
	private PeopleDrawerActionItem peopleDrawerActionItem;
	@Inject
	private SettingsDrawerActionItem settingsDrawerActionItem;
	@Inject
	private InviteDrawerActionItem inviteDrawerActionItem;
	
	public List<DrawerActionItem> allDrawerActionItems() {
		List<DrawerActionItem> items = new ArrayList<DrawerActionItem>();
		items.add(meDrawerActionItem);
		items.add(followingDrawerActionItem);
		items.add(peopleDrawerActionItem);
		items.add(settingsDrawerActionItem);
		items.add(inviteDrawerActionItem);
		return items;
	}
	
}
