package com.whereabts.widget.drawer;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.adapter.DrawerAdapter;

public class WhereabtsDrawer {

	@Inject
	private DrawerActionItems drawerActionItems;
	@Inject
	private DrawerItemClickListener drawerItemClickListener;
	@Inject
	private WhereabtsDrawerUpdater drawerUpdater;
	@Inject
	private Context context;
	
	private Activity activity = null;
	private DrawerLayout drawerLayout = null;
	private ListView drawerView = null;
	private ActionBarDrawerToggle drawerToggle = null;
	
	public void initDrawer(Activity activity, DrawerLayout layout, ListView view){
		this.activity = activity;
		this.drawerLayout = layout;
		this.drawerView = view;
		init();
	}
	
	public void destroyDrawer() {
		this.activity = null;
		this.drawerLayout = null;
		this.drawerView = null;
		this.drawerToggle = null;
	}

	private void init() {
		drawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		drawerView.setAdapter(new DrawerAdapter(context, drawerActionItems.allDrawerActionItems()));
		drawerView.setOnItemClickListener(drawerItemClickListener);
		drawerToggle = new ActionBarDrawerToggle(
						activity, drawerLayout, R.drawable.ic_drawer, 
						R.string.drawer_state_open, R.string.drawer_state_close){
			
			@Override
			public void onDrawerClosed(View drawerView) {
				activity.invalidateOptionsMenu();
			}
			
			@Override
			public void onDrawerOpened(View drawerView) {
				activity.invalidateOptionsMenu();
			}
		};
		drawerLayout.setDrawerListener(drawerToggle);
	}

	public void openDrawerIfUpdated() {
		boolean updated = drawerUpdater.update();
		if(updated){
			drawerLayout.openDrawer(drawerView);
		}
	}

	public void syncState() {
		drawerToggle.syncState();
	}
	
	public void onConfigurationChanged(Configuration newConfig) {
		drawerToggle.onConfigurationChanged(newConfig);
	}
	
	public boolean isDrawerOpen() {
		return drawerLayout.isDrawerVisible(drawerView);
	}
	
	public boolean onOptionsItemSelected(MenuItem item){
		return drawerToggle.onOptionsItemSelected(item);
	}
}
