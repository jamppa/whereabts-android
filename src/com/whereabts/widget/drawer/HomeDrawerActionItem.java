package com.whereabts.widget.drawer;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;

public class HomeDrawerActionItem implements DrawerActionItem {

	@Inject
	private ActivityLauncher activityLauncher;
	
	@Override
	public int labelResource() {
		return R.string.drawer_item_home;
	}

	@Override
	public int iconResource() {
		return R.drawable.ic_map;
	}

	@Override
	public void doAction() {
		activityLauncher.launchMapActivityToTop();
	}

}
