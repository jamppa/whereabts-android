package com.whereabts.widget.drawer;

public interface DrawerActionItem {
	public int labelResource();
	public int iconResource();
	public void doAction();
}
