package com.whereabts.widget.drawer;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import com.google.inject.Inject;

public class WhereabtsDrawerVersion {

	private static final String KEY = "com.whereabts.widget.drawer.VERSION";
	
	@Inject
	private Context context;
	
	public String get() {
		ApplicationInfo appInfo = getAppInfo();
		return appInfo.metaData.getString(KEY);
	}

	private ApplicationInfo getAppInfo() {
		try {
			return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			return new ApplicationInfo();
		}
	}
}
