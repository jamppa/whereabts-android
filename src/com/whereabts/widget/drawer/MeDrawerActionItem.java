package com.whereabts.widget.drawer;

import android.content.Context;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.service.UserService;

public class MeDrawerActionItem implements DrawerActionItem {

	@Inject
	private UserService userService;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private Context context;
	
	@Override
	public int labelResource() {
		return R.string.drawer_item_me;
	}

	@Override
	public int iconResource() {
		return R.drawable.ic_person;
	}

	@Override
	public void doAction() {
		activityLauncher.launchShowUserProfileActivity(userService.findUser().id());
	}

}
