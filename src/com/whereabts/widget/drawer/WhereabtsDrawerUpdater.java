package com.whereabts.widget.drawer;

import com.google.inject.Inject;
import com.whereabts.repo.WhereabtsDrawerRepository;

public class WhereabtsDrawerUpdater {

	@Inject
	private WhereabtsDrawerRepository repository;
	@Inject
	private WhereabtsDrawerVersion version;
	
	public boolean update() {
		if(!version.get().equals(repository.getDrawerVersion())){
			return updateVersion(version.get());
		}
		return false;
	}

	private boolean updateVersion(String newVersion) {
		repository.saveDrawerVersion(newVersion);
		return true;
	}
	
	
}
