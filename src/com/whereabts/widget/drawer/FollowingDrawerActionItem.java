package com.whereabts.widget.drawer;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;

public class FollowingDrawerActionItem implements DrawerActionItem {

	@Inject
	private ActivityLauncher activityLauncher;
	
	@Override
	public int labelResource() {
		return R.string.drawer_item_follow;
	}

	@Override
	public int iconResource() {
		return R.drawable.ic_following_stream;
	}

	@Override
	public void doAction() {
		activityLauncher.launchFollowingStreamActivity();
	}

}
