package com.whereabts.widget.drawer;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.service.InviteFriendsIntentCreateService;
import com.whereabts.util.StringUtils;

public class InviteDrawerActionItem implements DrawerActionItem {

	@Inject
	private InviteFriendsIntentCreateService intentCreateService;
	@Inject
	private ActivityLauncher activityLauncher;
	@Inject
	private StringUtils stringUtils;
	
	@Override
	public int labelResource() {
		return R.string.drawer_item_invite;
	}

	@Override
	public int iconResource() {
		return R.drawable.ic_share;
	}

	@Override
	public void doAction() {
		activityLauncher.launchInviteFriendsChooser(
				intentCreateService.createIntent(), stringUtils.getString(labelResource()));
	}

}
