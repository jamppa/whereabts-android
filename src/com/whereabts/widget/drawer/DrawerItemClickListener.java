package com.whereabts.widget.drawer;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class DrawerItemClickListener implements ListView.OnItemClickListener {

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int pos, long id) {
		DrawerActionItem drawerActionItem = (DrawerActionItem) adapter.getItemAtPosition(pos);
		if(drawerActionItem != null){
			drawerActionItem.doAction();
		}
	}
	
}
