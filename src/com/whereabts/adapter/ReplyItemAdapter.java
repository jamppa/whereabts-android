package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.model.Replies;
import com.whereabts.model.Reply;

public class ReplyItemAdapter extends ArrayAdapter<Reply> {

	private final List<Reply> replies = new ArrayList<Reply>();
	private final String photosApi;
	
	public ReplyItemAdapter(Context context, Replies replies, String photosApi) {
		super(context, R.layout.reply_details, replies.get());
		this.replies.addAll(replies.get());
		this.photosApi = photosApi;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = getLayoutInflater();
			convertView = inflater.inflate(R.layout.reply_details, null);
		}
		return initializedReplyView(convertView, replies.get(position));
	}

	private View initializedReplyView(View replyView, Reply reply) {
		
		ImageView profilePhoto = (ImageView) replyView.findViewById(R.id.reply_details_profile_pic);
		TextView time = (TextView) replyView.findViewById(R.id.reply_details_time);
		TextView nick = (TextView) replyView.findViewById(R.id.reply_details_nick);
		TextView replyMessage = (TextView) replyView.findViewById(R.id.reply_details_message);
		
		reply.userProfile().showPhoto(profilePhoto, photosApi);
		replyMessage.setText(reply.replyMessage());
		time.setText(reply.prettyTimestamp());
		nick.setText(reply.userNick());
		return replyView;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
		
}
