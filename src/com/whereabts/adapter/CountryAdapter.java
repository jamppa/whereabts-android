package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.model.Country;

public class CountryAdapter extends ArrayAdapter<Country>{

	private final List<Country> countries = new ArrayList<Country>();
	
	public CountryAdapter(Context context, List<Country> countries) {
		super(context, R.layout.spinner_item, countries);
		this.countries.addAll(countries);
	}
	
	public int getPositionByLocale(Locale locale){
		Country country = ofLocale(locale);
		return countries.indexOf(country);
	}

	private Country ofLocale(Locale locale) {
		for(Country each : countries){
			if(each.locale().getCountry().equals(locale.getCountry())){
				return each;
			}
		}
		return countries.get(0);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = getLayoutInflater();
			convertView = inflater.inflate(R.layout.spinner_item, parent, false);
		}
		return initializedView(countries.get(position), convertView);
	}
	
	private View initializedView(Country country, View countryView) {
		if(country != null && countryView != null){
			TextView label = (TextView) countryView;
			label.setText(country.displayCountry());
		}
		return countryView;
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if(row == null)
	     {
	         LayoutInflater inflater = getLayoutInflater();
	         row = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
	     }
		return initializedDropDownView(countries.get(position), row);
	}

	private View initializedDropDownView(Country country, View row) {
		TextView label = (TextView) row;
		label.setText(country.displayCountry());
		return row;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
}
