package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.CompactMessages;

public class CompactMessageAdapter extends ArrayAdapter<CompactMessage> {

	private final List<CompactMessage> messages = new ArrayList<CompactMessage>();
	private final String photosApi;
	
	public CompactMessageAdapter(Context context, CompactMessages messages, String photosApi) {
		super(context, R.layout.compact_message_list_item, messages.get());
		this.messages.addAll(messages.get());
		this.photosApi = photosApi;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = getLayoutInflater();
			convertView = inflater.inflate(R.layout.compact_message_list_item, null);
		}
		return initializedCompactMessageView(convertView, messages.get(position));
	}
	
	public CompactMessage first() {
		return messages.get(0);
	}

	@Override
	public int getCount() {
		return messages.size();
	}
	
	@Override
	public void addAll(Collection<? extends CompactMessage> collection) {
		this.messages.addAll(collection);
		super.addAll(collection);
	}
	
	private View initializedCompactMessageView(View view, CompactMessage message) {
		
		ImageView profilePhoto = (ImageView) view.findViewById(R.id.compact_message_profile_pic);
		TextView time = (TextView) view.findViewById(R.id.compact_message_time);
		TextView nick = (TextView) view.findViewById(R.id.compact_message_nick);
		TextView messageText = (TextView) view.findViewById(R.id.compact_message_text);
		
		message.userProfile().showPhoto(profilePhoto, photosApi);
		time.setText(message.creationTimeAsPretty());
		nick.setText(message.userProfile().nick());
		messageText.setText(message.shortMessage());
		return view;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

}
