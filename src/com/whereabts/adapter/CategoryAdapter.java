package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.model.Category;

public class CategoryAdapter extends ArrayAdapter<Category>{

	private List<Category> categories = new ArrayList<Category>();
	
	public CategoryAdapter(Context context, List<Category> categories) {
		super(context, R.layout.spinner_item, categories);
		this.categories.addAll(categories);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = getLayoutInflater();
		View categoryView = inflateView(parent, inflater);
		return initializedView(categories.get(position), categoryView);
	}

	private View inflateView(ViewGroup parent, LayoutInflater inflater) {
		return inflater.inflate(R.layout.spinner_item, parent, false);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if(row == null)
	     {
	         LayoutInflater inflater = getLayoutInflater();
	         row = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false);
	     }
		TextView label = (TextView) row;
		label.setText(getCategoryTextResource(categories.get(position)));
		return row;
	}
	
	private View initializedView(Category category, View categoryView) {
		if(category != null && categoryView != null){
			TextView label = (TextView) categoryView;
			label.setText(getCategoryTextResource(category));
		}
		return categoryView;
	}
	
	private int getCategoryTextResource(Category category){
		return getContext().getResources().getIdentifier(
				category.categoryKey(), "string", getContext().getPackageName());
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

}
