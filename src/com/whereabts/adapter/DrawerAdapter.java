package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.widget.drawer.DrawerActionItem;

public class DrawerAdapter extends ArrayAdapter<DrawerActionItem>{

	private final List<DrawerActionItem> drawerItems = new ArrayList<DrawerActionItem>();
	
	public DrawerAdapter(Context context, List<DrawerActionItem> drawerItems) {
		super(context, R.layout.drawer_list_item, drawerItems);
		this.drawerItems.addAll(drawerItems);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = getLayoutInflater();
		View drawerItemView = inflater.inflate(R.layout.drawer_list_item, null, false);
		return initializedDrawerItemView(drawerItems.get(position), drawerItemView);
	}

	private View initializedDrawerItemView(DrawerActionItem drawerItem, View drawerItemView) {
		TextView drawerItemText = (TextView) drawerItemView.findViewById(R.id.drawer_list_item_text);
		drawerItemText.setText(drawerItem.labelResource());
		drawerItemText.setCompoundDrawablesWithIntrinsicBounds(drawerItem.iconResource(), 0, 0, 0);
		return drawerItemView;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
}
