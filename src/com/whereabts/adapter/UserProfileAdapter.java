package com.whereabts.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.whereabts.R;
import com.whereabts.model.UserProfile;
import com.whereabts.model.UserProfiles;

public class UserProfileAdapter extends ArrayAdapter<UserProfile>{

	private final List<UserProfile> userProfiles;
	private final String photosApi;
	
	public UserProfileAdapter(Context context, UserProfiles userProfiles, String photosApi) {
		super(context, R.layout.userprofile_list_item, userProfiles.get());
		this.userProfiles = userProfiles.get();
		this.photosApi = photosApi;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null){
			LayoutInflater inflater = getLayoutInflater();
			convertView = inflater.inflate(R.layout.userprofile_list_item, null);
		}
		return initializedView(convertView, userProfiles.get(position));
	}
	
	private View initializedView(View userProfileView, UserProfile userProfile) {
		ImageView pic = (ImageView) userProfileView.findViewById(R.id.userprofile_list_item_pic);
		TextView name = (TextView) userProfileView.findViewById(R.id.userprofile_list_item_nick);
		TextView country = (TextView) userProfileView.findViewById(R.id.userprofile_list_item_country);
		userProfile.showPhoto(pic, photosApi);
		name.setText(userProfile.nick());
		country.setText(userProfile.countryAsString());
		return userProfileView;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
}
