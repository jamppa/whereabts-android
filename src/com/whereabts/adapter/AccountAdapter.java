package com.whereabts.adapter;

import java.util.ArrayList;
import java.util.List;

import com.whereabts.R;

import android.accounts.Account;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class AccountAdapter extends ArrayAdapter<Account>{

	private final List<Account> accounts = new ArrayList<Account>();
	
	public AccountAdapter(Context context, List<Account> accounts) {
		super(context, R.layout.simple_list_item, accounts);
		this.accounts.addAll(accounts);
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = getLayoutInflater();
		View accountView = inflater.inflate(R.layout.simple_list_item, parent, false);
		return initialized(accountView, accounts.get(position));
	}

	private View initialized(View view, Account account) {
		if(view != null && account != null){
			TextView accountName = (TextView) view;
			accountName.setText(account.name);
		}
		return view;
	}

	private LayoutInflater getLayoutInflater() {
		return (LayoutInflater) getContext()
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

}
