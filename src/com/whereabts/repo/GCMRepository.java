package com.whereabts.repo;

import roboguice.util.Strings;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager.NameNotFoundException;

import com.google.inject.Inject;

public class GCMRepository {

	private static final String PREF = "com.whereabts.repo.GCMRepositoryPreferences";
	private static final String PREF_REGISTRATION_ID = "registration_id";
	private static final String PREF_APP_VERSION = "app_version";
	private static final String PREF_EXPIRATION_TIME = "expiration_time_ms";
	
	private static final long EXPIRATION_TIME_MS = 1000 * 3600 * 24 * 7;
	
	@Inject
	private Context context;
	
	public boolean hasValidRegistrationId() {
		return !Strings.isEmpty(getRegistrationId());
	}
	
	public void setRegistrationId(String gcmRegistrationId) {
		saveCurrentAppVersion();
		saveExpirationTime();
		saveGCMRegistrationId(gcmRegistrationId);
	}
	
	private void saveGCMRegistrationId(String gcmRegistrationId) {
		SharedPreferences.Editor edit = getGCMPreferences().edit();
		edit.putString(PREF_REGISTRATION_ID, gcmRegistrationId);
		edit.commit();
	}

	private void saveExpirationTime() {
		SharedPreferences.Editor edit = getGCMPreferences().edit();
		edit.putLong(PREF_EXPIRATION_TIME, System.currentTimeMillis() + EXPIRATION_TIME_MS);
		edit.commit();
	}

	private void saveCurrentAppVersion() {
		SharedPreferences.Editor edit = getGCMPreferences().edit();
		edit.putInt(PREF_APP_VERSION, currentAppVersion());
		edit.commit();
	}

	public String getRegistrationId() {
		if(isRegistrationIdExpired()){
			return "";
		}
		if(isAppUpdated()){
			return "";
		}
		return getGCMPreferences().getString(PREF_REGISTRATION_ID, "");
	}

	private boolean isAppUpdated() {
		int registeredAppVersion = getGCMPreferences().getInt(PREF_APP_VERSION, Integer.MIN_VALUE);
		return registeredAppVersion != currentAppVersion();
	}

	private boolean isRegistrationIdExpired() {
		long expirationTime = getGCMPreferences().getLong(PREF_EXPIRATION_TIME, -1);
		return System.currentTimeMillis() > expirationTime;
	}

	public int currentAppVersion() {
		try {
			return context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0).versionCode;
		} catch (NameNotFoundException e) {
			throw new IllegalArgumentException("Couldn't get package name!");
		}
	}

	private SharedPreferences getGCMPreferences() {
		return context.getSharedPreferences(PREF, Context.MODE_PRIVATE);
	}
	
}
