package com.whereabts.repo;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.inject.Inject;

public class WhereabtsDrawerRepository {

	public static final String PREFS = "whereabts_drawer_repository";
	public static final String PREF_DRAWER_VERSION = "drawer_version";
	
	@Inject
	private Context context;
		
	public void saveDrawerVersion(String version) {
		SharedPreferences.Editor editor = getEditor();
		editor.putString(PREF_DRAWER_VERSION, version);
		editor.commit();
	}
	
	public String getDrawerVersion() {
		return getPreferences().getString(PREF_DRAWER_VERSION, "");
	}
	
	private SharedPreferences.Editor getEditor() {
		return getPreferences().edit();
	}
	
	private SharedPreferences getPreferences() {
		return context.getSharedPreferences(PREFS, 0);
	}
	
}
