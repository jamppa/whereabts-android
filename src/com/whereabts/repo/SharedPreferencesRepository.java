package com.whereabts.repo;

import com.google.inject.Inject;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class SharedPreferencesRepository {

	@Inject
	private Context context;
	
	protected SharedPreferences getPreferences() {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
	
}
