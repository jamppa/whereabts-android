package com.whereabts.repo;

import android.content.Context;

import com.google.inject.Inject;

public class SettingsPreferencesRepository extends SharedPreferencesRepository {
	
	public static final String PREF_KEY_REPLY_NOTIFICATIONS = "pref_key_notifications_reply";
	public static final String PREF_KEY_REPLY_RESPONDENTS_NOTIFICATIONS = "pref_key_notifications_reply_respondents";
	public static final String PREF_KEY_LIKES_NOTIFICATIONS = "pref_key_notifications_likes";
	public static final String PREF_KEY_NEW_FOLLOWER_NOTIFICATIONS = "pref_key_notifications_new_follower";
	public static final String PREF_KEY_SHARE_TO_FACEBOOK = "pref_key_facebook_share";
	
	@Inject
	private Context context;
	
	public boolean isReplyNotificationsEnabled() {
		return getPreferences().getBoolean(PREF_KEY_REPLY_NOTIFICATIONS, true);
	}

	public boolean isReplyRespondentsNotificationsEnabled() {
		return getPreferences().getBoolean(PREF_KEY_REPLY_RESPONDENTS_NOTIFICATIONS, true);
	}
	
	public boolean isLikeNotificationsEnabled() {
		return getPreferences().getBoolean(PREF_KEY_LIKES_NOTIFICATIONS, true);
	}

	public boolean isNewFollowerNotificationsEnabled() {
		return getPreferences().getBoolean(PREF_KEY_NEW_FOLLOWER_NOTIFICATIONS, true);
	}
	
	public boolean isShareToFacebookEnabled() {
		return getPreferences().getBoolean(PREF_KEY_SHARE_TO_FACEBOOK, false);
	}
		
}
