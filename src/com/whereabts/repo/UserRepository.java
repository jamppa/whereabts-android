package com.whereabts.repo;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.inject.Inject;
import com.whereabts.model.User;
import com.whereabts.model.UserProfile;

public class UserRepository {

	public static final String PREFS = "com.whereabts.repo.UserRepositoryPreferences";
	public static final String USER_UUID_PREF = "pref_key_user_uuid";
	public static final String USER_EMAIL_PREF = "pref_key_user_email";
	public static final String USER_PROFILE_ID_PREF = "pref_key_user_profile_id";
	public static final String USER_ID_PREF = "pref_key_user_id";
	
	@Inject
	private Context context;
	
	public void saveUser(User user){
		SharedPreferences.Editor editor = getEditor();
		editor.putString(USER_UUID_PREF, user.userUuId());
		editor.putString(USER_EMAIL_PREF, user.email());
		editor.putString(USER_ID_PREF, user.id());
		editor.commit();
	}
	
	public User getUser() {
		String userId = getPreferences().getString(USER_ID_PREF, "");
		String userUuid = getPreferences().getString(USER_UUID_PREF, "");
		String email = getPreferences().getString(USER_EMAIL_PREF, "");
		return User.newUser(userId, userUuid, email);
	}
	
	public void saveUserProfile(UserProfile profile) {
		SharedPreferences.Editor editor = getEditor();
		editor.putString(USER_PROFILE_ID_PREF, profile.id());
		editor.commit();
	}

	private SharedPreferences.Editor getEditor() {
		return getPreferences().edit();
	}
	
	private SharedPreferences getPreferences() {
		return context.getSharedPreferences(PREFS, 0);
	}
	
}
