package com.whereabts.api.feedback;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.newly.NewFeedback;

public class SendFeedbackApi {
		
	@Inject
	private WhereabtsApi whereabtsApi;
	
	public void sendFeedback(final NewFeedback feedback){
		HttpRequest req = whereabtsApi.post(ApiRoutes.FEEDBACKS_API);
		whereabtsApi.created(req.send(feedback.asJson().toString()));
	}
	
}
