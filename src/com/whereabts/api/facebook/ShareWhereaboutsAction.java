package com.whereabts.api.facebook;

import com.facebook.model.OpenGraphAction;
import com.google.inject.Singleton;
import com.whereabts.model.newly.NewLocationMessage;

@Singleton
public class ShareWhereaboutsAction {

	private static final String ACTION_NS = "whereabts:share";
	
	public OpenGraphAction createForPost(NewLocationMessage message) {
		OpenGraphAction shareAction = OpenGraphAction.Factory.createForPost(ACTION_NS);
		shareAction.setExplicitlyShared(true);
		shareAction.setProperty("whereabouts", "{result=whereaboutsCreated:$.id}");
		shareAction.setMessage(message.messageText());
		return shareAction;
	}
	
}
