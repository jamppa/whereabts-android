package com.whereabts.api.facebook;

import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Session;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.google.inject.Singleton;

@Singleton
public class WhereabtsRequest {
	
	public Request objectPostRequest(Session session, OpenGraphObject object) {
		Request req =  Request.newPostOpenGraphObjectRequest(session, object, null);
		req.setBatchEntryName("whereaboutsCreated");
		return req;
	}
	
	public Request actionPostRequest(Session session, OpenGraphAction action) {
		return Request.newPostOpenGraphActionRequest(session, action, null);
	}
	
	public RequestBatch newRequestBatch() {
		return new RequestBatch();
	}
	
	public void executeRequestBatch(RequestBatch batch) {
		batch.executeAndWait();
	}
}
