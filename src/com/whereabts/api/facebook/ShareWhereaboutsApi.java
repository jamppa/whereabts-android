package com.whereabts.api.facebook;

import com.facebook.RequestBatch;
import com.facebook.Session;
import com.google.inject.Inject;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.service.FacebookSessionService;

public class ShareWhereaboutsApi {

	@Inject
	private FacebookSessionService facebookSession;
	@Inject
	private ShareWhereaboutsAction shareWhereaboutsAction;
	@Inject
	private WhereabtsRequest whereabtsRequest;
	
	public void shareWhereabouts(NewLocationMessage message) {
		RequestBatch requestBatch = whereabtsRequest.newRequestBatch();
		addPostObjectRequestToBatch(requestBatch, facebookSession.getActiveSession(), message);
		addPostActionRequestToBatch(requestBatch, facebookSession.getActiveSession(), message);
		whereabtsRequest.executeRequestBatch(requestBatch);
	}

	private void addPostActionRequestToBatch(RequestBatch requestBatch, Session session, NewLocationMessage message) {
		requestBatch.add(whereabtsRequest.actionPostRequest(session, shareWhereaboutsAction.createForPost(message)));
	}

	private void addPostObjectRequestToBatch(RequestBatch requestBatch, Session session, NewLocationMessage message) {
		requestBatch.add(whereabtsRequest.objectPostRequest(session, message.asOpenGraphObject()));
	}
	
}
