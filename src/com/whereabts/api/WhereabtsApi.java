package com.whereabts.api;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;

import org.json.JSONObject;

import android.graphics.Bitmap;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.api.exception.ApiException;
import com.whereabts.model.User;
import com.whereabts.service.UserService;
import com.whereabts.util.StringUtils;

public class WhereabtsApi {

	@Inject
	private ApiSettings apiSettings;
	@Inject
	private UserService userService;
	@Inject
	private StringUtils stringUtils;
	
	public HttpRequest get(String url) {
		return contentTypeAndAcceptJson(
				withBasic(connectedToNetwork(HttpRequest.get(apiSettings.apiRoot() + url))));
	}
	
	public HttpRequest post(String url) {
		return contentTypeAndAcceptJson(
				withBasic(connectedToNetwork(HttpRequest.post(apiSettings.apiRoot() + url))));
	}
	
	public HttpRequest postPhoto(String url) {
		return contentTypeImageAndAcceptJson(withBasic(
				connectedToNetwork(HttpRequest.post(apiSettings.photosApiRoot() + url))));
	}
	
	public HttpRequest delete(String url){
		return contentTypeAndAcceptJson(
				withBasic(connectedToNetwork(HttpRequest.delete(apiSettings.apiRoot() + url))));

	}
	
	public HttpRequest withBasic(HttpRequest req) {
		User user = userService.findUser();
		return req.basic(user.email(), user.userUuId());
	}
	
	public HttpRequest ok(HttpRequest request) {
		if(!request.ok()){
			throw new ApiException(stringUtils.getString(R.string.api_error));
		}
		return request;
	}
	
	public HttpRequest okOrNotFound(HttpRequest req){
		if(req.notFound() || req.ok()){
			return req;
		}
		throw new ApiException(stringUtils.getString(R.string.api_error));
	}

	public HttpRequest created(HttpRequest request) {
		if(!request.created()){
			throw new ApiException(stringUtils.getString(R.string.api_error));
		}
		return request;
	}
	
	public JSONObject asJson(HttpRequest request) {
		BufferedReader buf = request.bufferedReader("UTF-8");
		try {
			StringBuilder responseBuilder = new StringBuilder();
			String line;
			while((line = buf.readLine()) != null){
				responseBuilder.append(line);
			}
			return new JSONObject(responseBuilder.toString());
		} catch (Exception e) {
			throw new ApiException(stringUtils.getString(R.string.api_error));
		}
		finally {
			try {
				buf.close();
			} catch (IOException e) {
				// ignored
			}
		}
	}
	
	public HttpRequest sendPhotoBitmap(HttpRequest req, Bitmap bitmap) {
		DataOutputStream out = null;
		try {
			out = new DataOutputStream(req.getConnection().getOutputStream());
			bitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
			return req;
		} catch (IOException e) {
			throw new ApiException(stringUtils.getString(R.string.api_error));
		}
		finally {
			try {
				out.close();
			} catch (IOException e) {
				// ignored
			}
		}
	}
	
	private HttpRequest connectedToNetwork(HttpRequest request){
		if(apiSettings.isConnectedToNetwork()){
			return request;
		}
		throw new ApiException(stringUtils.getString(R.string.no_network));
	}
	
	private HttpRequest contentTypeAndAcceptJson(HttpRequest req){
		return req.acceptJson()
				.contentType("application/json")
				.connectTimeout(15000);
	}
	
	private HttpRequest contentTypeImageAndAcceptJson(HttpRequest req) {
		return req.acceptJson()
				.contentType("image/jpeg")
				.connectTimeout(15000);
	}

}
