package com.whereabts.api.user;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.api.exception.ApiException;
import com.whereabts.model.GCMRegistrationId;
import com.whereabts.model.User;

public class UserRegistrationApi {

	@Inject
	private WhereabtsApi api;
	
	public User registerUser(final User user, final String inCountry) {
		HttpRequest req = api.post(ApiRoutes.USER_REGISTRATION_API);
		JSONObject json = api.asJson(
				api.created(req.send(user.asRegistrationJson(inCountry).toString())));
		return fromJson(json);
	}

	public User registerGCMForUser(final User user, GCMRegistrationId id){
		HttpRequest req = api.post(ApiRoutes.GCM_ID_REGISTRATION_API);
		JSONObject json = api.asJson(
				api.created(req.send(id.asJson().toString())));
		return fromJson(json);
	}

	public int findRequiredClientVersion(){
		HttpRequest req = api.get(ApiRoutes.REQUIRED_VERSION_API);
		JSONObject json = api.asJson(api.ok(req));
		return requiredClientVersionFrom(json);
	}
	
	private int requiredClientVersionFrom(JSONObject json) {
		try {
			return json.getInt("version-code");
		} catch (JSONException e) {
			e.printStackTrace();
			throw new ApiException("");
		}
	}
	
	private User fromJson(JSONObject json) {
		try {
			return new User(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
	
}
