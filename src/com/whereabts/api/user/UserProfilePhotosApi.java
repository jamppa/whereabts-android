package com.whereabts.api.user;

import android.graphics.Bitmap;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;

public class UserProfilePhotosApi {

	@Inject
	private WhereabtsApi api;
	
	public void saveUserProfilePhoto(Bitmap profilePhotoBitmap) {
		HttpRequest req = api.postPhoto(ApiRoutes.USER_PROFILE_PHOTOS_API);
		sendPhoto(profilePhotoBitmap, req);
	}

	private void sendPhoto(Bitmap profilePhotoBitmap, HttpRequest req) {
		try {
			api.created(api.sendPhotoBitmap(req, profilePhotoBitmap));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
