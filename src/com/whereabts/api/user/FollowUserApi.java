package com.whereabts.api.user;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.User;
import com.whereabts.model.UserWithProfile;

public class FollowUserApi {
	
	@Inject
	private WhereabtsApi whereabtsApi;

	public void followUser(UserWithProfile userProfile) {
		HttpRequest req = whereabtsApi.post(followUrl(userProfile.user()));
		whereabtsApi.created(req);
	}
	
	public void unFollowUser(UserWithProfile userProfile) {
		HttpRequest req = whereabtsApi.delete(followUrl(userProfile.user()));
		whereabtsApi.ok(req);
	}
	
	private String followUrl(User user) {
		return ApiRoutes.USER_FOLLOW_API.replace(":id", user.id());
	}
}
