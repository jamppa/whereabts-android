package com.whereabts.api.user;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.UserProfiles;

public class UserFollowersApi {

	@Inject
	private WhereabtsApi api;

	public UserProfiles findFollowersOfUser(String userId) {
		HttpRequest req = api.get(userFollowersUrl(userId));
		JSONObject json = api.asJson(api.ok(req));
		return fromJson(json);
	}

	private UserProfiles fromJson(JSONObject json) {	
		try {
			return new UserProfiles(json.getJSONArray("followers"));
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String userFollowersUrl(String userId) {
		return ApiRoutes.USER_FOLLOW_API.replace(":id", userId);
	}
	
}
