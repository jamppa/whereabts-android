package com.whereabts.api.user;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.UserProfile;
import com.whereabts.model.UserWithProfile;
import com.whereabts.model.newly.NewProfileDetails;
import com.whereabts.service.UserService;

public class UserProfilesApi {
	
	@Inject
	private UserService userService;
	@Inject
	private WhereabtsApi api;
	
	public UserProfile findUserProfile() {
		HttpRequest req = api.get(ApiRoutes.USER_PROFILES_API);
		JSONObject json = api.asJson(api.okOrNotFound(req));
		return userProfileFromJson(json);
	}

	public UserProfile saveNewUserProfile(NewProfileDetails profileDetails) {
		HttpRequest req = api.post(ApiRoutes.USER_PROFILES_API);
		JSONObject json = api.asJson(api.created(req.send(profileDetails.asJson().toString())));
		return userProfileFromJson(json);
	}
	
	public UserWithProfile findUserWithProfile(String userId){
		HttpRequest req = api.get(userProfileRoute(userId));
		JSONObject json = api.asJson(api.ok(req));
		return userWithProfileFromJson(json);
	}
	
	private UserWithProfile userWithProfileFromJson(JSONObject json) {
		try {
			return new UserWithProfile(json, userService.findUser());
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private UserProfile userProfileFromJson(JSONObject json) {
		try {
			if(json.has("_id")){
				return new UserProfile(json);
			}
			return UserProfile.newEmpty();
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}
	
	private String userProfileRoute(String userId) {
		return ApiRoutes.USER_PROFILES_API + "/" + userId;
	}
	
}
