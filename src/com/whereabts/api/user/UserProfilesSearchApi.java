package com.whereabts.api.user;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.UserProfiles;

public class UserProfilesSearchApi {

	@Inject
	private WhereabtsApi api;
	
	public UserProfiles findMostRecent() {
		HttpRequest req = api.get(ApiRoutes.RECENT_USER_PROFILES_API);
		JSONObject json = api.asJson(api.ok(req));
		return fromJson(json);
	}
	
	public UserProfiles find(String search) {
		HttpRequest req = api.get(searchUrl(search));
		JSONObject json = api.asJson(api.ok(req));
		return fromJson(json);
	}

	private String searchUrl(String search) {
		try {
			String searchEncoded = URLEncoder.encode(search, "UTF-8");
			return ApiRoutes.USER_PROFILES_SEARCH_API.replace(":search", searchEncoded);
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}

	private UserProfiles fromJson(JSONObject json) {
		try {
			return new UserProfiles(json.getJSONArray("users"));
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

}
