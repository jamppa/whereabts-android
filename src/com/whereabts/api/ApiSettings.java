package com.whereabts.api;

import android.app.Application;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ApiSettings {

	@Inject
	private Application context;
	
	public String apiRoot() {
		ApplicationInfo appInfo = getAppInfo();
		Bundle metaData = appInfo.metaData;
		return metaData.getString("com.whereabts.api.URL");
	}
	
	public String photosApiRoot() {
		ApplicationInfo appInfo = getAppInfo();
		Bundle metaData = appInfo.metaData;
		return metaData.getString("com.whereabts.photosapi.URL");
	}
	
	public boolean isConnectedToNetwork() {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		return networkInfo != null && networkInfo.isConnected();
	}

	private ApplicationInfo getAppInfo() {
		try {
			return context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
		} catch (NameNotFoundException e) {
			return new ApplicationInfo();
		}
	}
	
}
