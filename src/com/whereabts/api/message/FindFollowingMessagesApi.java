package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.CompactMessages;

public class FindFollowingMessagesApi {

	@Inject
	private WhereabtsApi api;
	
	public CompactMessages findFollowingMessagesOlderThan(int skip, long olderThan) {
		HttpRequest req = api.get(followingMessagesOlderThanUrl(skip, olderThan));
		JSONObject json = api.asJson(api.ok(req));
		return fromJson(json);
	}

	private CompactMessages fromJson(JSONObject json) {
		try {
			return new CompactMessages(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String followingMessagesOlderThanUrl(final int skip, final long olderThan) {
		return ApiRoutes.MESSAGES_FOLLOWING_OLDER_THAN
				.replace(":skip", String.valueOf(skip)).replace(":olderThan", String.valueOf(olderThan));
	}
}
