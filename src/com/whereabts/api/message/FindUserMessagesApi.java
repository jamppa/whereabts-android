package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.CompactMessages;

public class FindUserMessagesApi {

	@Inject
	private WhereabtsApi api;
	
	public CompactMessages findOlderThan(String userId, int skip, long olderThan) {
		HttpRequest req = api.get(userMessagesUrl(userId, skip, olderThan));
		JSONObject json = api.asJson(api.ok(req));
		return fromJson(json);
	}

	private CompactMessages fromJson(JSONObject json) {
		try {
			return new CompactMessages(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String userMessagesUrl(String userId, int skip, long olderThan) {
		return ApiRoutes.USER_MESSAGES_API
				.replace(":id", userId)
				.replace(":skip", Integer.toString(skip))
				.replace(":olderThan", Long.toString(olderThan));
	}

}
