package com.whereabts.api.message;

import android.graphics.Bitmap;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;

public class WhereaboutsPhotosApi {

	@Inject
	private WhereabtsApi whereabtsApi;

	public void uploadWhereaboutsPhoto(Bitmap photoBitmap, String whereaboutsId) {
		HttpRequest req = whereabtsApi.postPhoto(whereaboutsPhotoApiRoute(whereaboutsId));
		sendPhoto(req, photoBitmap);
	}

	private void sendPhoto(HttpRequest req, Bitmap photoBitmap) {
		whereabtsApi.created(
				whereabtsApi.sendPhotoBitmap(req, photoBitmap));
	}

	private String whereaboutsPhotoApiRoute(String whereaboutsId) {
		return ApiRoutes.MESSAGES_PHOTOS_API.replace(":id", whereaboutsId);
	}
	
}
