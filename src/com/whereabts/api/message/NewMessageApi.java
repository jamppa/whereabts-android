package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.CompactMessage;
import com.whereabts.model.newly.NewLocationMessage;

public class NewMessageApi {

	@Inject
	private WhereabtsApi api;
	
	public CompactMessage sendMessage(final NewLocationMessage message) {
		HttpRequest newMessageRequest = api.post(ApiRoutes.MESSAGES_API);
		JSONObject json = api.asJson(api.created(newMessageRequest.send(message.asJson().toString())));
		return fromJson(json);
	}

	private CompactMessage fromJson(JSONObject json) {
		try {
			return new CompactMessage(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

}
