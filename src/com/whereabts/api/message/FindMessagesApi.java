package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.BoundingBox;
import com.whereabts.model.CompactMessages;

public class FindMessagesApi {
	
	@Inject
	private WhereabtsApi whereabtsApi;
	
	public CompactMessages findMessagesByBoundingBox(BoundingBox boundingBox) {
		HttpRequest request = whereabtsApi.get(formatRouteFromBoundingBox(boundingBox));
		JSONObject json = whereabtsApi.asJson(whereabtsApi.ok(request));
		return fromJson(json);
	}

	private CompactMessages fromJson(JSONObject json) {
		try {
			return new CompactMessages(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String formatRouteFromBoundingBox(BoundingBox boundingBox) {
		String routeRoot = ApiRoutes.MESSAGES_API;
		return routeRoot + "/" + 
				boundingBox.lowerLeft().longitude() + "/" +
				boundingBox.lowerLeft().latitude() + "/" +
				boundingBox.upperRight().longitude() + "/" +
				boundingBox.upperRight().latitude();
	}
	
}
