package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;
import com.whereabts.model.Reply;
import com.whereabts.model.newly.NewReply;

public class ReplyMessageApi {
	
	@Inject
	private WhereabtsApi api;
	
	public Reply sendReply(final NewReply reply) {
		HttpRequest replyRequest = api.post(formatRoute(reply));
		JSONObject json = api.asJson(
				api.created(replyRequest.send(reply.asJson().toString())));
		return fromJson(json);
	}

	private Reply fromJson(JSONObject json) {
		try {
			return new Reply(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String formatRoute(NewReply reply) {
		Message message = reply.messageToReply();
		return new String(ApiRoutes.MESSAGE_REPLIES_API).replace(":id", message.id());
	}
}
