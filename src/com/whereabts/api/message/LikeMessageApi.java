package com.whereabts.api.message;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;

public class LikeMessageApi {

	@Inject
	private WhereabtsApi whereabtsApi;
	
	public void like(Message message) {
		HttpRequest likeRequest = whereabtsApi.post(likeMessageApiRoute(message));
		whereabtsApi.created(likeRequest);
	}

	private String likeMessageApiRoute(Message message) {
		return new String(ApiRoutes.MESSAGE_LIKES_API).replace(":id", message.id());
	}

}
