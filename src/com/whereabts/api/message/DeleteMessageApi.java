package com.whereabts.api.message;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;

public class DeleteMessageApi {
	
	@Inject
	WhereabtsApi whereabtsApi;
	
	public void deleteMessage(final Message message) {
		HttpRequest deleteRequest = whereabtsApi.delete(deleteMessageUrl(message));
		whereabtsApi.ok(deleteRequest);
	}

	private String deleteMessageUrl(Message message) {
		return ApiRoutes.MESSAGES_API + "/" +  message.id();
	}
}
