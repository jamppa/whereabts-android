package com.whereabts.api.message;

import org.json.JSONException;
import org.json.JSONObject;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;
import com.whereabts.util.StringUtils;

public class FindMessageApi {
		
	@Inject
	private WhereabtsApi whereabtsApi;
	@Inject
	private StringUtils stringUtils;
	
	public Message findMessageBy(String id) {
		HttpRequest findRequest = whereabtsApi.get(messageUrl(id));
		JSONObject json = whereabtsApi.asJson(whereabtsApi.okOrNotFound(findRequest));
		return fromJson(json);
	}
	
	private Message fromJson(JSONObject json){
		try {
			if(!json.has("_id")){
				throw new RuntimeException(stringUtils.getString(R.string.item_not_found));
			}
			return new Message(json);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
	}

	private String messageUrl(String id) {
		return ApiRoutes.MESSAGES_API + "/" + id;
	}
}
