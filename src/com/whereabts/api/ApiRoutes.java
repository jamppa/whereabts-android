package com.whereabts.api;

public class ApiRoutes {
	
	public static final String MESSAGES_API = "/messages";
	public static final String FEEDBACKS_API = "/feedbacks";
	public static final String MESSAGE_REPLIES_API = "/messages/:id/replies";
	public static final String USER_PROFILE_PHOTOS_API = "/img/user";
	public static final String MESSAGES_PHOTOS_API = "/img/whereabouts/:id";
	public static final String NO_USER_PROFILE_PHOTO_API = "/assets/images/profile_silhuette.png";
	public static final String USER_PROFILES_API = "/users";
	public static final String USER_PROFILES_SEARCH_API = "/users_search?search=:search";
	public static final String USER_FOLLOW_API = "/user/:id/followers";
	public static final String USER_FOLLOWING_API = "/user/:id/following";
	public static final String USER_REGISTRATION_API = "/register_user";
	public static final String GCM_ID_REGISTRATION_API = "/register_gcm";
	public static final String REQUIRED_VERSION_API = "/required_client_version";
	public static final String MESSAGE_LIKES_API = "/messages/:id/likes";
	public static final String MESSAGES_FOLLOWING = "/messages/following/:skip";
	public static final String MESSAGES_FOLLOWING_OLDER_THAN = "/messages/following/:skip/:olderThan";
	public static final String RECENT_USER_PROFILES_API = "/recent/users";
	public static final String USER_MESSAGES_API = "/users/:id/messages/:skip/:olderThan";
}
