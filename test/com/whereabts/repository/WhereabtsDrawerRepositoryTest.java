package com.whereabts.repository;

import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.repo.WhereabtsDrawerRepository;

public class WhereabtsDrawerRepositoryTest extends WhereabtsTest {

	@Inject
	private WhereabtsDrawerRepository repository;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		super.setUp();
	}
	
	@Test
	public void shouldSaveDrawerVersionToPreferences() {
		
		repository.saveDrawerVersion("v1");
		
		Assert.assertEquals("v1", repository.getDrawerVersion());
	}
}
