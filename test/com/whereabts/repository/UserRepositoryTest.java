package com.whereabts.repository;

import static junit.framework.Assert.assertEquals;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.model.User;
import com.whereabts.repo.UserRepository;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserRepositoryTest {

	TestGuiceModule  testModule = new TestGuiceModule();
	
	@Inject
	private UserRepository userRepository;
	
	@Before
	public void setUp() {
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldSaveUserToRepository() throws JSONException {
		
		User user = setupUser();
		
		userRepository.saveUser(user);
		User fromRepo = userRepository.getUser();
		
		assertUser(fromRepo);
	}

	private void assertUser(User fromRepo) {
		assertEquals("email", fromRepo.email());
		assertEquals("id", fromRepo.id());
		assertEquals("userUuid", fromRepo.userUuId());
	}

	private User setupUser() throws JSONException {
		return new User(new JSONObject()
			.put("_id", "id")
			.put("email", "email")
			.put("user-uuid", "userUuid")
			.put("profile_id", "profileId"));
	}	
}
