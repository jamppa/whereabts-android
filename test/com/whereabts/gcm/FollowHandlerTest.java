package com.whereabts.gcm;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.app.NotificationManager;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.FollowNotificationService;

public class FollowHandlerTest extends WhereabtsTest {

	@Inject
	private FollowHandler followHandler;
	
	@Mock
	private FollowNotificationService notificationService;
	@Mock
	private NotificationManager notificationManager;
	@Mock
	private SettingsPreferencesRepository settingsRepository;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(NotificationManager.class, notificationManager);
		testModule.addBinding(FollowNotificationService.class, notificationService);
		testModule.addBinding(SettingsPreferencesRepository.class, settingsRepository);
		super.setUp();
	}
	
	@Test
	public void shouldHaveCorrectType() {
		
		String type = followHandler.type();
		
		assertEquals("TYPE_MESSAGE_NEW_FOLLOWER", type);
	}
	
	@Test
	public void shouldNotifyOnNewFollower() {
		
		when(settingsRepository.isNewFollowerNotificationsEnabled()).thenReturn(true);
		
		followHandler.handle(bundle);
		
		verify(notificationService).notify(bundle);
	}
	
	@Test
	public void shouldNotNotifyOnNewFollowerWhenSettingsDisabled() {
		
		when(settingsRepository.isNewFollowerNotificationsEnabled()).thenReturn(false);
		
		followHandler.handle(bundle);
		
		verify(notificationService, Mockito.times(0)).notify(bundle);
	}
	
}
