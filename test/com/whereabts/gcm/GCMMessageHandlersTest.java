package com.whereabts.gcm;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import android.app.NotificationManager;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;

public class GCMMessageHandlersTest extends WhereabtsTest {

	@Inject
	private GCMMessageHandlers handlers;
	@Mock
	private ReplyHandler replyHandler;
	@Mock
	private ReplyRespondentsHandler replyRespondentsHandler;
	@Mock
	private LikeHandler likeHandler;
	@Mock
	private FollowHandler followHandler;
	@Mock
	private NotificationManager notificationManager;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(NotificationManager.class, notificationManager);
		testModule.addBinding(ReplyHandler.class, replyHandler);
		testModule.addBinding(ReplyRespondentsHandler.class, replyRespondentsHandler);
		testModule.addBinding(LikeHandler.class, likeHandler);
		testModule.addBinding(FollowHandler.class, followHandler);
		setupHandlerTypes();
		super.setUp();
	}
	
	private void setupHandlerTypes() {
		when(replyRespondentsHandler.type()).thenReturn("TYPE_MESSAGE_REPLY_RESPONDENTS");
		when(replyHandler.type()).thenReturn("TYPE_MESSAGE_REPLY");
		when(likeHandler.type()).thenReturn("TYPE_MESSAGE_LIKE");
		when(followHandler.type()).thenReturn("TYPE_MESSAGE_NEW_FOLLOWER");
	}

	@Test
	public void shouldHaveReplyHandler() {
			
		boolean hasIt = handlers.hasHandlerWithType("TYPE_MESSAGE_REPLY");
		
		assertTrue(hasIt);
	}
	
	@Test
	public void shouldHaveReplyRespondentsHandler() {
		
		boolean hasIt = handlers.hasHandlerWithType("TYPE_MESSAGE_REPLY_RESPONDENTS");
		
		assertTrue(hasIt);
	}
	
	@Test
	public void shouldHaveLikeHandler() {
		
		boolean hasIt = handlers.hasHandlerWithType("TYPE_MESSAGE_LIKE");
		
		assertTrue(hasIt);
	}
	
	@Test
	public void shouldHaveFollowerHandler() {
		
		boolean hasIt = handlers.hasHandlerWithType("TYPE_MESSAGE_NEW_FOLLOWER");
		
		assertTrue(hasIt);
	}
	
	@Test
	public void shouldNotHaveNonExistingHandler() {
			
		boolean hasIt = handlers.hasHandlerWithType("TYPE_MESSAGE_SOMETHING_WEIRD");
		
		assertFalse(hasIt);
	}
	
	@Test
	public void shouldCallHandlerWithType() {
		
		Bundle bundle = new Bundle();
		
		handlers.callHandler("TYPE_MESSAGE_LIKE", bundle);
		
		verify(likeHandler).handle(bundle);
	}
	
}
