package com.whereabts.gcm;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.app.NotificationManager;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.MessageLikeNotificationService;

public class LikeHandlerTest extends WhereabtsTest {

	@Inject
	private LikeHandler handler;
	@Mock
	private MessageLikeNotificationService notificationService;
	@Mock
	private SettingsPreferencesRepository settingsRepository;
	@Mock
	private NotificationManager notificationManager;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(NotificationManager.class, notificationManager);
		testModule.addBinding(MessageLikeNotificationService.class, notificationService);
		testModule.addBinding(SettingsPreferencesRepository.class, settingsRepository);
		super.setUp();
	}
	
	@Test
	public void shouldHaveCorrectType() {
		
		String type = handler.type();
		
		assertEquals("TYPE_MESSAGE_LIKE", type);
	}
	
	@Test
	public void shouldNotifyOnLikeWhenSettingsEnabled() {
		
		when(settingsRepository.isLikeNotificationsEnabled()).thenReturn(true);
		
		handler.handle(bundle);
		
		verify(notificationService).notify(bundle);
	}
	
	@Test
	public void shouldNotNotifyOnLikeWhenSettingsDisabled() {
		
		when(settingsRepository.isLikeNotificationsEnabled()).thenReturn(false);
		
		handler.handle(bundle);
		
		verify(notificationService, Mockito.times(0)).notify(bundle);
	}
	
}
