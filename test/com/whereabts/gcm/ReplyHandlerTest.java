package com.whereabts.gcm;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.app.NotificationManager;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.repo.SettingsPreferencesRepository;
import com.whereabts.service.MessageReplyNotificationService;

public class ReplyHandlerTest extends WhereabtsTest {

	@Inject
	private ReplyHandler handler;
	@Mock
	private MessageReplyNotificationService notificationService;
	@Mock
	private SettingsPreferencesRepository settingsRepository;
	@Mock
	private NotificationManager notificationManager;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(NotificationManager.class, notificationManager);
		testModule.addBinding(MessageReplyNotificationService.class, notificationService);
		testModule.addBinding(SettingsPreferencesRepository.class, settingsRepository);
		super.setUp();
	}
	
	@Test
	public void shouldNotifyOnHandleWhenSettingsAreEnabled() {
		
		when(settingsRepository.isReplyNotificationsEnabled()).thenReturn(true);
		
		handler.handle(bundle);
		
		verify(notificationService).notify(bundle);
	}
	
	@Test
	public void shouldNotNotifyWhenSettingsAreDisabled() {
		
		when(settingsRepository.isReplyNotificationsEnabled()).thenReturn(false);
		
		handler.handle(bundle);
		
		verify(notificationService, Mockito.times(0)).notify(bundle);
	}
	
	@Test
	public void shouldHaveType() {
		
		String type = handler.type();
		
		assertEquals("TYPE_MESSAGE_REPLY", type);
	}
	
}
