package com.whereabts.model;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserTest {

	private User user;
		
	@Test
	public void shouldCreateNewUserWithEmailAndNewlyGeneratedUuid() {
		
		user = User.newUser("test@man.fi");
		
		assertNewUserHasOnlyUuidAndEmail();
	}
	
	@Test
	public void shouldCreateNewUserWithAllDetails() {
		
		user = User.newUser("id", "userUuid", "email");
		
		assertUser(user);
	}
	
	@Test
	public void shouldCreateNewPublicUser() {
		
		user = User.newPublicUser();
		
		assertNewPublicUser(user);
	}
	
	@Test
	public void shouldKnowWhenTwoUsersAreTheSame() {
		
		user = User.newPublicUser();
		User another = User.newPublicUser();
		
		assertTrue(user.isSameAs(another));
	}
	
	@Test
	public void shouldKnowWhenTowUsersAreNotTheSame() {
		
		user = User.newPublicUser();
		User another = User.newUser("teppo@testman.fi");
		
		assertFalse(user.isSameAs(another));
	}
	
	@Test
	public void shouldKnowIfUserIsPublicUser() {
		
		user = User.newPublicUser();
		
		assertTrue(user.isPublicUser());
	}
	
	@Test
	public void shouldCreateNewUserFromJson() throws JSONException {
		
		user = new User(userJson());
		
		assertUser(user);
	}
	
	@Test
	public void shouldKnowThatUserIsWholeWhenAllDetailsExist() {
		
		user = User.newUser("123", "123-abc", "test@land.fi");
		
		assertTrue(user.isWhole());
	}
	
	@Test
	public void shouldKnowThatUserIsWholeWhenProfileIdIsMissing() {
		
		user = User.newUser("123", "123-abc", "test@land.fi");
		
		assertTrue(user.isWhole());
	}
	
	@Test
	public void shouldKnowThatUserIsNotWholeWhenIdIsMissing() {
		
		user = User.newUser("", "123-abc", "test@land.fi");
		
		assertFalse(user.isWhole());
	}
	
	@Test
	public void shouldKnowThatUserIsNotWholeWhenUserUuidIsMissing() {
		
		user = User.newUser("123", "", "test@land.fi");
		
		assertFalse(user.isWhole());
	}
	
	@Test
	public void shouldKnowThatUserIsNotWholeWhenUserEmailIsMissing() {
		
		user = User.newUser("123", "123-abc", "");
		
		assertFalse(user.isWhole());
	}
	
	@Test
	public void shouldKnowThatUserIsNotWholeWhenAllAreMissing() {
		
		user = User.newUser("", "", "");
		
		assertFalse(user.isWhole());
	}
	
	@Test
	public void shouldReturnUserDefaultNickFromEmail() {
		
		user = User.newUser("", "", "test.guy@testland.fi");
		
		String defaultNick = user.defaultNick();
		
		assertEquals("test.guy", defaultNick);
	}
	
	@Test
	public void shouldReturnUserDefaulfNickFromBrokenEmail() {
		
		user = User.newUser("", "", "test.guy");
		
		String defaultNick = user.defaultNick();
		
		assertEquals("test.guy", defaultNick);
	}
	
	@Test
	public void shouldReturnUserDefaultNickFromOnePartEmail() {
		
		user = User.newUser("", "", "test@gmail.com");
		
		String defaultNick = user.defaultNick();
		
		assertEquals("test", defaultNick);
	}

	@Test
	public void shouldReturnUserAsRegistrationJson() throws JSONException {
		
		user = User.newUser("", "111-aaa", "test@user.fi");
		
		JSONObject registrationJson = user.asRegistrationJson("fi");
		
		assertRegistrationJson(registrationJson);
	}
	
	private void assertRegistrationJson(JSONObject registrationJson) throws JSONException {
		assertEquals("111-aaa", registrationJson.get("user-uuid"));
		assertEquals("test@user.fi", registrationJson.get("email"));
		assertEquals("test", registrationJson.get("nick"));
		assertEquals("fi", registrationJson.get("country"));
		assertEquals("", registrationJson.get("description"));
		assertEquals("", registrationJson.get("photo"));
	}

	private JSONObject userJson() throws JSONException {		
		return new JSONObject().put("_id", "id")
				.put("email", "email")
				.put("user-uuid", "userUuid")
				.put("profile_id", "profileId");
	}

	private void assertNewPublicUser(User user) {
		assertEquals("", user.id());
		assertEquals(User.PUBLIC_EMAIL, user.email());
		assertEquals(User.PUBLIC_USER_UUID, user.userUuId());
	}

	private void assertNewUserHasOnlyUuidAndEmail() {
		assertEquals("test@man.fi", user.email());
		assertNotNull(UUID.fromString(user.userUuId()));
		assertTrue(user.id().isEmpty());
	}
	
	private void assertUser(User user) {
		assertEquals("id", user.id());
		assertEquals("email", user.email());
		assertEquals("userUuid", user.userUuId());
	}
	
}
