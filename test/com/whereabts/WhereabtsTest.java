package com.whereabts;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public abstract class WhereabtsTest {

	public TestGuiceModule testModule = new TestGuiceModule();
	
	@Before
	public void setUp() {
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
