package com.whereabts.api.message;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.newly.NewLocationMessage;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class NewMessageApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private NewMessageApi messageApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest newMessageRequest = HttpRequest.post("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void shouldPostNewMessageToWhereabtsMessagesApi() {
		
		NewLocationMessage message = messageToPost();
		when(whereabtsApi.post("/messages")).thenReturn(newMessageRequest);
		when(whereabtsApi.created(newMessageRequest)).thenReturn(newMessageRequest);
		
		try{
			messageApi.sendMessage(message);
			assertTrue(false);
		}catch(RuntimeException e){
			verifyiMessageSent();
		}
	}

	private void verifyiMessageSent() {
		verify(newMessageRequest).send(Mockito.anyString());
		verify(whereabtsApi).created(newMessageRequest);
		verify(whereabtsApi).asJson(newMessageRequest);
	}

	private NewLocationMessage messageToPost() {
		NewLocationMessage message = mock(NewLocationMessage.class);
		when(message.asJson()).thenReturn(new JSONObject());
		return message;
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
