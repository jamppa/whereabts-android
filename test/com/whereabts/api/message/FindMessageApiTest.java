package com.whereabts.api.message;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class FindMessageApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject 
	private FindMessageApi findMessageApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest findRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldFindMessageFromWhereabtsApi() {
		
		when(whereabtsApi.get(ApiRoutes.MESSAGES_API + "/123abc")).thenReturn(findRequest);
		when(whereabtsApi.ok(findRequest)).thenReturn(findRequest);
		when(whereabtsApi.asJson(findRequest)).thenReturn(new JSONObject());
		
		findMessageApi.findMessageBy("123abc");
		
		verify(whereabtsApi).get(ApiRoutes.MESSAGES_API + "/123abc");
		verify(whereabtsApi).ok(findRequest);
		verify(whereabtsApi).asJson(findRequest);
	}
	
}
