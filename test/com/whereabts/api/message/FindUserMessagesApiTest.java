package com.whereabts.api.message;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class FindUserMessagesApiTest extends WhereabtsTest {

	@Inject
	private FindUserMessagesApi findUserMessagesApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest req = HttpRequest.get("http://whereabts.com");
	
	private long olderThan = System.currentTimeMillis();
	private String userId = "123abc";
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		whereabtsApiWithUserMessages();
		super.setUp();
	}
	
	private void whereabtsApiWithUserMessages() {
		when(whereabtsApi.get("/users/123abc/messages/0/" + olderThan)).thenReturn(req);
		when(whereabtsApi.ok(req)).thenReturn(req);
	}

	@Test
	public void shouldFindUsersMessagesFromWhereabtsApi() {
		try{
			findUserMessagesApi.findOlderThan(userId, 0, olderThan);
			assertFalse(true);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).get("/users/123abc/messages/0/" + olderThan);
			verify(whereabtsApi).ok(req);
			verify(whereabtsApi).asJson(req);
		}
	}
}
