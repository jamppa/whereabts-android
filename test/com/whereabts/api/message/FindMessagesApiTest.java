package com.whereabts.api.message;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.BoundingBox;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class FindMessagesApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private FindMessagesApi findMessagesApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest findRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldFindMessagesFromWhereabtsMessagesApi() {
		
		BoundingBox bbox = 
				new BoundingBox(new LatLngBounds(new LatLng(1, 1), new LatLng(1, 1)));
		when(whereabtsApi.get(ApiRoutes.MESSAGES_API + "/1.0/1.0/1.0/1.0")).thenReturn(findRequest);
		when(whereabtsApi.ok(findRequest)).thenReturn(findRequest);
		
		try{
			findMessagesApi.findMessagesByBoundingBox(bbox);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).get(ApiRoutes.MESSAGES_API + "/1.0/1.0/1.0/1.0");
			verify(whereabtsApi).ok(findRequest);
			verify(whereabtsApi).asJson(findRequest);
		}
	}
	
}
