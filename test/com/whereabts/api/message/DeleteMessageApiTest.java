package com.whereabts.api.message;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class DeleteMessageApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private DeleteMessageApi deleteMessageApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest deleteRequest = HttpRequest.delete("http://whereabts.com/delete");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void shouldSendDeleteRequestToWhereabtsMessagesApi() {
		
		Message messageToDelete = mock(Message.class);
		when(messageToDelete.id()).thenReturn("1");
		when(whereabtsApi.delete(ApiRoutes.MESSAGES_API + "/1")).thenReturn(deleteRequest);
		
		deleteMessageApi.deleteMessage(messageToDelete);
		
		verify(whereabtsApi).ok(deleteRequest);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
