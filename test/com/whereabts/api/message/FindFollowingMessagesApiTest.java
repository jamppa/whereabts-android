package com.whereabts.api.message;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class FindFollowingMessagesApiTest extends WhereabtsTest {

	@Inject
	private FindFollowingMessagesApi api;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest findRequest = HttpRequest.get("http://whereabts.com/some");
	
	private long olderThan = System.currentTimeMillis();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		whereabtsApiWithFollowinMessages();
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		super.setUp();
	}
	
	private void whereabtsApiWithFollowinMessages() {
		when(whereabtsApi.get("/messages/following/5/" + olderThan)).thenReturn(findRequest);
		when(whereabtsApi.ok(findRequest)).thenReturn(findRequest);
	}
	
	@Test
	public void shouldFindFollowingMessagesOlderThanFromWhereabtsApi() {
		try{
			api.findFollowingMessagesOlderThan(5, olderThan);
			assertFalse(true);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).get("/messages/following/5/" + olderThan);
			verify(whereabtsApi).ok(findRequest);
			verify(whereabtsApi).asJson(findRequest);
		}
	}
	
}
