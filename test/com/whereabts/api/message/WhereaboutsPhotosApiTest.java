package com.whereabts.api.message;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import android.graphics.Bitmap;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class WhereaboutsPhotosApiTest extends WhereabtsTest {

	@Inject
	private WhereaboutsPhotosApi whereaboutsPhotosApi;
	
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest postPhotoRequest = HttpRequest.post("http://whereabts.com/img");
	@Spy
	private Bitmap photoBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		super.setUp();
	}
	
	@Test
	public void shouldPostWhereaboutsPhotoToWhereabtsApi() {
		
		when(whereabtsApi.postPhoto("/img/whereabouts/123")).thenReturn(postPhotoRequest);
		when(whereabtsApi.sendPhotoBitmap(postPhotoRequest, photoBitmap)).thenReturn(postPhotoRequest);
		
		whereaboutsPhotosApi.uploadWhereaboutsPhoto(photoBitmap, "123");
		
		verify(whereabtsApi).created(postPhotoRequest);
	}
	
}
