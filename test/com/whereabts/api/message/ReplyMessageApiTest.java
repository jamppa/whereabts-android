package com.whereabts.api.message;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;
import com.whereabts.model.newly.NewReply;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class ReplyMessageApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private ReplyMessageApi replyMessageApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest replyRequest = HttpRequest.post("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldPostNewReplyToWhereabtsRepliesApi() {
		
		NewReply reply = newReply();
		when(whereabtsApi.post("/messages/123/replies")).thenReturn(replyRequest);
		when(whereabtsApi.created(replyRequest)).thenReturn(replyRequest);
		when(whereabtsApi.asJson(replyRequest)).thenReturn(new JSONObject());
		
		try{
			replyMessageApi.sendReply(reply);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).post("/messages/123/replies");
			verify(replyRequest).send(Mockito.anyString());
			verify(whereabtsApi).created(replyRequest);
			verify(whereabtsApi).asJson(replyRequest);
		}
	}

	private NewReply newReply() {
		Message messageToReply = mock(Message.class);
		NewReply reply = mock(NewReply.class);
		when(messageToReply.id()).thenReturn("123");
		when(reply.asJson()).thenReturn(new JSONObject());
		when(reply.messageToReply()).thenReturn(messageToReply);
		return reply;
	}
	
}
