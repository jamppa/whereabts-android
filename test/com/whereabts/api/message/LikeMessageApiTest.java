package com.whereabts.api.message;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Message;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class LikeMessageApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private LikeMessageApi likeMessageApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Mock
	private Message messageToLike;
	@Spy
	private HttpRequest likeRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldPostLikeRequestToWhereabtsMessageApi() {
		
		when(messageToLike.id()).thenReturn("123");
		when(whereabtsApi.post("/messages/123/likes")).thenReturn(likeRequest);
		
		likeMessageApi.like(messageToLike);
		
		verify(whereabtsApi).created(likeRequest);
	}
	
}
