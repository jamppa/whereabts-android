package com.whereabts.api.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.facebook.Request;
import com.facebook.Session;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;

public class WhereabtsRequestTest extends WhereabtsTest {

	@Inject
	private WhereabtsRequest request;
	@Mock
	private Session session;
	@Mock
	private OpenGraphObject object;
	@Mock
	private OpenGraphAction action;
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		super.setUp();
	}
	
	@Test
	public void shouldCreatePostRequestForObject() {
		
		when(object.getType()).thenReturn("asdasd");
		when(object.getTitle()).thenReturn("asdasd");
		when(object.getInnerJSONObject()).thenReturn(new JSONObject());
		
		Request req = request.objectPostRequest(session, object);
		
		assertNotNull(req);
		assertEquals("whereaboutsCreated", req.getBatchEntryName());
	}
	
	@Test
	public void shouldCreatePostRequestForAction() {
		
		when(action.getType()).thenReturn("asdfasd");
		
		Request req = request.actionPostRequest(session, action);
		
		assertNotNull(req);
	}
	
}
