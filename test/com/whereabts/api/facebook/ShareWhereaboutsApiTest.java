package com.whereabts.api.facebook;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.facebook.RequestBatch;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.model.newly.NewLocationMessage;
import com.whereabts.service.FacebookSessionService;

public class ShareWhereaboutsApiTest extends WhereabtsTest {

	@Inject
	private ShareWhereaboutsApi api;
	@Mock
	private FacebookSessionService facebookSession;
	@Mock
	private ShareWhereaboutsAction shareAction;
	@Mock
	private WhereabtsRequest request;
	
	@Mock
	private NewLocationMessage message;
	@Spy
	private RequestBatch requestBatch = new RequestBatch();
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(FacebookSessionService.class, facebookSession);
		testModule.addBinding(ShareWhereaboutsAction.class, shareAction);
		testModule.addBinding(WhereabtsRequest.class, request);
		super.setUp();
	}
	
	@Test
	public void shouldShareWhereaboutsToFacebook() {
		
		when(request.newRequestBatch()).thenReturn(requestBatch);
		
		api.shareWhereabouts(message);
		
		verify(request).executeRequestBatch(requestBatch);
	}
	
}
