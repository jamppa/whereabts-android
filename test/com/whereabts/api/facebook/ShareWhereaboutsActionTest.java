package com.whereabts.api.facebook;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.facebook.model.OpenGraphAction;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.model.newly.NewLocationMessage;

public class ShareWhereaboutsActionTest extends WhereabtsTest {

	@Inject
	private ShareWhereaboutsAction action;
	@Mock
	private NewLocationMessage message;
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		super.setUp();
	}
	
	@Test
	public void shouldCreateShareActionFromMessage() {
		
		OpenGraphAction shareAction = action.createForPost(message);
		
		assertShareAction(message, shareAction);
	}

	private void assertShareAction(NewLocationMessage message, OpenGraphAction shareAction) {
		assertEquals(message.messageText(), shareAction.getMessage());
		assertEquals("{result=whereaboutsCreated:$.id}", shareAction.getProperty("whereabouts"));
		assertTrue(shareAction.getExplicitlyShared());
		assertEquals("whereabts:share", shareAction.getType());
	}
	
}
