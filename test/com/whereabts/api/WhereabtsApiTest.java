package com.whereabts.api;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.exception.ApiException;
import com.whereabts.model.User;
import com.whereabts.service.UserService;
import com.whereabts.util.StringUtils;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class WhereabtsApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private WhereabtsApi whereabtsApi;
	
	@Mock
	private ApiSettings apiSettings;
	@Mock
	private UserService userService;
	@Mock
	private StringUtils stringUtils;
	@Mock
	private HttpRequest request;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(ApiSettings.class, apiSettings);
		testModule.addBinding(UserService.class, userService);
		testModule.addBinding(StringUtils.class, stringUtils);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void shouldCreateGetRequestForTheGivenUrl() {
		
		when(apiSettings.isConnectedToNetwork()).thenReturn(true);
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		HttpRequest req = whereabtsApi.get("/some/url");
		
		assertEquals(req.url().toString(), "http://whereabts.com/some/url");
		assertEquals("GET", req.getConnection().getRequestMethod());
		assertEquals("application/json", req.getConnection().getRequestProperty("Accept"));
	}
	
	@Test
	public void shouldCreateDeleteRequestForTheGivenUrl() {
		
		when(apiSettings.isConnectedToNetwork()).thenReturn(true);
		when(userService.findUser()).thenReturn(User.newPublicUser());
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		
		HttpRequest req = whereabtsApi.delete("/some");
		
		assertEquals("http://whereabts.com/some", req.url().toString());
		assertEquals("DELETE", req.getConnection().getRequestMethod());
	}
	
	@Test
	public void shouldThrowApiExceptionWhenTryingToCreateGetRequestWhileNotConnectedToNetwork() {
		
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		when(apiSettings.isConnectedToNetwork()).thenReturn(false);
		when(stringUtils.getString(R.string.no_network)).thenReturn("no network!");
		
		try{
			whereabtsApi.get("/some");
			assertTrue(false);
		}
		catch(ApiException e){
			Assert.assertEquals("no network!", e.getMessage());
		}
	}
	
	@Test
	public void shouldThrowApiExceptionWhenTryingToCreateDeleteRequestWhileNotConnectedToNetwork() {
		
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		when(apiSettings.isConnectedToNetwork()).thenReturn(false);
		when(stringUtils.getString(R.string.no_network)).thenReturn("no network!");
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		try{
			whereabtsApi.delete("/some");
			assertTrue(false);
		}
		catch(ApiException e){
			Assert.assertEquals("no network!", e.getMessage());
		}
	}
	
	@Test
	public void shouldThrowApiExceptionWhenTryingToCreatePostRequestWhileNotConnectedToNetwork() {
		
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		when(apiSettings.isConnectedToNetwork()).thenReturn(false);
		when(stringUtils.getString(R.string.no_network)).thenReturn("no network!");
		
		try{
			whereabtsApi.post("/some");
			assertTrue(false);
		}
		catch(ApiException e){
			Assert.assertEquals("no network!", e.getMessage());
		}
	}
	
	@Test
	public void shouldCreateRequestWithBasicAuthenticationUsingCurrentUser() {
		
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		whereabtsApi.withBasic(request);
		
		verify(request).basic(User.newPublicUser().email(), User.newPublicUser().userUuId());
	}
	
	@Test
	public void shouldCreatePostRequestForGivenUrl() {
		
		when(apiSettings.isConnectedToNetwork()).thenReturn(true);
		when(apiSettings.apiRoot()).thenReturn("http://whereabts.com");
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		HttpRequest req = whereabtsApi.post("/some/post");
		
		assertEquals(req.url().toString(), "http://whereabts.com/some/post");
		assertEquals("POST", req.getConnection().getRequestMethod());
		assertEquals("application/json", req.getConnection().getRequestProperty("Accept"));
		assertEquals("application/json", req.getConnection().getRequestProperty("Content-Type"));
	}
	
	@Test
	public void shouldVerifyThatRequestIsOk200AndReturnIt() {
		
		when(request.ok()).thenReturn(true);
		
		HttpRequest req = whereabtsApi.ok(request);
		
		assertNotNull(req);
	}
	
	@Test
	public void shouldVerifyThatRequestIsNotOkAndThrowException() {
		
		when(request.ok()).thenReturn(false);
		when(stringUtils.getString(R.string.api_error)).thenReturn("api error message!");
		
		try{
			whereabtsApi.ok(request);
			assertTrue(false);
		}
		catch(ApiException e){
			assertEquals("api error message!", e.getMessage());
		}
	}
	
	@Test
	public void shouldVerifyThatRequestIsCreated201AndReturnIt() {
		
		when(request.created()).thenReturn(true);
		
		HttpRequest verified = whereabtsApi.created(request);
		
		assertNotNull(verified);
	}
	
	@Test(expected = ApiException.class)
	public void shouldVerifyThatRequestIsNotCreatedAndThrowException() {
		
		when(request.created()).thenReturn(false);
		whereabtsApi.created(request);
	}
	
	@Test
	public void shouldVerifyThatRequestIsOkOrNotFoundWithOkAndReturnIt() {
		
		when(request.ok()).thenReturn(true);
		when(request.notFound()).thenReturn(false);
		
		HttpRequest verified = whereabtsApi.okOrNotFound(request);
		
		Assert.assertEquals(request, verified);
	}
	
	@Test
	public void shouldVerifyThatRequestIsOkOrNotFoundWithNotFoundAndReturnIt() {
		
		when(request.ok()).thenReturn(false);
		when(request.notFound()).thenReturn(true);
		
		HttpRequest verified = whereabtsApi.okOrNotFound(request);
		
		Assert.assertEquals(request, verified);
	}
	
	@Test(expected = ApiException.class)
	public void shouldVerifyThatRequestIsNotOkOrNotFoundAndThrowException() {
		
		when(request.ok()).thenReturn(false);
		when(request.notFound()).thenReturn(false);
		
		whereabtsApi.okOrNotFound(request);
	}
	
	@Test
	public void shouldReturnRequestContentAsJson() {
		
		when(request.bufferedReader("UTF-8")).thenReturn(
				new BufferedReader(new InputStreamReader(new ByteArrayInputStream("{}".getBytes()))));
		
		JSONObject json = whereabtsApi.asJson(request);
		
		assertNotNull(json);
	}
	
	@Test
	public void shouldCreatePostRequestForPhotos() {
		
		when(apiSettings.photosApiRoot()).thenReturn("http://whereabts.com/img");
		when(apiSettings.isConnectedToNetwork()).thenReturn(true);
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		HttpRequest req = whereabtsApi.postPhoto("/user");
		
		assertEquals("http://whereabts.com/img/user", req.url().toString());
		assertEquals("POST", req.getConnection().getRequestMethod());
	}
	
	@Test(expected = ApiException.class)
	public void shouldThrowExceptionWhenCreatingPostRequestForPhotosWhileNotConnectedToNetwork() {
	
		when(apiSettings.photosApiRoot()).thenReturn("http://whereabts.com/img");
		when(apiSettings.isConnectedToNetwork()).thenReturn(false);
		
		whereabtsApi.postPhoto("/user");
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
