package com.whereabts.api.feedback;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.newly.NewFeedback;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class SendFeedbackApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private SendFeedbackApi feedbackApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest feedbackPostRequest = HttpRequest.post("http://whereabts.com");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void shouldSendNewFeedbackToWhereabtsFeedbacksApi() {
		
		NewFeedback feedback = new NewFeedback("my feedback!");
		doReturn(feedbackPostRequest).when(whereabtsApi).post(ApiRoutes.FEEDBACKS_API);
		
		feedbackApi.sendFeedback(feedback);
		
		verify(feedbackPostRequest).send(feedback.asJson().toString());
		verify(whereabtsApi).created(feedbackPostRequest);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
