package com.whereabts.api.user;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class UserFollowingApiTest extends WhereabtsTest {

	@Inject
	private UserFollowingApi userFollowingApi;
	@Mock
	private WhereabtsApi api;
	@Spy
	private HttpRequest userFollowingRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsApi.class, api);
		super.setUp();
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldFindFollowingsFromWhereabtsFollowingApi() {
		
		when(api.get("/user/asd/following")).thenReturn(userFollowingRequest);
		when(api.ok(userFollowingRequest)).thenReturn(userFollowingRequest);
		when(api.asJson(userFollowingRequest)).thenReturn(new JSONObject());
		
		userFollowingApi.findFollowingOfUser("asd");
	}
}
