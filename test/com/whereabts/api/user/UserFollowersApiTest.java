package com.whereabts.api.user;

import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class UserFollowersApiTest extends WhereabtsTest {

	@Inject
	private UserFollowersApi userFollowersApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest followersRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		super.setUp();
	}
	
	@Test(expected = RuntimeException.class)
	public void shouldFindFollowersOfUserFromWhereabtsFollowersApi() {
		
		when(whereabtsApi.get("/user/123abc/followers")).thenReturn(followersRequest);
		when(whereabtsApi.ok(followersRequest)).thenReturn(followersRequest);
		when(whereabtsApi.asJson(followersRequest)).thenReturn(new JSONObject());
		
		userFollowersApi.findFollowersOfUser("123abc");
	}
	
}
