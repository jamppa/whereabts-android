package com.whereabts.api.user;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.User;
import com.whereabts.model.UserWithProfile;

public class FollowUserApiTest extends WhereabtsTest {

	@Inject
	private FollowUserApi followUserApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Mock
	private UserWithProfile userProfile;
	@Spy
	private HttpRequest followRequest = HttpRequest.post("http://whereabts.com/test");
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		super.setUp();
	}
	
	@Test
	public void shouldSendPostRequestToWhereabtsFollowUserApi() {
		
		userProfileWithUser();
		when(whereabtsApi.post("/user/123/followers")).thenReturn(followRequest);
		
		followUserApi.followUser(userProfile);
		
		verify(whereabtsApi).post("/user/123/followers");
		verify(whereabtsApi).created(followRequest);
	}
	
	@Test
	public void shoulSendDeleteRequestToWhereabtsUnFollowUserApi() {
		
		userProfileWithUser();
		when(whereabtsApi.delete("/user/123/followers")).thenReturn(followRequest);
		
		followUserApi.unFollowUser(userProfile);
		
		verify(whereabtsApi).delete("/user/123/followers");
		verify(whereabtsApi).ok(followRequest);
	}

	private void userProfileWithUser() {
		User user = User.newUser("123", "", "");
		when(userProfile.user()).thenReturn(user);
	}
	
}
