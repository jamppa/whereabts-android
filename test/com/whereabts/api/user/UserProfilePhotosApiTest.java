package com.whereabts.api.user;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import android.graphics.Bitmap;
import android.net.Uri;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserProfilePhotosApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private UserProfilePhotosApi userProfilePhotosApi;
	
	@Mock
	private WhereabtsApi whereabtsApi;
	@Mock
	private Uri photoUri;
	
	@Spy
	private HttpRequest postPhotoRequest = HttpRequest.post("http://whereabts.com/img");
	@Spy
	private Bitmap photoBitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.RGB_565);
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldPostUserProfilePhotoToWhereabtsPhotosApi() throws Exception {
		
		when(whereabtsApi.postPhoto(ApiRoutes.USER_PROFILE_PHOTOS_API)).thenReturn(postPhotoRequest);
		when(whereabtsApi.sendPhotoBitmap(postPhotoRequest, photoBitmap)).thenReturn(postPhotoRequest);
		
		userProfilePhotosApi.saveUserProfilePhoto(photoBitmap);
		
		verify(whereabtsApi).sendPhotoBitmap(postPhotoRequest, photoBitmap);
		verify(whereabtsApi).created(postPhotoRequest);
	}
	
}
