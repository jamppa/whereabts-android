package com.whereabts.api.user;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.GCMRegistrationId;
import com.whereabts.model.User;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserRegistrationApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private UserRegistrationApi userRegistrationApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	
	@Spy
	private HttpRequest registrationRequest = HttpRequest.post("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldPostRegistrationToWhereabtsUserRegistrationApi() throws JSONException {
		
		when(whereabtsApi.post(ApiRoutes.USER_REGISTRATION_API)).thenReturn(registrationRequest);
		when(whereabtsApi.created(registrationRequest)).thenReturn(registrationRequest);
		when(whereabtsApi.asJson(registrationRequest)).thenReturn(userAsJson());
		
		User registered = userRegistrationApi.registerUser(user(), "fi");
		
		assertNotNull(registered);
		verify(registrationRequest).send(Mockito.anyString());
	}
	
	@Test
	public void shouldPostGoogleCloudMessagingIdRegistrationOfUserToWhereabtsApi() throws JSONException {
		
		when(whereabtsApi.post(ApiRoutes.GCM_ID_REGISTRATION_API)).thenReturn(registrationRequest);
		when(whereabtsApi.created(registrationRequest)).thenReturn(registrationRequest);
		when(whereabtsApi.asJson(registrationRequest)).thenReturn(userAsJson());
		
		User gcmRegistered = userRegistrationApi.registerGCMForUser(user(), new GCMRegistrationId("123ABC"));
		
		assertNotNull(gcmRegistered);
		verify(registrationRequest).send(Mockito.anyString());
	}
	
	@Test
	public void shouldGetRequiredClientVersionFromWhereabtsApi() throws JSONException {
		
		when(whereabtsApi.get(ApiRoutes.REQUIRED_VERSION_API)).thenReturn(registrationRequest);
		when(whereabtsApi.ok(registrationRequest)).thenReturn(registrationRequest);
		when(whereabtsApi.asJson(registrationRequest)).thenReturn(new JSONObject().put("version-code", 1));
		
		int minVersion = userRegistrationApi.findRequiredClientVersion();
		
		assertEquals(1, minVersion);
		verify(whereabtsApi).asJson(registrationRequest);
	}

	private JSONObject userAsJson() throws JSONException {
		return User.newUser("", "", "").asRegistrationJson("fi")
				.put("_id", "123abc").put("profile_id", "abc123");
	}

	private User user() {
		return User.newUser("", "", "");
	}
	
	
}
