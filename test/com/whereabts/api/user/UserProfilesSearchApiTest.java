package com.whereabts.api.user;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.WhereabtsApi;

public class UserProfilesSearchApiTest extends WhereabtsTest {

	@Inject
	private UserProfilesSearchApi api;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Spy
	private HttpRequest searchRequest = HttpRequest.get("http://whereabts.com");
	
	private String search = "my search string";
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		super.setUp();
	}
	
	@Test
	public void shouldFindMostRecentUserProfilesFromWhereabtsApi() {
		when(whereabtsApi.get("/recent/users")).thenReturn(searchRequest);
		when(whereabtsApi.ok(searchRequest)).thenReturn(searchRequest);
		try{
			api.findMostRecent();
			assertFalse(true);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).get("/recent/users");
			verify(whereabtsApi).ok(searchRequest);
		}	
	}
	
	@Test
	public void shouldFindUserProfilesFromWhereabtsApiByGivenSearchString() throws UnsupportedEncodingException {
		String searchEncoded = URLEncoder.encode(search, "UTF-8");
		when(whereabtsApi.get("/users_search?search=" + searchEncoded)).thenReturn(searchRequest);
		when(whereabtsApi.ok(searchRequest)).thenReturn(searchRequest);
		try{
			api.find("my search string");
			assertFalse(true);
		}
		catch(RuntimeException e){
			verify(whereabtsApi).get("/users_search?search=" + searchEncoded);
			verify(whereabtsApi).ok(searchRequest);
		}	
	}
}
