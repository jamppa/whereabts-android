package com.whereabts.api.user;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.ApiRoutes;
import com.whereabts.api.WhereabtsApi;
import com.whereabts.model.Country;
import com.whereabts.model.User;
import com.whereabts.model.UserProfile;
import com.whereabts.model.newly.NewProfileDetails;
import com.whereabts.service.UserService;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserProfilesApiTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private UserProfilesApi userProfilesApi;
	@Mock
	private WhereabtsApi whereabtsApi;
	@Mock
	private UserService userService;
	
	@Spy
	private NewProfileDetails profileDetails = new NewProfileDetails("", Country.currentCountry());
	@Spy
	private HttpRequest profileRequest = HttpRequest.get("http://whereabts.com/some");
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(WhereabtsApi.class, whereabtsApi);
		testModule.addBinding(UserService.class, userService);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldGetOwnUserProfileFromUserProfilesApi() {
		
		when(whereabtsApi.get(ApiRoutes.USER_PROFILES_API)).thenReturn(profileRequest);
		when(whereabtsApi.okOrNotFound(profileRequest)).thenReturn(profileRequest);
		when(whereabtsApi.asJson(profileRequest)).thenReturn(new JSONObject());
		
		UserProfile profile = userProfilesApi.findUserProfile();
		
		assertNotNull(profile);
	}
	
	@Test
	public void shouldPostUserProfileToUserProfilesApi() {
		
		when(whereabtsApi.post(ApiRoutes.USER_PROFILES_API)).thenReturn(profileRequest);
		when(profileRequest.send(Mockito.anyString())).thenReturn(profileRequest);
		when(whereabtsApi.created(profileRequest)).thenReturn(profileRequest);
		when(whereabtsApi.asJson(profileRequest)).thenReturn(new JSONObject());
		
		UserProfile saved = userProfilesApi.saveNewUserProfile(profileDetails);
		
		assertNotNull(saved);
	}
	
	@Test
	public void shouldGetUserProfileOfAUserFromUserProfilesApi() {
		
		when(whereabtsApi.get(ApiRoutes.USER_PROFILES_API + "/123abc")).thenReturn(profileRequest);
		when(whereabtsApi.ok(profileRequest)).thenReturn(profileRequest);
		when(whereabtsApi.asJson(profileRequest)).thenReturn(new JSONObject());
		when(userService.findUser()).thenReturn(User.newPublicUser());
		
		try{
			userProfilesApi.findUserWithProfile("123abc");
		}catch(RuntimeException e){
			verify(whereabtsApi).asJson(profileRequest);
		}
		
	}
	
}
