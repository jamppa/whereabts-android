package com.whereabts;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

import roboguice.RoboGuice;
import roboguice.config.DefaultRoboModule;

import android.app.Application;

import com.google.inject.AbstractModule;
import com.google.inject.Module;
import com.google.inject.util.Modules;
import com.xtremelabs.robolectric.Robolectric;

public class TestGuiceModule extends AbstractModule {
	
	private HashMap<Class<?>, Object> bindings;
	
	public TestGuiceModule() {
		bindings = new HashMap<Class<?>, Object>();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void configure() {
		Set<Entry<Class<?>, Object>> entries = bindings.entrySet();
	    for (Entry<Class<?>, Object> entry : entries) {
	      bind((Class<Object>) entry.getKey()).toInstance(entry.getValue());
	    }
	}
	
	public void addBinding(Class<?> type, Object obj) {
		this.bindings.put(type, obj);
	}
	
	public static void setUp(Object testObj, TestGuiceModule module) {
		Module defaultModule = RoboGuice.newDefaultRoboModule(Robolectric.application);
		Module testModule = Modules.override(defaultModule).with(module);
		RoboGuice.setBaseApplicationInjector(Robolectric.application, RoboGuice.DEFAULT_STAGE, testModule);
		RoboGuice.getInjector(Robolectric.application).injectMembers(testObj);
	}
	
	public static void tearDown() {
		RoboGuice.util.reset();
		Application app = Robolectric.application;
		DefaultRoboModule defaultModule = RoboGuice.newDefaultRoboModule(app);
		RoboGuice.setBaseApplicationInjector(app, RoboGuice.DEFAULT_STAGE, defaultModule);
	}

}
