package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.api.exception.ApiException;
import com.whereabts.api.user.FollowUserApi;
import com.whereabts.model.UserWithProfile;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class FollowUserServiceTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private FollowUserService followUserService;
	@Mock
	private FollowUserApi followUserApi;
	@Mock
	private Intent serviceIntent;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(FollowUserApi.class, followUserApi);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldFollowUser() {
		
		UserWithProfile profile = intentWithUserProfile();
		when(profile.isFollowing()).thenReturn(true);
		
		followUserService.onHandleIntent(serviceIntent);
		
		verify(followUserApi).followUser(profile);
		verify(followUserApi, times(0)).unFollowUser(profile);
	}
	
	@Test
	public void shouldUnfollowUser() {
		
		UserWithProfile profile = intentWithUserProfile();
		when(profile.isFollowing()).thenReturn(false);
		
		followUserService.onHandleIntent(serviceIntent);
		
		verify(followUserApi).unFollowUser(profile);
		verify(followUserApi, times(0)).followUser(profile);
	}
	
	@Test
	public void shouldIgnoreExceptionFromFollowApi() {
		
		UserWithProfile profile = intentWithUserProfile();
		when(profile.isFollowing()).thenReturn(true);
		doThrow(new ApiException("")).when(followUserApi).followUser(profile);
		
		followUserService.onHandleIntent(serviceIntent);
	}

	private UserWithProfile intentWithUserProfile() {
		UserWithProfile profile = mock(UserWithProfile.class);
		when(serviceIntent.getSerializableExtra(FollowUserService.EXTRAS_USER_PROFILE)).thenReturn(profile);
		return profile;
	}
	
}
