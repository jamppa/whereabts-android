package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.user.UserFollowingApi;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.WhereabtsResultReceiver;

public class FindUserFollowingServiceTest extends WhereabtsTest {

	@Inject
	private FindUserFollowingService service;
	@Mock
	private UserFollowingApi userFollowingApi;
	@Mock
	private Intent intent;
	@Mock
	private WhereabtsResultReceiver receiver;
	@Mock
	private UserProfiles profiles;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(UserFollowingApi.class, userFollowingApi);
		super.setUp();
	}
	
	@Test
	public void shouldFindUserFollowingsFromUserFollowingApi() {
		
		intentWithUserIdAndReceiver();
		apiReturningUserProfiles();
		
		service.onHandleIntent(intent);
		
		verify(userFollowingApi).findFollowingOfUser("asd123");
		verify(receiver).send(0, bundle);
	}
	
	@Test
	public void shouldReturnImmediatelyWhenUserIdExtraMissingFromIntent() {
		
		intentWithoutUserId();
		
		service.onHandleIntent(intent);
		
		verify(userFollowingApi, Mockito.times(0)).findFollowingOfUser(null);
	}
	
	@Test
	public void shoudReturnImmediatelyWhenReceiverIsMissingFromIntent() {
		
		intentWithoutReceiver();
		
		service.onHandleIntent(intent);
		
		verify(userFollowingApi, Mockito.times(0)).findFollowingOfUser(Mockito.anyString());
	}
	
	@Test
	public void shouldSendErrorWhenExceptionIsThrownFromApi() {
		
		intentWithUserIdAndReceiver();
		doThrow(new RuntimeException()).when(userFollowingApi).findFollowingOfUser(Mockito.anyString());
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(Mockito.eq(1), Mockito.any(Bundle.class));
	}

	private void intentWithoutReceiver() {
		when(intent.getStringExtra(FindUserFollowingService.EXTRAS_RESULT_RECEIVER)).thenReturn(null);
	}

	private void intentWithoutUserId() {
		when(intent.getStringExtra(FindUserFollowingService.EXTRAS_USER_ID)).thenReturn(null);
	}

	private void apiReturningUserProfiles() {
		when(userFollowingApi.findFollowingOfUser("asd123")).thenReturn(profiles);
		when(profiles.asBundle()).thenReturn(bundle);
	}

	private void intentWithUserIdAndReceiver() {
		when(intent.getStringExtra(FindUserFollowingService.EXTRAS_USER_ID)).thenReturn("asd123");
		when(intent.getParcelableExtra(FindUserFollowingService.EXTRAS_RESULT_RECEIVER)).thenReturn(receiver);
	}
	
}
