package com.whereabts.service.intent;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.facebook.ShareWhereaboutsApi;
import com.whereabts.model.newly.NewLocationMessage;

public class ShareWhereaboutsFacebookServiceTest extends WhereabtsTest {

	@Inject
	private ShareWhereaboutsFacebookService service;
	@Mock
	private ShareWhereaboutsApi api;
	@Mock
	private Intent intent;
	@Mock
	private NewLocationMessage message;
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(ShareWhereaboutsApi.class, api);
		super.setUp();
	}
	
	@Test
	public void shouldShareWhereaboutsToFacebookWithShareWhereaboutsApi() {
		
		intentWithMessage(true);
		
		service.onHandleIntent(intent);
		
		verify(api).shareWhereabouts(message);
	}
	
	@Test
	public void shouldNotShareWhereaboutsToFacebookWhenIntentMessageIsNull() {
		
		intentWithoutMessage();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.never()).shareWhereabouts(null);
	}
	
	@Test
	public void shouldNotShareWhereaboutsToFacebookWhenMessageIsNotSupposedToBeShared() {
		
		intentWithMessage(false);
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.never()).shareWhereabouts(message);
	}

	private void intentWithoutMessage() {
		Mockito.when(intent.getSerializableExtra(
				ShareWhereaboutsFacebookService.EXTRAS_MESSAGE)).thenReturn(null);
	}

	private void intentWithMessage(boolean shareToFacebook) {
		when(intent.getSerializableExtra(
				ShareWhereaboutsFacebookService.EXTRAS_MESSAGE)).thenReturn(message);
		when(message.shareToFacebook()).thenReturn(shareToFacebook);
	}
	
}
