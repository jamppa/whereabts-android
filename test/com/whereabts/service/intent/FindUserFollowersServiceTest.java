package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.user.UserFollowersApi;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.WhereabtsResultReceiver;

public class FindUserFollowersServiceTest extends WhereabtsTest {

	@Inject
	private FindUserFollowersService findUserFollowersService;
	@Mock
	private UserFollowersApi userFollowersApi;
	@Mock
	private Intent intent;
	@Mock
	private WhereabtsResultReceiver resultReceiver;
	@Mock
	private UserProfiles foundProfiles;
	@Mock
	private Bundle foundProfilesAsBundle;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(UserFollowersApi.class, userFollowersApi);
		super.setUp();
	}
	
	@Test
	public void shouldFindUserFollowersFromUserFollowersApi() {
		
		intentWithUserIdAndResultReceiver();
		when(userFollowersApi.findFollowersOfUser("123")).thenReturn(foundProfiles);
		when(foundProfiles.asBundle()).thenReturn(foundProfilesAsBundle);
		
		findUserFollowersService.onHandleIntent(intent);
		
		verify(userFollowersApi).findFollowersOfUser("123");
		verify(resultReceiver).send(0, foundProfilesAsBundle);
	}
	
	@Test
	public void shouldDoNothingWhenExtrasUserIdIsNull() {
		
		intentWithoutUserId();
		
		findUserFollowersService.onHandleIntent(intent);
		
		verify(userFollowersApi, times(0)).findFollowersOfUser(null);
		verify(resultReceiver, times(0)).send(0, null);
	}
	
	@Test
	public void shouldDoNothingWhenExtrasResultReceiverIsNull() {
		
		intentWithoutResultReceiver();
		
		findUserFollowersService.onHandleIntent(intent);
		
		verify(userFollowersApi, times(0)).findFollowersOfUser(null);
	}
	
	@Test
	public void shouldSendErrorWhenExceptionIsThrownWhileFindinUserFollowers() {
		
		intentWithUserIdAndResultReceiver();
		doThrow(new RuntimeException("")).when(userFollowersApi).findFollowersOfUser(Mockito.anyString());
		
		findUserFollowersService.onHandleIntent(intent);
		
		verify(resultReceiver).send(Mockito.eq(1), Mockito.any(Bundle.class));
	}

	private void intentWithoutResultReceiver() {
		when(intent.getParcelableExtra(FindUserFollowersService.EXTRAS_RESULT_RECEIVER)).thenReturn(null);
	}

	private void intentWithoutUserId() {
		when(intent.getStringExtra(FindUserFollowersService.EXTRAS_USER_ID)).thenReturn(null);
	}

	private void intentWithUserIdAndResultReceiver() {
		when(intent.getStringExtra(FindUserFollowersService.EXTRAS_USER_ID)).thenReturn("123");
		when(intent.getParcelableExtra(FindUserFollowersService.EXTRAS_RESULT_RECEIVER)).thenReturn(resultReceiver);
	}
}
