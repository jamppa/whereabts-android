package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.message.FindUserMessagesApi;
import com.whereabts.model.CompactMessages;
import com.whereabts.util.WhereabtsResultReceiver;

public class FindUserMessagesServiceTest extends WhereabtsTest {

	@Inject
	private FindUserMessagesService service;
	@Mock
	private FindUserMessagesApi api;
	@Mock
	private Intent intent;
	@Mock
	private CompactMessages compactMessages;
	@Mock
	private WhereabtsResultReceiver receiver;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(FindUserMessagesApi.class, api);
		super.setUp();
	}
	
	@Test
	public void shouldFindUserMessagesFromWhereabtsApi() {
		
		intentWithAllTheExtras(0);
		apiReturningMessages();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(0, bundle);
	}
	
	@Test
	public void shouldNotFindUserMessagesWhenUserIdIsMissing() {
		
		intentWithUserIdMissing();

		service.onHandleIntent(intent);
		
		verify(api, Mockito.never()).findOlderThan(null, 0, 123123L);
	}
	
	@Test
	public void shouldNotFindUserMessagesWhenReceiverIsMissing() {
		
		intentWithReceiverMissing();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.never()).findOlderThan("abc", 0, 123123L);
	}
	
	@Test
	public void shouldSendErrorToReceiverWhenApiThrowsException() {
		
		intentWithAllTheExtras(0);
		apiThrowingException();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(Mockito.eq(1), Mockito.any(Bundle.class));
	}
	
	@Test
	public void shouldSendEmptyMessagesWhenSkippingIsInLimits() {
		
		intentWithAllTheExtras(250);
		
		service.onHandleIntent(intent);
		
		verify(api, never()).findOlderThan("abc", 250, 123123L);
		verify(receiver).send(Mockito.eq(0), Mockito.any(Bundle.class));
	}

	private void apiThrowingException() {
		doThrow(new RuntimeException("")).when(api).findOlderThan("abc", 0, 123123L);
	}

	private void intentWithReceiverMissing() {
		intentWithAllTheExtras(0);
		when(intent.getParcelableExtra(FindUserMessagesService.EXTRAS_RECEIVER)).thenReturn(null);
	}

	private void intentWithUserIdMissing() {
		intentWithAllTheExtras(0);
		when(intent.getStringExtra(FindUserMessagesService.EXTRAS_USER_ID)).thenReturn(null);
	}

	private void apiReturningMessages() {
		when(api.findOlderThan("abc", 0, 123123L)).thenReturn(compactMessages);
		when(compactMessages.asBundle()).thenReturn(bundle);
	}

	private void intentWithAllTheExtras(int skipping) {
		when(intent.getStringExtra(FindUserMessagesService.EXTRAS_USER_ID)).thenReturn("abc");
		when(intent.getIntExtra(FindUserMessagesService.EXTRAS_SKIP, 0)).thenReturn(skipping);
		when(intent.getParcelableExtra(FindUserMessagesService.EXTRAS_RECEIVER)).thenReturn(receiver);
		when(intent.getLongExtra(Mockito.eq(FindUserMessagesService.EXTRAS_OLDER_THAN), Mockito.anyLong())).thenReturn(123123L);
	}
}
