package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.user.UserProfilesSearchApi;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.WhereabtsResultReceiver;

public class SearchUserProfilesServiceTest extends WhereabtsTest {

	@Inject
	private SearchUserProfilesService service;
	@Mock
	private UserProfilesSearchApi api;
	@Mock
	private Intent intent;
	@Mock
	private UserProfiles foundProfiles;
	@Mock
	private WhereabtsResultReceiver receiver;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(UserProfilesSearchApi.class, api);
		super.setUp();
	}
	
	@Test
	public void shouldSearchUserProfilesFromUserProfilesSearchApiWithGivenSearchString() {
		
		intentWithReceiverAndSearch();
		apiFoundingUserProfilesWithSearch();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(0, bundle);
	}
	
	@Test
	public void shouldSendErrorWhenUserProfilesSearchApiThrowsException() {
		
		intentWithReceiverAndSearch();
		apiThrowingExceptionForSomeReason();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(Mockito.eq(-1), Mockito.any(Bundle.class));
	}
	
	@Test
	public void shouldDoNothingWhenIntentHasNoReceiver() {
		
		intentWithoutReceiver();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.times(0)).find(Mockito.anyString());
	}
	
	@Test
	public void shouldDoNothingWhenIntentHasNoSearch() {
		
		intentWithoutSearch();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.times(0)).find(null);
	}

	private void intentWithoutSearch() {
		when(intent.getParcelableExtra(SearchUserProfilesService.EXTRAS_RESULT_RECEIVER))
			.thenReturn(receiver);
		when(intent.getStringExtra(SearchUserProfilesService.EXTRAS_SEARCH)).thenReturn(null);
	}

	private void intentWithoutReceiver() {
		when(intent.getParcelableExtra(
				SearchUserProfilesService.EXTRAS_RESULT_RECEIVER)).thenReturn(null);
	}

	private void apiThrowingExceptionForSomeReason() {
		doThrow(new RuntimeException("")).when(api).find(Mockito.anyString());
	}

	private void apiFoundingUserProfilesWithSearch() {
		when(api.find("man of the hour")).thenReturn(foundProfiles);
		when(foundProfiles.asBundle()).thenReturn(bundle);
	}

	private void intentWithReceiverAndSearch() {
		when(intent.getParcelableExtra(SearchUserProfilesService.EXTRAS_RESULT_RECEIVER))
			.thenReturn(receiver);
		when(intent.getStringExtra(SearchUserProfilesService.EXTRAS_SEARCH)).thenReturn("man of the hour");
	}
	
}
