package com.whereabts.service.intent;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.message.FindFollowingMessagesApi;
import com.whereabts.model.CompactMessages;
import com.whereabts.util.WhereabtsResultReceiver;

public class FindFollowingMessagesServiceTest extends WhereabtsTest {

	@Inject
	private FindFollowingMessagesService service;
	@Mock
	private FindFollowingMessagesApi api;
	@Mock
	private Intent intent;
	@Mock
	private WhereabtsResultReceiver receiver;
	@Mock
	private CompactMessages messages;
	
	private Bundle bundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(FindFollowingMessagesApi.class, api);
		super.setUp();
	}
	
	@Test
	public void shouldFindFollowingMessagesFromApi() {
		
		intentWithReceiverAndSkip(1);
		apiReturningCompactMessges();
		
		service.onHandleIntent(intent);
		
		verify(api).findFollowingMessagesOlderThan(1, 123123L);
		verify(receiver).send(0, bundle);
	}
	
	@Test
	public void shouldNotDoAnythingWhenReceiverIsMissingFromIntent() {
		
		intentMissingReceiver();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.times(0)).findFollowingMessagesOlderThan(1, 123123L);
	}
	
	@Test
	public void shouldSendErrorWhenExceptionIsThrownFromApi() {
		
		intentWithReceiverAndSkip(1);
		apiThrowingException();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(1, bundle);
	}
	
	@Test
	public void shouldSendEmptyMessagesWhenSkippingIsInLimit() {
		
		intentWithReceiverAndSkip(250);
		
		service.onHandleIntent(intent);
		
		verify(api, never()).findFollowingMessagesOlderThan(250, 123123L);
		verify(receiver).send(Mockito.eq(0), Mockito.any(Bundle.class));
	}

	private void apiThrowingException() {
		Mockito.doThrow(new RuntimeException()).when(api).findFollowingMessagesOlderThan(1, 123123L);
	}

	private void intentMissingReceiver() {
		when(intent.getParcelableExtra(FindFollowingMessagesService.EXTRAS_RECEIVER)).thenReturn(null);
	}

	private void apiReturningCompactMessges() {
		when(api.findFollowingMessagesOlderThan(1, 123123L)).thenReturn(messages);
		when(messages.asBundle()).thenReturn(bundle);
	}

	private void intentWithReceiverAndSkip(int skip) {
		when(intent.getIntExtra(FindFollowingMessagesService.EXTRAS_SKIP, 0)).thenReturn(skip);
		when(intent.getLongExtra(Mockito.eq(FindFollowingMessagesService.EXTRAS_OLDER_THAN), Mockito.anyLong())).thenReturn(123123L);
		when(intent.getParcelableExtra(FindFollowingMessagesService.EXTRAS_RECEIVER)).thenReturn(receiver);
	}
	
}
