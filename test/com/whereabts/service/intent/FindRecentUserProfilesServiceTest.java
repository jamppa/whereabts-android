package com.whereabts.service.intent;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.content.Intent;
import android.os.Bundle;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.api.user.UserProfilesSearchApi;
import com.whereabts.model.UserProfiles;
import com.whereabts.util.WhereabtsResultReceiver;

public class FindRecentUserProfilesServiceTest extends WhereabtsTest {

	@Inject
	private FindRecentUserProfilesService service;
	@Mock
	private UserProfilesSearchApi api;
	@Mock
	private Intent intent;
	@Mock
	private WhereabtsResultReceiver receiver;
	@Mock
	private UserProfiles foundProfiles;
	
	private Bundle asBundle = new Bundle();
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(UserProfilesSearchApi.class, api);
		super.setUp();
	}
	
	@Test
	public void shouldFindRecentUserProfilesFromUserProfilesSearchApi() {
		
		intentWithResultReceiver();
		apiFoundingRecentProfiles();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(0, asBundle);
	}
	
	@Test
	public void shouldSendErrorWhenUserProfilesSearchThrowsException() {
		
		intentWithResultReceiver();
		apiThrowingException();
		
		service.onHandleIntent(intent);
		
		verify(receiver).send(Mockito.eq(-1), Mockito.any(Bundle.class));
	}
	
	@Test
	public void shouldReturnImmediatelyWhenIntentHasNoReceiver() {
		
		intentWithoutResultReceiver();
		apiFoundingRecentProfiles();
		
		service.onHandleIntent(intent);
		
		verify(api, Mockito.times(0)).findMostRecent();
	}

	private void intentWithoutResultReceiver() {
		when(intent.getParcelableExtra(
				FindRecentUserProfilesService.EXTRAS_RESULT_RECEIVER)).thenReturn(null);
	}

	private void apiThrowingException() {
		doThrow(new RuntimeException("")).when(api).findMostRecent();
	}

	private void apiFoundingRecentProfiles() {
		when(api.findMostRecent()).thenReturn(foundProfiles);
		when(foundProfiles.asBundle()).thenReturn(asBundle);
	}

	private void intentWithResultReceiver() {
		when(intent.getParcelableExtra(
				FindRecentUserProfilesService.EXTRAS_RESULT_RECEIVER)).thenReturn(receiver);
	}
	
}
