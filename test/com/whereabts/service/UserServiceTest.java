package com.whereabts.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import android.content.Context;
import android.telephony.TelephonyManager;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.model.User;
import com.whereabts.model.UserProfile;
import com.whereabts.repo.UserRepository;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserServiceTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private UserService userService;
	@Mock
	private UserRepository repository;
	@Mock
	private Context context;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(UserRepository.class, repository);
		testModule.addBinding(Context.class, context);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldHaveRegisteredUserWhenRepositoryReturnsWholeUser() {
		
		repositoryReturnsAUser(true);
		
		boolean hasRegisteredUser = userService.hasRegisteredUser();
		
		assertTrue(hasRegisteredUser);
	}
	
	@Test
	public void shouldNotHaveRegisteredUserWhenRepositoryReturnsUnwholeUser() {
		
		repositoryReturnsAUser(false);
		
		boolean hasRegisteredUser = userService.hasRegisteredUser();
		
		assertFalse(hasRegisteredUser);
	}
	
	@Test
	public void shouldReturnUserFromRepositoryWhenFindingOne() {
		
		User userFromRepo = repositoryReturnsAUser(true);
		
		User user = userService.findUser();
		
		assertEquals(userFromRepo, user);
	}
	
	@Test
	public void shouldReturnPublicUserWhenRepositoryDoesntFindOne() {
		
		repositoryReturnsAUser(false);
		
		User user = userService.findUser();
		
		assertTrue(user.isPublicUser());
	}
	
	@Test
	public void shouldSaveUserToRepository() {
		
		User user = mock(User.class);
		
		userService.saveUser(user);
		
		verify(repository).saveUser(user);
	}
	
	@Test
	public void shouldSaveUserProfileToRepository() {
		
		UserProfile profile = mock(UserProfile.class);
		
		userService.saveUserProfile(profile);
		
		verify(repository).saveUserProfile(profile);
	}
	
	@Test
	public void shouldReturnUsersCountryCodeAsSimCountryWhenSuchIsPresent() {
		
		TelephonyManager telephone = contextWithTelephonyManager();
		when(telephone.getSimCountryIso()).thenReturn("FI");
		
		String countryCode = userService.userCountry();
		
		assertEquals("FI", countryCode);
	}
	
	@Test
	public void shouldReturnUsersCountryCodeAsDefaultLocaleWhenSimCountryIsNotAvailable() {
		
		TelephonyManager telephone = contextWithTelephonyManager();
		when(telephone.getSimCountryIso()).thenReturn("");
		
		String countryCode = userService.userCountry();
		
		assertEquals("US", countryCode);
	}

	private TelephonyManager contextWithTelephonyManager() {
		TelephonyManager telephone = mock(TelephonyManager.class);
		when(context.getSystemService(Context.TELEPHONY_SERVICE)).thenReturn(telephone);
		return telephone;
	}

	private User repositoryReturnsAUser(boolean isWhole) {
		User user = mock(User.class);
		when(user.isWhole()).thenReturn(isWhole);
		when(repository.getUser()).thenReturn(user);
		return user;
	}
	
}
