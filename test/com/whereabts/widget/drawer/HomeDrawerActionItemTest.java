package com.whereabts.widget.drawer;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.TestGuiceModule;
import com.whereabts.activity.ActivityLauncher;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class HomeDrawerActionItemTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private HomeDrawerActionItem homeDrawerActionItem;
	@Mock
	private ActivityLauncher activityLauncher;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(ActivityLauncher.class, activityLauncher);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldLaunchMapActivityWhenDoingAction() {
		
		homeDrawerActionItem.doAction();
		
		verify(activityLauncher).launchMapActivityToTop();
	}
	
	@Test
	public void shouldHaveLabelResource() {
		
		int resId = homeDrawerActionItem.labelResource();
		
		assertEquals(R.string.drawer_item_home, resId);
	}
	
	@Test
	public void shouldHaveIconResource() {
		
		int resId = homeDrawerActionItem.iconResource();
		
		assertEquals(R.drawable.ic_map, resId);
	}
	
}
