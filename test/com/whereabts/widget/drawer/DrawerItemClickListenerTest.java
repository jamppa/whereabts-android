package com.whereabts.widget.drawer;

import static org.mockito.Mockito.verify;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import android.widget.AdapterView;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class DrawerItemClickListenerTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private DrawerItemClickListener listener;
	
	@Mock
	private DrawerActionItem drawerActionItem;
	@Mock
	private AdapterView<?> adapter;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void shouldDoActionWhenClicked() {
		
		int position = 0;
		Mockito.when(adapter.getItemAtPosition(position)).thenReturn(drawerActionItem);
		
		listener.onItemClick(adapter, null, position, 0);
		
		verify(drawerActionItem).doAction();
	}
	
	@Test
	public void shouldDoNothingWhenItemReturnedIsNull() {
		
		int position = 0;
		Mockito.when(adapter.getItemAtPosition(position)).thenReturn(null);
		
		listener.onItemClick(adapter, null, position, 0);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
