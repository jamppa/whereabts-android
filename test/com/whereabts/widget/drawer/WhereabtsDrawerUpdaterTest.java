package com.whereabts.widget.drawer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.repo.WhereabtsDrawerRepository;

public class WhereabtsDrawerUpdaterTest extends WhereabtsTest {

	@Inject
	private WhereabtsDrawerUpdater drawerUpdater;
	
	@Mock
	private WhereabtsDrawerRepository repository;
	@Mock
	private WhereabtsDrawerVersion drawerVersion;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(WhereabtsDrawerRepository.class, repository);
		testModule.addBinding(WhereabtsDrawerVersion.class, drawerVersion);
		super.setUp();
	}
	
	@Test
	public void shouldUpdateDrawerVersionToReposiToryWhenItHasChanged() {
		
		when(drawerVersion.get()).thenReturn("v2");
		when(repository.getDrawerVersion()).thenReturn("v1");
		
		boolean updated = drawerUpdater.update();
		
		assertTrue(updated);
		verify(repository).saveDrawerVersion("v2");
	}
	
	@Test
	public void shouldNotUpdateDrawerVersionToRepositoryWhenItHasNotChanged() {
		
		when(drawerVersion.get()).thenReturn("v1");
		when(repository.getDrawerVersion()).thenReturn("v1");
		
		boolean updated = drawerUpdater.update();
		
		assertFalse(updated);
		verify(repository, times(0)).saveDrawerVersion("v1");
	}
	
}
