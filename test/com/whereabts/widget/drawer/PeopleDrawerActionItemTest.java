package com.whereabts.widget.drawer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.WhereabtsTest;
import com.whereabts.activity.ActivityLauncher;

public class PeopleDrawerActionItemTest extends WhereabtsTest {

	@Inject
	private PeopleDrawerActionItem peopleDrawerItem;
	@Mock
	private ActivityLauncher launcher;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(ActivityLauncher.class, launcher);
		super.setUp();
	}
	
	@Test
	public void shouldHaveLabelResource() {
		
		int res = peopleDrawerItem.labelResource();
		
		assertEquals(R.string.drawer_item_people, res);
	}
	
	@Test
	public void shouldHaveIconResource() {
		
		int res = peopleDrawerItem.iconResource();
		
		assertEquals(R.drawable.ic_group, res);
	}
	
	@Test
	public void shouldLaunchPeopleActivity() {
		
		peopleDrawerItem.doAction();
		
		verify(launcher).launchPeopleActivity();
	}
	
}
