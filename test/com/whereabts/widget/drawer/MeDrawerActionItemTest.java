package com.whereabts.widget.drawer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.model.User;
import com.whereabts.service.UserService;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class MeDrawerActionItemTest {
	
	private TestGuiceModule testGuiceModule = new TestGuiceModule();

	@Inject
	private MeDrawerActionItem meDrawerAction;
	
	@Mock
	private UserService userService;
	@Mock
	private ActivityLauncher activityLauncher;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testGuiceModule.addBinding(UserService.class, userService);
		testGuiceModule.addBinding(ActivityLauncher.class, activityLauncher);
		TestGuiceModule.setUp(this, testGuiceModule);
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldLaunchUserProfileActivityWhenUserHasProfile() {
		
		User user = User.newUser("123AbC", "", "");
		when(userService.findUser()).thenReturn(user);
		
		meDrawerAction.doAction();
		
		verify(activityLauncher).launchShowUserProfileActivity("123AbC");
	}
	
	
}
