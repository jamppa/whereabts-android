package com.whereabts.widget.drawer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.WhereabtsTest;
import com.whereabts.activity.ActivityLauncher;

public class FollowingDrawerActionItemTest extends WhereabtsTest {

	@Inject
	private FollowingDrawerActionItem item;
	@Mock
	private ActivityLauncher activityLauncher;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(ActivityLauncher.class, activityLauncher);
		super.setUp();
	}
	
	@Test
	public void shouldHaveIconResource() {
		
		int iconRes = item.iconResource();
		
		assertEquals(R.drawable.ic_following_stream, iconRes);
	}
	
	@Test
	public void shouldHaveLabelResource() {
		
		int lableRes = item.labelResource();
		
		assertEquals(R.string.drawer_item_follow, lableRes);
	}
	
	@Test
	public void shouldLaunchFollowingStreamActivity() {
		
		item.doAction();
		
		verify(activityLauncher).launchFollowingStreamActivity();
	}
	
}
