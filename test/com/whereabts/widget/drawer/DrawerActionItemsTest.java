package com.whereabts.widget.drawer;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.google.inject.Inject;
import com.whereabts.TestGuiceModule;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class DrawerActionItemsTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private DrawerActionItems drawerActionItems;
	@Mock
	private MeDrawerActionItem meDrawerActionItem;
	@Mock
	private FollowingDrawerActionItem followingDrawerActionItem;
	@Mock
	private PeopleDrawerActionItem peopleDrawerActionItem;
	@Mock
	private SettingsDrawerActionItem settingsDrawerActionItem;
	@Mock
	private InviteDrawerActionItem inviteDrawerActionItem;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(MeDrawerActionItem.class, meDrawerActionItem);
		testModule.addBinding(FollowingDrawerActionItem.class, followingDrawerActionItem);
		testModule.addBinding(PeopleDrawerActionItem.class, peopleDrawerActionItem);
		testModule.addBinding(SettingsDrawerActionItem.class, settingsDrawerActionItem);
		testModule.addBinding(InviteDrawerActionItem.class, inviteDrawerActionItem);
		TestGuiceModule.setUp(this, testModule);
	}
	
	@Test
	public void allDrawerActionItemsShouldContainMeDrawerActionItem() {
		
		List<DrawerActionItem> allItems = drawerActionItems.allDrawerActionItems();
		
		assertTrue(allItems.contains(meDrawerActionItem));
	}
		
	@Test
	public void allDrawerActionItemsShouldContainFollowingActionItem() {
		
		List<DrawerActionItem> allItems = drawerActionItems.allDrawerActionItems();
		
		assertTrue(allItems.contains(followingDrawerActionItem));
	}
	
	@Test
	public void allDrawerActionItemsShouldContainPeopleActionItem() {
		
		List<DrawerActionItem> allItems = drawerActionItems.allDrawerActionItems();
		
		assertTrue(allItems.contains(peopleDrawerActionItem));
	}
	
	@Test
	public void allDrawerActionItemsShouldContainSettingsActionItem() {
		
		List<DrawerActionItem> allItems = drawerActionItems.allDrawerActionItems();
		
		assertTrue(allItems.contains(settingsDrawerActionItem));
	}
	
	@Test
	public void allDrawerActionItemsShouldContainInviteActionItem() {
		
		List<DrawerActionItem> allItems = drawerActionItems.allDrawerActionItems();
		
		assertTrue(allItems.contains(inviteDrawerActionItem));
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
}
