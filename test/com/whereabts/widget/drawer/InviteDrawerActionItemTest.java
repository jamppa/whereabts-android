package com.whereabts.widget.drawer;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import android.content.Intent;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.WhereabtsTest;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.service.InviteFriendsIntentCreateService;
import com.whereabts.util.StringUtils;

public class InviteDrawerActionItemTest extends WhereabtsTest {

	@Inject
	private InviteDrawerActionItem drawerItem;
	@Mock
	private InviteFriendsIntentCreateService inviteCreateService;
	@Mock
	private ActivityLauncher activityLauncher;
	@Mock
	private StringUtils stringUtils;
	@Mock
	private Intent inviteIntent;
	
	@Override
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(InviteFriendsIntentCreateService.class, inviteCreateService);
		testModule.addBinding(ActivityLauncher.class, activityLauncher);
		testModule.addBinding(StringUtils.class, stringUtils);
		super.setUp();
	}
	
	@Test
	public void shouldHaveLabelResource() {
		
		int res = drawerItem.labelResource();
		
		assertEquals(R.string.drawer_item_invite, res);
	}
	
	@Test
	public void shouldHaveIconResource() {
		
		int res = drawerItem.iconResource();
		
		assertEquals(R.drawable.ic_share, res);
	}
	
	@Test
	public void shouldLaunchInviteChooser() {
		
		when(inviteCreateService.createIntent()).thenReturn(inviteIntent);
		when(stringUtils.getString(R.string.drawer_item_invite)).thenReturn("Invite friends");
		
		drawerItem.doAction();
		
		Mockito.verify(activityLauncher).launchInviteFriendsChooser(inviteIntent, "Invite friends");
	}
	
}
