package com.whereabts.widget.userprofile;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import android.view.Menu;
import android.view.MenuItem;

import com.google.inject.Inject;
import com.whereabts.R;
import com.whereabts.TestGuiceModule;
import com.whereabts.model.UserWithProfile;
import com.whereabts.util.ToastShower;
import com.xtremelabs.robolectric.RobolectricTestRunner;

@RunWith(RobolectricTestRunner.class)
public class UserProfileMenuTest {

	private TestGuiceModule testModule = new TestGuiceModule();
	
	@Inject
	private UserProfileMenu userProfileMenu;
	@Mock
	private ToastShower toastShower;
	@Mock
	private Menu menu;
	@Mock
	private MenuItem followMenuItem;
	@Mock
	private MenuItem editMenuItem;
	@Mock
	private UserWithProfile userWithProfile;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		testModule.addBinding(ToastShower.class, toastShower);
		TestGuiceModule.setUp(this, testModule);
		menuContainingItems();
	}
	
	@After
	public void tearDown() {
		TestGuiceModule.tearDown();
	}
	
	@Test
	public void shouldShowFollowMenuItemWhenNotFollowing() {
		
		userNotFollowing();
		
		userProfileMenu.showMenuItems(userWithProfile);
		
		verifyFollowMenuItemWasShown(1);
	}
	
	@Test
	public void shouldShowUnfollowMenuItemWhenFollowing() {
		
		userFollowing();
		
		userProfileMenu.showMenuItems(userWithProfile);
		
		verifyUnfollowMenuItemWasShown(1);
	}
	
	@Test
	public void shouldShowOwnerControlsWhenViewingOwnProfile() {
		
		userOwningProfile();
		
		userProfileMenu.showMenuItems(userWithProfile);
		
		verifyOwnerControlsAreShown();
	}
	
	@Test
	public void shouldNotShowOwnerControlsWhenNotViewingOwnProfile() {
		
		userNotOwningProfile();
		
		userProfileMenu.showMenuItems(userWithProfile);
		
		verifyOwnerControlsAreNotShown();
	}
	
	@Test
	public void shouldToggleFollowMenuItemToUnfollowWhenNotFollowing() {
		
		userNotFollowing();
		userProfileMenu.showMenuItems(userWithProfile);
		
		userProfileMenu.toggleFollowMenuItem();
		
		verifyUnfollowMenuItemWasShown(2);
	}

	@Test
	public void shouldToggleFollowMenuItemToFollowWhenFollowing() {
		
		userFollowing();
		userProfileMenu.showMenuItems(userWithProfile);
		
		userProfileMenu.toggleFollowMenuItem();
		
		verifyFollowMenuItemWasShown(2);
	}
	
	private void verifyOwnerControlsAreNotShown() {
		verify(editMenuItem, times(0)).setVisible(true);
	}

	private void userNotOwningProfile() {
		when(userWithProfile.isOwnProfile()).thenReturn(false);
	}

	private void verifyOwnerControlsAreShown() {
		verify(editMenuItem).setVisible(true);
		verify(followMenuItem).setVisible(false);
	}

	private void userOwningProfile() {
		when(userWithProfile.isOwnProfile()).thenReturn(true);
	}

	private void verifyUnfollowMenuItemWasShown(int times) {
		verify(followMenuItem).setIcon(R.drawable.ic_unfollow_person);
		verify(followMenuItem).setTitle(R.string.menu_show_profile_unfollow);
		verify(followMenuItem, times(times)).setVisible(true);
	}

	private void userFollowing() {
		when(userWithProfile.isFollowing()).thenReturn(true);
	}

	private void verifyFollowMenuItemWasShown(int times) {
		verify(followMenuItem).setIcon(R.drawable.ic_follow_person);
		verify(followMenuItem).setTitle(R.string.menu_show_profile_follow);
		verify(followMenuItem, times(times)).setVisible(true);
	}

	private void userNotFollowing() {
		when(userWithProfile.isFollowing()).thenReturn(false);
	}

	private void menuContainingItems() {
		when(menu.findItem(R.id.activity_show_userprofile_follow)).thenReturn(followMenuItem);
		when(menu.findItem(R.id.activity_show_userprofile_edit)).thenReturn(editMenuItem);
		userProfileMenu.setMenu(menu);
	}
	
}
