package com.whereabts.widget.userprofile;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import android.widget.AdapterView;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.model.UserProfile;

public class UserProfileItemClickListenerTest extends WhereabtsTest {

	@Inject
	private UserProfileItemClickListener listener;
	@Mock
	private ActivityLauncher activityLauncher;
	@Mock
	private AdapterView<?> adapter;
	@Mock
	private UserProfile profile;
	
	@Override
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(ActivityLauncher.class, activityLauncher);
		super.setUp();
	}
	
	@Test
	public void shouldLaunchUserProfileOnClick() {
		
		when(adapter.getItemAtPosition(0)).thenReturn(profile);
		when(profile.userId()).thenReturn("123");
		
		listener.onItemClick(adapter, null, 0, 0);
		
		verify(activityLauncher).launchShowUserProfileActivity("123");
	}
	
	@Test
	public void shouldNotLaunchUserProfileOnClickWhenItemIsNull() {
		
		when(adapter.getItemAtPosition(0)).thenReturn(null);

		listener.onItemClick(adapter, null, 0, 0);
		
		verify(activityLauncher, times(0)).launchShowUserProfileActivity(Mockito.anyString());
	}
	
}
