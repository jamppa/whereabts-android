package com.whereabts.widget;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.google.inject.Inject;
import com.whereabts.WhereabtsTest;
import com.whereabts.activity.ActivityLauncher;
import com.whereabts.model.Message;
import com.whereabts.util.StringUtils;

public class MessagePhotoClickListenerTest extends WhereabtsTest {

	@Inject
	private MessagePhotoClickListener listener;
	
	@Mock
	private ActivityLauncher activityLauncher;
	@Mock
	private StringUtils stringUtils;
	@Mock
	private Message whereaboutsMessage;
	
	@Before
	public void setUp() {
		initMocks(this);
		testModule.addBinding(ActivityLauncher.class, activityLauncher);
		testModule.addBinding(StringUtils.class, stringUtils);
		super.setUp();
	}
	
	@Test
	public void shouldLaunchShowFullscreenActivityForMessage() {
		
		whereaboutsMessageWithPhoto(true);
		listener.setMessage(whereaboutsMessage);
		
		listener.onClick(null);
		
		verify(activityLauncher).launchShowPhotoFullscreenActivity("photourl");
	}
	
	@Test
	public void shouldNotLaunchShowFullscreenActivityForMessageWhenItDoesntHavePhoto() {
		
		whereaboutsMessageWithPhoto(false);
		listener.setMessage(whereaboutsMessage);
		
		listener.onClick(null);
		
		verify(activityLauncher, never()).launchShowPhotoFullscreenActivity(Mockito.anyString());
	}
	
	@Test
	public void shouldNotLaunchShowFullscreenActivityForMessageWhenItHappensToBeNull() {
		
		listener.setMessage(null);
		
		listener.onClick(null);
		
		verify(activityLauncher, never()).launchShowPhotoFullscreenActivity(Mockito.anyString());
	}

	private void whereaboutsMessageWithPhoto(boolean hasPhoto) {
		when(whereaboutsMessage.hasPhoto()).thenReturn(hasPhoto);
		when(whereaboutsMessage.messagePhotoUrl(Mockito.anyString())).thenReturn("photourl");
	}
}
